# Homomorphic Encryption Stage 2
Here we house code for the homomorphic encryption project. All code can be run inside a docker container; the current version is called `zacharyzanussi/seal:3.5`. 

## Cleartext Implementation
We have first implemented the network in the cleartext in the files `neural.cpp`, `neural.h`, and `network.cpp`. Currently, the network is implemented and working for a single layer network; to use multilayers, I still need to implement backward propagation. This shouldn't be a big job but we don't need it yet and I've opted to move onto more pressing matters (I'm being "agile").

We use four classes; 
- `Data`, which holds the input entries and the output labels, both implemented as `vector<vector<double>>`, or as vectors that hold more vectors. SEAL takes `vector<double>`'s as input, so it makes sense to use these wherever possible. The `Data` class has as attributes `entries`, which holds the input to the network, and `labels`, which holds the output. It also has some attributes that hold data such as the number of vectors (`num_examples`) and the dimensions of the vectors (`num_features` and `num_labels`). We also have a member function `get_mini_batch` which gives a mini batch in the form of another instance of `Data` which can easily be passed into the propagation functions. This function takes two integers, the first being the index of the first entry to take, and the second being the number of entries to take. 
This class has a constructor that takes the file name of the dataset and loads directly from there, as well as one that takes lists of vectors for the entries and labels.
- `Caches`, which is only necessary in a multilayer network. For this reason, this class has not been implemented. However, it's designed to hold the cached values that are necessary in backward propagation, that is, the activations that are outputed by the hidden layers. 
- `WeightLayer`, which is one layer in the neural network. It stores its weights in an attribute called `weights`, a `vector<vector<double>>`. This is to be interpreted as a matrix, not as a list of vectors. The `WeightLayer` class also has attributes that store its dimensions and the type of activation layer, which is built in. Each `WeightLayer` is responsible for propagating data past itself, using the `propagate_past` and `propagate_past_back` functions. It's also responsible for updating it's own weights using the gradients computed in back prop. This class has a constructor that takes dimensions and an activation and randomly generates itself. Future work on this class would be to implement more activation functions; currently only softmax is implemented.
- `NeuralNet`, which has as attributes a vector of `WeightLayer`'s and also data such as hyperparameters and the dimensions and activations of it's layers. The final goal of this class is that we simply pass it a `Data` instance and it automatically performs forward and back prop and updates its layers. It has functions that take data and propagate it forward and backward using the functions in its `WeightLayer`s. It has a constructor that takes the dimensions of its layers and automatically generates each of its layers. 

### Ciphertext Implementation and control flow;

This topic deserves its own page; see the wiki.

### Parallelism

By default, C++ only uses one thread, which means that only one computing core is in use at a time. This is a waste; my computer has 8 cores, and the cloud currently has 4. I'm working on implementing parallelism using a dispatch queue (see [here](https://embeddedartistry.com/blog/2017/02/08/implementing-an-asynchronous-dispatch-queue/)). This has not be implemented in the cleartext, however currently in the ciphertext we use dispatch queues in two ways; first, a queue with multiple threads that propagates a ciphertext forward and backward deposits the gradient in another queue, which has a single thread that sums all the gradients before proceeding. Thus, the multithreading occurs at the batch level.

## Network Implementation

I think there is nothing surprising about how the network itself is implemented (in the file `network.cpp`). First, we load the train and test sets into `Data` instances called `train_data` and `test_data` respectively. Then, in a loop we grab a batch (in a new `Data` instance) and perform forward and back prop on it. At the end, we perform a prediction on the test data and print the results. 

## Directory Structure

All the C++ work is contained in the directory `cpp`. In here, I have followed a standard directory structure for a C++ project; there are folders `src` which store any `.cpp` file that has no `main` function, `include` which stores header files (`.h` and `.hpp`), and `test` which stores `.cpp` files with a `main` function. We also have `stage1` which holds the source files for stage 1 of this project; none of these are currently in use in the second stage. Finally, there is `CMakeLists.txt`. This is crucial for building the project using `cmake`, which is the only way I've been able to get `SEAL` to work. One needs Cmake 1.12 or higher, which is a problem for some computers (including mine, as Ubuntu 18 does not include this version of `cmake`). If this does not compile on your computer, use the docker image. 

To compile, create a directory `bin` inside the directory `cpp` and from inside `bin` run `cmake ..`. This puts all the garbage that `cmake` creates inside this directory, so if you need to recompile from scratch you can just delete the `bin` folder. `cmake` generates `makefiles`, so after that run `make` from within the same directory and you will generate the all the executables, which you just need to run. 

If you are going to run it yourself, beware the amount of data you load in. This is controlled by parameters `num_examples` for training and `num_test_examples` for testing. My 8GB computer can only handle about 15000 total; any more will cause a crash.