### CMakeLists.txt to aid compiling programs running Microsoft SEAL
## sample compilation command: 
# cmake . -DCMAKE_PREFIX_PATH=/home/zantos/mylibs/lib/cmake/SEAL-3.7/ -DWITH_TESTING=FALSE -Bbuild 

cmake_minimum_required(VERSION 3.12)

# You can change the project name, if you'd like
project(sample)

SET(CMAKE_CXX_STANDARD 17)

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Ofast")
set(TESTING_OPTION_STR "Using testing dev")
option(WITH_TESTING ${TESTING_OPTION_STR} OFF)

# The testing flag can be set with -DWITH_TESTING=True
if(WITH_TESTING)
	message("${TESTING_OPTION_STR}")
    add_definitions(-DTESTING)
    find_package(Boost REQUIRED COMPONENTS timer log)
    find_package(OpenMP REQUIRED)
else()
	message("Not using testing")
endif()

SET(CMAKE_CXX_FLAGS_DEBUG  "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra -Wfloat-equal -pedantic -Werror -fdiagnostics-parseable-fixits")

find_package(SEAL 3.7 REQUIRED)


include_directories(include)

# Put all of your .cpp files here, space separated, in quotes
file(GLOB SOURCES "sample.cpp")

# You will need a 'add_executable' and 'target_link_library' pair for each
# executable file (.cpp file with a main function)
add_executable(main ${SOURCES} main.cpp)
target_link_libraries(main SEAL::seal)
