// An sample program that should help you figure out how to compile your
// own projects using cmake. 

#include "sample.h"


using namespace std;
using namespace seal;


int main() 
{
	EncryptionParameters parms(scheme_type::ckks);

	size_t poly_modulus_degree = 8192;
    parms.set_poly_modulus_degree(poly_modulus_degree);
    parms.set_coeff_modulus(CoeffModulus::Create(poly_modulus_degree, { 60, 40, 40, 60 }));

	double scale = pow(2.0, 40);

    SEALContext context(parms);
    cout << endl;

    KeyGenerator keygen(context);
    auto secret_key = keygen.secret_key();
    PublicKey public_key;
    keygen.create_public_key(public_key);
    RelinKeys relin_keys;
    keygen.create_relin_keys(relin_keys);
    GaloisKeys gal_keys;
    keygen.create_galois_keys(gal_keys);
    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    Decryptor decryptor(context, secret_key);

    CKKSEncoder encoder(context);
    size_t slot_count = encoder.slot_count();
    cout << "Number of slots: " << slot_count << endl;

    vector<double> input;
    input.reserve(slot_count);
    double curr_point = 0;
    double step_size = 1;
    for (size_t i = 0; i < slot_count; i++)
    {
        input.push_back(curr_point);
        curr_point += step_size;
    }
    Plaintext pt;
    encoder.encode(input, scale, pt);
    Ciphertext ct;
    encryptor.encrypt(pt, ct);

    cout << "Evaluating polynomial 1/16 x^2 + 1/4 x + 1/8 ..." << endl;


	vector<parms_id_type> eparms_ids;

	auto count_context = context.first_context_data();

	while (count_context) {
		eparms_ids.push_back(count_context->parms_id());
		count_context = count_context->next_context_data();
	}
	cout << "Collected " << eparms_ids.size() << " parms_ids" << endl;

	vector<Plaintext> constants;
    generate_softmax_constants(scale, encoder, eparms_ids, 0, constants);
    softmax_approx_privft_inplace(ct, constants, evaluator, relin_keys);

    decryptor.decrypt(ct, pt);
    vector<double> results;
    encoder.decode(pt, results);

    for (int i = 0; i < 10; i++) {
    	cout << input.at(i) << " -> " << results.at(i) << endl;
    }
}