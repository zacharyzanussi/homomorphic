#include "sample.h"

void softmax_approx_privft_inplace(Ciphertext& input,
                                   vector<Plaintext>& constants,
                                   Evaluator& evaluator,
                                   const RelinKeys& relin_keys) {
  // 1/8 x^2 + 1/2 x + 1/4
  Ciphertext sqd, lin, output;
  evaluator.multiply(input, input, sqd);
  evaluator.relinearize_inplace(sqd, relin_keys);
  evaluator.rescale_to_next_inplace(sqd);  // L - 2

  evaluator.multiply_plain_inplace(sqd, constants.at(0));
  evaluator.rescale_to_next_inplace(sqd);  // L - 3
  sqd.scale() = input.scale();

  evaluator.multiply_plain(input, constants.at(1), lin);
  evaluator.rescale_to_next_inplace(lin);     // L - 2
  evaluator.mod_switch_to_next_inplace(lin);  // L - 3
  lin.scale() = input.scale();

  evaluator.add(sqd, lin, output);
  evaluator.add_plain_inplace(output, constants.at(2));

  input = output;
}

void generate_softmax_constants(size_t scale, CKKSEncoder& encoder,
                                vector<parms_id_type>& parms_ids, size_t level,
                                vector<Plaintext>& constants) {
  constants.clear();
  Plaintext over2_pt, over4_pt, over8_pt;
  double factor = 2.0;

  encoder.encode(1.0 / 2.0 / factor, parms_ids.at(level), scale, over2_pt);
  encoder.encode(1.0 / 8.0 / factor, parms_ids.at(level + 1), scale, over8_pt);
  encoder.encode(1.0 / 4.0 / factor, parms_ids.at(level + 2), scale, over4_pt);

  constants.push_back(over8_pt);
  constants.push_back(over2_pt);
  constants.push_back(over4_pt);
}
