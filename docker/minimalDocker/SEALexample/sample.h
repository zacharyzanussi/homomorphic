#include <vector>
#include "seal/seal.h"

using namespace std;
using namespace seal;


void softmax_approx_privft_inplace(Ciphertext& input,
                                   vector<Plaintext>& constants,
                                   Evaluator& evaluator,
                                   const RelinKeys& relin_keys);

void generate_softmax_constants(size_t scale, CKKSEncoder& encoder,
                                vector<parms_id_type>& parms_ids, size_t level,
                                vector<Plaintext>& constants);