#!/bin/bash

# Script for installing Microsoft SEAL in the min-HE docker container
# Written by Zachary Zanussi, September 2021.


mkdir /tmp/SEAL37 && cd /tmp/SEAL37
git clone https://github.com/microsoft/SEAL.git

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++


SEAL_LIB=~/mylibs
cd SEAL/ &&
	cmake -S . -B build -DSEAL_BUILD_EXAMPLES=ON -DCMAKE_INSTALL_PREFIX=$SEAL_LIB &&
	cmake --build build  &&
	cd build && 
	make -j4 &&
	make install

echo " "
echo "SEAL has been installed locally to $SEAL_LIB"
echo "Examples are located in /tmp/SEAL37/SEAL/build/bin"

# cd SEAL/ && 
# 	cmake -S . -B build -DSEAL_BUILD_EXAMPLES=ON && 
# 	cd build && 
# 	make -j4 && 
# 	sudo make install