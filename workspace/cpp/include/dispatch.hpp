#ifndef DISPATCH_HPP
#define DISPATCH_HPP

#pragma once

#include <condition_variable>
#include <cstdint>
#include <cstdio>
#include <functional>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>

//! Class for parallelizing several tasks
/*!
  One problem with creating and deploying threads is that each thread 
  takes up space in memory; this means that if you have, say, 10,000
  tasks, just deploying these will often consume all your RAM.

  One alternative is to create a small number of threads (say, one or
  two for each compute core) and having them select jobs from a queue
  as they become available. This idea is called 'dispatching', and this
  class implements such a dispatch queue. Simply create the queue and
  deposit the jobs, and the threads will work away until the jobs are 
  done. 

  To implement this, I followed
  https://embeddedartistry.com/blog/2017/02/08/implementing-an-asynchronous-dispatch-queue/
  One difference; I had to make some small modifications to this to 
  make it so that when the queue is destructed, the threads will 
  finish all the jobs. Before this, upon destruction all jobs in the
  queue were destroyed.

  To pass a function into the queue, you have to use lambda functions
  Usage: 
  \code
  // Construct the queue
  dispatch_queue name(" --- Message --- ", num_threads);
  for (...) {
    // Pass tasks into the queue
    name.dispatch([your, &function, &arguments] {
      function(your, arguments);
    });
  }
  // Upon destruction (ie leaving scope), the queue will pause execution
  and empty before continuing
  \endcode
*/
class dispatch_queue {
  typedef std::function<void(void)> fp_t; /*!< template for functions that will
                                               be passed into the queue*/

 public:
  //! Construct a new queue
  /*! 
    Construct a new queue that you can pass tasks into. 
    \param name Name of the queue
    \param thread_cnt Number of threads to create and run. Typically maxes
      out at twice the number of your compute cores, but you can really set
      this to whatever you'd like
  */
  dispatch_queue(std::string name, size_t thread_cnt = 1);
  ~dispatch_queue();

  //! Dispatch and copy a task into the queue
  /*!
    Passing a lambda function into this will add it to the queue. By default, the 
    function must be <void(void)>; that is, it must take no input and return nothing.
    You can change the declaration of `fp_t` above if you want this to be different.
    \param op The function to be added to the queue
  */
  void dispatch(const fp_t& op);
  // Dispatch and move a task into the queue
  /*!
    Identical (almost)to `dispatch(const fp_t& op), go see that one
    \sa dispatch()
  */ 
  void dispatch(fp_t&& op);

  // Deleted operations
  dispatch_queue(const dispatch_queue& rhs) = delete;
  dispatch_queue& operator=(const dispatch_queue& rhs) = delete;
  dispatch_queue(dispatch_queue&& rhs) = delete;
  dispatch_queue& operator=(dispatch_queue&& rhs) = delete;

 private:
  std::string name_;
  std::mutex lock_;
  std::vector<std::thread> threads_;
  std::queue<fp_t> q_;
  std::condition_variable cv_;
  bool quit_ = false;

  // in C, a function with signature fun() can actually take *any* parameter,
  // not just none. So we use void as an input to signify that we actually want
  // no arguments. In C++, fun(void) and fun() are equivalent. So don't read
  // into that void in the input.
  void dispatch_thread_handler(void);
};

#endif