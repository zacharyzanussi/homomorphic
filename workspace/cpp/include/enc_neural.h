#ifndef ENC_NEURAL_H
#define ENC_NEURAL_H

#include <iostream>
#include <string>
#include <vector>

#include "seal/seal.h"
//#include "log.h"

using namespace std;
using namespace seal;

//! Class for storing encrypted data
/*!
  This class stores encrypted data that we can then use to learn on.
  Data is stored as vectors of ciphertexts.

  For now, we leave the labels in plaintext, and the label pool is
  just two values. This is a little easier and will be better for
  figuring out if this stuff works. Later we can implement the
  obscuring protocol.
*/
class EncData {
  
 private:
  vector<Ciphertext> entries; /*!< The encrypted data vectors, stored in a vector */
  // vector<vector<int>> labels;
  vector<int> labels; /*!< The labels, stored as integers for now. These will need to 
                           be obscured before moving to production. */
  vector<Ciphertext> label_pool; /*!< Since labels need to be ciphertexts to interact 
                                      with the data, and these ciphertexts are highly
                                      repetitive, here they are stored for reuse */
  size_t num_examples; /*!< The number of ciphertexts stored in this `EncData` instance */
  size_t num_features; /*!< The number of input features in each entry */
  size_t num_labels;   /*!< The number of output labels, or classes */
  size_t num_packed;   /*!< The number of entries packed into each ciphertext */
  Plaintext zero_one;  /*!< The zero-one plaintext, used to propagate values after summing */

 public:
  //! Default constructor
  /*!
    Default constructor, doesn't load data and initializes values to zeros.
  */
  EncData() : num_examples(0), num_features(0), num_labels(0), num_packed(1){};

  //! Standard EncData Constructor
  /*! 
    This constructor takes all the data one needs to load and encrypt data from 
    a file stream. The file must have the following form: 
    label,description,3gram,4gram,5gram,6gram
  
    The list of *grams should be integers separated by spaces. Only one actually
    needs to be filled in; the rest can be empty, but you should have the commas still.

    Make sure you skip over the first row!

    \param file The input file to read data from. It should have the form described
                above. Make sure you skip over the column labels!
    \param hashor The value to hash the input vectors to. This function currently 
                  only allows hashing, however it can be modified to allow no hashing
                  by adding a parameter to set `num_features` and setting hashor to 0.
                  The packing constructor has this change implemented, so you can check
                  that out
    \param num_labels The number of output labels or classes
    \param num_examples The number of ciphertexts in this instance
    \param encoder An encoder instance
    \param scale The scale to use for encoding
    \param encryptor An encryptor instance
    \param data_parms_id The parameter id to encrypt the data to
    \param label_parms_id The parameter id to encrypt the label pool to
  */
  EncData(ifstream& file, size_t hashor, size_t num_labels, size_t num_examples,
          CKKSEncoder& encoder, size_t scale, Encryptor& encryptor,
          const parms_id_type& data_parms_id,
          const parms_id_type& label_parms_id);

  //! EncData Constructor for packing
  /*! 
    This constructor takes all the data one needs to load and encrypt data,
    while packing it. Make sure you skip over the column headers. The file must have the following form: 
    label,description,3gram,4gram,5gram,6gram

    The list of *grams should be integers separated by spaces. Only one actually
    needs to be filled in; the rest can be empty, but you should have the commas still.

    This function will pack `num_packed` entries into a single ciphertext. It also encodes the 
    zero-one plaintext that you need to perform chunked sums.

    \param file The input file to read data from. It should have the form described
                above. Make sure you skip over the column labels!
    \param num_packed The number of entries to pack into each ciphertext
    \param num_features The number of features in each entry. This value will be 
                        disregarded if `hashor == 0`
    \param hashor The value to hash the input vectors to. This function currently 
                  only allows hashing, however it can be modified to allow no hashing
                  by adding a parameter to set `num_features` and setting hashor to 0
    \param num_labels The number of output labels or classes
    \param num_examples The number of ciphertexts in this instance
    \param encoder An encoder instance
    \param scale The scale to use for encoding
    \param encryptor An encryptor instance
    \param data_parms_id The parameter id to encrypt the data to
    \param label_parms_id The parameter id to encrypt the label pool to
    \param normalize Whether or not to normalize the entry vectors so that they sum to 1.
                     Default is `false`.
    \throws invalid_argument if hashor * num_packed exceeds the slot count
  */
  EncData(ifstream& file, size_t num_packed, size_t num_features, size_t hashor, size_t num_labels,
          size_t num_ciphertexts, CKKSEncoder& encoder, size_t scale,
          Encryptor& encryptor, const parms_id_type& data_parms_id,
          const parms_id_type& zero_one_parms_id,
          const parms_id_type& label_parms_id, bool normalize = false);

  //! Load EncData from filename
  /*! 
    Same as the other non-packing constructor, except you pass in a file name and it 
    takes care of file opening and closing for you. The parameter `burn_in` lets you 
    start partway through the file. This just iterates through the file until it 'burns'
    enough entries, so you should really try passing in a fstream if you can!

    The file must have the following form: 
    label,description,3gram,4gram,5gram,6gram

    The list of *grams should be integers separated by spaces. Only one actually
    needs to be filled in; the rest can be empty, but you should have the commas still.

    \param file The input file to read data from. It should have the form described
                above.
    \param hashor The value to hash the input vectors to. This function currently 
                  only allows hashing, however it can be modified to allow no hashing
                  by adding a parameter to set `num_features` and setting hashor to 0.
                  The packing constructor has this change implemented, so you can check
                  that out
    \param num_labels The number of output labels or classes
    \param num_examples The number of ciphertexts in this instance
    \param encoder An encoder instance
    \param scale The scale to use for encoding
    \param encryptor An encryptor instance
    \param data_parms_id The parameter id to encrypt the data to
    \param label_parms_id The parameter id to encrypt the label pool to
    \param burn_in The number of entries to skip before beginning to encode
    \sa EncData()
  */
  EncData(string file_name, size_t hashor, size_t num_labels,
          size_t num_examples, CKKSEncoder& encoder, size_t scale,
          Encryptor& encryptor, const parms_id_type& data_parms_id,
          const parms_id_type& label_parms_id,
          int burn_in = 0); 

  //! Construct from loaded data
  /*! 
    Given data already loaded in the form of vector vector double, takes
    and encrypts using the other parameters given
    FIXME - not sure what's wrong with it, but this function isn't really in use
    these days
  */
  EncData(vector<vector<double>>& batch, vector<vector<double>>& batch_labels);

  //! Construct EncData from serialized data
  /*! 
    Given entries, labels, and label pool, construct EncData, supplemented by
    data from the json files.

    \param entries 
    \param labels 
    \param label_pool
    \param num_features
    \param num_labels
  */
  EncData(vector<Ciphertext>& entries, vector<int>& labels,
          vector<Ciphertext>& label_pool, size_t num_features,
          size_t num_labels);

  //! Deserialize data from disk and create an EncData instance
  /*! 
    Given streams open to the entries, labels, and label pool, deserialize the
    data into an EncData instance.
      
    \param entry_stream Reference to an ifstream, open to the file containing the 
                        encrypted entries. Make sure this has the ios::binary flag! 
     \param label_stream Reference to an ifstream, open to the file containing the labels 
     \param pool_stream Reference to an ifstream, open to the file containing the label 
                        pool. Make sure this has the ios::binary flag! 
     \param context SEAL context, however it is implemented. Used to verify the current
                    SEAL parameters match with that of the data 
     \param hash_size The hash size of the data. This value can't be inferred from the encrypted data 
     \param num_labels The number of labels. This value isn't neccessarily inferred 
                      from the labels data 
     \throws invalid_argument If the number of entry ciphertexts does not match
                              the number of labels
    \sa EncData::deserialize()
  */
#ifndef SEAL36
  EncData(ifstream& entry_stream, ifstream& label_stream, ifstream& pool_stream,
          const std::shared_ptr<seal::SEALContext>& context, size_t hash_size,
          size_t num_labels);
#else
  EncData(ifstream& entry_stream, ifstream& label_stream, ifstream& pool_stream,
          const SEALContext& context, size_t hash_size, size_t num_labels);
#endif

  size_t get_num_examples() { return this->num_examples; } /*!< Get the number of examples */
  size_t get_num_features() { return this->num_features; } /*!< Get the number of features */
  size_t get_num_labels() { return this->num_labels; }     /*!< Get the number of labels */
  size_t get_num_packed() { return this->num_packed; }     /*!< Get the number of entries packed into a ciphertext */
  string to_string();                                      /*!< Return a string describing the instance */
  void get_entries(vector<Ciphertext>& out) { out = this->entries; } /*!< Get the data as a vector of ciphertexts */
  void get_an_entry(size_t which, Ciphertext& out) {
    out = this->entries.at(which);
  } /*!< Get a particular data ciphertext */
  void get_labels(vector<int>& out) { out = this->labels; } /*!< Get the labels as a vector of integers */
  void get_label_pool(vector<Ciphertext>& out) { out = this->label_pool; } /*!< Get the label pool as a vector of ciphertexts */
  void get_zero_one(Plaintext& out) { out = this->zero_one; } /*!< Get the zero-one plaintext */
  void first_and_last(CKKSEncoder&, Decryptor&, int n = 4); /*!< Print the head of the first and last ciphertext to cout */
  void peekAtFirstChunks(CKKSEncoder&, Decryptor&, int n = 4); /*!< Print the head of every chunk in the first data ciphertext to cout */
  void retrieve_labels(size_t which_ct, vector<int>& chunk_labels); /*!< Get labels corresponding to a particular ciphertext */
  //! Mod-switch the data
  /*!
    Perform a modulus switch to all the data in the instance. Specify the level using a parameter id.

    \param evaluator An evaluator instance
    \param parms_id The parameter id to mod switch to
  */ 
  void mod_switch_to(Evaluator& evaluator, parms_id_type& parms_id); 

  //! Serialize the data instance
  /*!
    Serialize and save the data instance to file. Can choose whether or not to save the label pool; many times, one 
    only needs to save a single label pool.

    The data will be saved in a single file called `file_prefix + ".ct"`. The labels will be called `file_prefix + "_labels.txt"`.
    The label pool, if saved, will be called `file_prefix + "_label_pool.ct"`.

    \param file_prefix The prefix to apply to the data, labels, and label pool.
    \param save_pool Whether or not to save the label pool. Default is true. If you are saving multiple different data
              tiles, you might only need to save the label pool once
    \sa EncData::deserialize()
  */
  void serialize(string file_prefix, bool save_pool = true);

  //! Deserialize data from disk
  /*! 
    Given streams open to the entries, labels, and label pool, deserialize the data into an `EncData` instance.
    
    \param entry_stream reference to an ifstream, open to the file containing the encrypted entries. Make sure this has the 
        ios::binary flag!
    \param label_stream reference to an ifstream, open to the file containing the labels 
    \param pool_stream reference to an ifstream, open to the file containing the label pool. Make sure this has the 
        ios::binary flag! 
    \param context SEAL context, however it is implemented. Used to verify the current SEAL parameters match with that of the data 
    \param hash_size integer, store the hash size for sanity checks. This value can't be inferred from the encrypted data 
    \param num_labels integer, store the number of labels. This value isn't neccessarily inferred from the labels data 
    \throws invalid_argument error if the number of entry ciphertexts does not match the number of labels, or if data or 
        labels is empty
    \sa EncData::serialize()
  */
#ifndef SEAL36
  void deserialize(ifstream&, ifstream&, ifstream&,
                   const std::shared_ptr<seal::SEALContext>&, size_t, size_t);
#else
  void deserialize(ifstream&, ifstream&, ifstream&, const SEALContext& context,
                   size_t, size_t);
#endif
};

//! Load data from file, bypassing `EncData`
/*!
  Depricated. Loads data from file, not into an `EncData` instance. 
*/
void bare_load(const string file_name, const size_t hashor,
               const size_t num_examples, CKKSEncoder& encoder,
               const size_t scale, Encryptor& encryptor,
               const parms_id_type& data_parms_id,
               const parms_id_type& label_parms_id, vector<Ciphertext>& entries,
               vector<int>& labels, vector<Ciphertext>& label_pool,
               int burn_in = 0);

//! Stream insertion for a params_id_type
/*!
  Convenience stream insertion operator for params_id_type. Simply inserts the hash string into the stream.

  \param os Just here because this is how stream insertion overloads work
  \param pid The param id to print
*/
ostream& operator<<(ostream& os, const parms_id_type& pid);

//! Class for storing and working with encrypted weights
/*!
  Stores an encrypted weight layer for training.

  The weight/parameter matrix is stored row-wise in a vector of ciphertexts.
*/
class EncWeightLayer {
 private:
  vector<Ciphertext> weights; /*!< The encrypted parameters. Matrix is encoded row-wise */
  size_t num_rows; /*!< The number of rows in the parameter matrix. That is, the number of ciphertexts in this instance */
  size_t num_cols; /*!< The number of columns in the parameter matrix. That is, the number of slots in the ciphertext 
      that are in this parameter matrix */
  size_t num_packed; /*!< The number of models packed into this instance. Should be a power of 2. */
  string activation; /*!< The activation function in use in this layer. Can be "softmax", "sigmoid", "wide", "tight", "none". */
  Plaintext zero_one; /*!< The zero-one plaintext used for summations in packed ciphertexts. */

 public:
  //! Random constructor for packing
  /*!
    Construct a random weight layer. Multiple parameter matrices are packed into a single instance.

    Values are sampled randomly from either the uniform distribution on [- 1 / sqrt(num_cols), 1 / sqrt(num_cols)]
    (if random == "uniform") or the normal distribution with mean 0 and stdev 1 / sqrt(num_cols) (if random == 
    "normal", default).

    Can pass the activation function in, which will facilitate the propagation process. 

    \param num_rows The number of rows in the parameter/weight matrix
    \param num_cols The number of columns in the parameter/weight matrix
    \param num_packed The number of models to pack into this weight layer instance
    \param scale The scale to encode the weights at
    \param encoder An encoder instance
    \param encryptor An encryptor instance
    \param random The random generator to use. See description. Can be "uniform" or "normal" (default)
    \param random_seed Whether or not to use a true random seed or a fixed one. This is
        probably still bugged; best not to use 'false' unless you test it first (the problem
        would be that every value is identical)
    \param activation The activation function attached to this layer
    \sa random_packed_weight_matrix()
  */
  EncWeightLayer(const size_t num_rows, const size_t num_cols, const size_t num_packed, const size_t scale,
                 CKKSEncoder& encoder, Encryptor& encryptor,
                 const string random = "normal", bool random_seed = true,
                 const string activation = "softmax");

  //! Random constructor
  /*!
    Construct a random weight layer.

    Values are sampled randomly from either the uniform distribution on [- 1 / sqrt(num_cols), 1 / sqrt(num_cols)]
    (if random == "uniform") or the normal distribution with mean 0 and stdev 1 / sqrt(num_cols) (if random == 
    "normal", default).

    Can pass the activation function in, which will facilitate the propagation process. 

    \param num_rows The number of rows in the parameter/weight matrix
    \param num_cols The number of columns in the parameter/weight matrix
    \param scale The scale to encode the weights at
    \param encoder An encoder instance
    \param encryptor An encryptor instance
    \param random The random generator to use. See description. Can be "uniform" or "normal" (default)
    \param random_seed Whether or not to use a true random seed or a fixed one. This is
        probably still bugged; best not to use 'false' unless you test it first (the problem
        would be that every value is identical)
    \param activation The activation function attached to this layer
    \sa random_weight_matrix()
  */
  EncWeightLayer(const size_t num_rows, const size_t num_cols, const size_t scale,
                 CKKSEncoder& encoder, Encryptor& encryptor,
                 const string random = "normal", bool random_seed = true,
                 const string activation = "softmax");
  //! Default constructor
  /*!
    Not implemented
  */
  EncWeightLayer();
  //! Copy constructor
  /*!
    Standard deep copy constructor.

    \param layer The instance to copy
  */
  EncWeightLayer(const EncWeightLayer& layer);

  //! Copy assignment operator
  /*!
    Rule of 3
  */
  EncWeightLayer& operator=(EncWeightLayer el) {
    std::swap(weights, el.weights);
    std::swap(num_rows, el.num_rows);
    std::swap(num_cols, el.num_cols);
    std::swap(num_packed, el.num_packed);
    std::swap(activation, el.activation);
    return *this;
  }

  //! Peek at the weights
  /*!
    Print the upper corner of the weight matrix to cout. 

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param num The number of elements to print. Will print the upper num x num block.
        If 0, prints the whole matrix.
  */
  void print_weights(CKKSEncoder&, Decryptor&, size_t num = 0);
  void set_zero_one(Plaintext& in) { this->zero_one = in; } /*!< Set the zero-one plaintext to a certain plaintext */
  size_t get_num_packed() { return this->num_packed; } /*!< Returns the number of models packed into this instance */
  void set_num_packed(size_t num) { this->num_packed = num; } /*!< Allows you to set the number of models packed into this instance */
  //! Peek at the sub-weights
  /*!
    Print the upper corner of each packed weight matrix to cout. 

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param num_chunks The number of submodels packed into this instance
    \param num The number of elements to print. Will print the upper num x num block.
        If 0, prints the whole matrix.
  */
  void peekAtMatrixChunks(CKKSEncoder& encoder, Decryptor& decryptor,
                          size_t num_chunks = 0, size_t num = 4);
  
  //! Decrypt weightlayer
  /*!
    Decrypt the weights and return as a vector of vectors.

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param weights The decrypted weights for output
  */
  void decrypt(CKKSEncoder& encoder, Decryptor& decryptor,
               vector<vector<double>>& weights);

  //! Propagate past weights
  /*! 
    Propagates the given data ciphertext past this layer. Also performs the
    necessary relinearization. Function automatically applies the correct level
    of packing, if necessary.

    The input is a vector (which may be packed) encrypted as a single ciphertext. 
    After multiplication by the weight matrix, we have L ciphertexts, each containing
    the component-wise product of the data with each row of the matrix. These are summed
    so that each of the L ciphertexts contain the activation in each slot. 

    \param data The data to propagate forward
    \param evaluator An evaluator instance
    \param galois_keys A set of Galois keys for rotation
    \param relin_keys A set of relinearization keys
    \param output The output activations
    \sa multiply_and_relin(), multiply_and_relin_packed()
  */
  void linear_forward(const Ciphertext& data, Evaluator& evaluator, const GaloisKeys& galois_keys,
                      const RelinKeys& relin_keys, vector<Ciphertext>& output) const;  
  // void propagate_forward(EncData&, Evaluator&, vector<Ciphertext>&);

  //! Mod-switch the weight ciphertexts
  /*!
    Performs modulus switching on the weight ciphertexts. Weights will be switched to 
    the provided parms_id. 

    \param evaluator An evaluator instance
    \param parms_id The parms_id to mod switch to
  */
  void mod_switch_to(Evaluator& evaluator, parms_id_type& parms_id);
  
  //! Mod-switch and return a copy of the weight ciphertexts
  /*!
    Performs modulus switching on the weight ciphertexts and returns a copy.
    Weights will be switched to the provided parms_id. Weights attached to
    this instance will not be changed.

    \param evaluator An evaluator instance
    \param parms_id The parms_id to mod switch to
    \param result A deep copy of the mod switched weights
  */
  void mod_switch_to_and_copy(Evaluator& evaluator, parms_id_type& parms_id,
                              vector<Ciphertext>& result) const;
  
  //! Subtract the gradients
  /*!
    Update the weights by subtracting gradients. This function assumes that
    any multiplying by learning rate has already occurred.

    \param evaluator An evaluator instance
    \param gradients The gradients to subtract
  */
  void update_gradients(Evaluator& evaluator, const vector<Ciphertext>& gradients);

  void print_parms_id() const {
    cout << "Weights " << weights.at(0).parms_id() << endl;
  } /*!< Print the parms id of the weights ciphertexts to cout */
  int coeff_modulus_size() { return weights.at(0).coeff_modulus_size(); } 
    /*!< Returns the current coefficient modulus size of the weight ciphertexts. 
         Useful for determining the current level */

  parms_id_type get_parms_id() { return weights.at(0).parms_id(); } 
      /*!< Returns the parms id of the weight ciphertexts */

  //! Apply the momentum before gradient descent
  /*!
    In Nesterov momentum, before forward propagation we "look-forward" by applying
    the momentum to the weights. This implements this look forward step.

    The way momentum is currently set up, all this function needs to do is subtract
    the momentum from the weights.

    \param evaluator An evaluator instance
    \param momentum The momentum to subtract
  */
  void apply_momentum(Evaluator& evaluator, const vector<Ciphertext>& momentum);

  size_t get_num_rows() const { return num_rows; } /*!< Returns the number of rows in the weight matrix */

  //! Perform prediction on encrypted data
  /*!
    Given an `EncData` instance, performs prediction. Returns the number of correct
    predictions.

    \param data The data to perform prediction on
    \param evaluator An evaluator instance
    \param decryptor A decryptor instance
    \param encoder An encoder instance
    \param gal_keys A set of Galois keys
    \param relin_keys A set of relinearization keys
    \returns The number of correct predictions 
  */
  double predict(EncData& data, Evaluator& evaluator, Decryptor& Decryptor, 
                 CKKSEncoder& encoder, const GaloisKeys& gal_keys, 
                 const RelinKeys& relin_keys);

  //! Perform prediction on encrypted data in parallel
  /*!
    Uses a dispatch queue to perform the prediction process in parallel.

    \param data The data to perform prediction on
    \param evaluator An evaluator instance
    \param decryptor A decryptor instance
    \param encoder An encoder instance
    \param gal_keys A set of Galois keys
    \param relin_keys A set of relinearization keys
    \param threads The number of threads to create. Default is 4
    \returns The number of correct predictions 
    \sa EncWeightLayer::piece_of_predict()
  */
  double predict_parallel(EncData& data, Evaluator& evaluator, 
                          Decryptor& decryptor, CKKSEncoder& encoder,
                          const GaloisKeys& gal_keys, 
                          const RelinKeys& relin_keys, size_t threads = 4);
  
  //! Helper function to perform parallel predictions
  /*!

    \param ct The single data instance to predict on
    \param label The label corresponding to data
    \param evaluator An evaluator instance
    \param decryptor A decryptor instance
    \param encoder An encoder instance
    \param gal_keys A set of Galois keys
    \param relin_keys A set of relinearization keys
    \returns Whether or not this prediction was correct
    \sa EncWeightLayer::predict_parallel()
  */
  double piece_of_predict(const Ciphertext& ct, int label, Evaluator& evaluator,
                          Decryptor& decryptor, CKKSEncoder& encoder,
                          const GaloisKeys& gal_keys,
                          const RelinKeys& relin_keys);

  //! Save weights to file
  /*!
    Serialize and save the weights to file. This does not save much metadata;
    in particular, you will need to keep track of if the data is packed or not.

    \param file_name The name of the file to save to
  */
  void save(string file_name);

  //! Load weights from file
  /*!
    Deserialized and load the weights from file. This does not load the metadata;
    you will need to manually set the activation function, `num_packed`, and maybe
    even `num_cols`. 

    Validates the loaded weights against the given SEAL context.

    \param file_name The file to load from 
    \param context The SEAL context to check against. Different between 
        versions of SEAL
  */
#ifndef SEAL36
  void load(string file_name, shared_ptr<seal::SEALContext> context);
#else
  void load(string file_name, const SEALContext& context);
#endif

  //! Sum the parameters in the weight matrix
  /*!
    Computes and returns the sum of all the parameters in the weight matrix. 

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \returns The sum of the parameters
    \sa sum_ciphertexts()
  */
  double sum_weights(CKKSEncoder& encoder, Decryptor& decryptor);
  
  //! Sum the absolute values of parameters in the weight matrix
  /*!
    Computes and returns the absolute value sum of all the parameters
    in the weight matrix. 

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \returns The absolute value sum of the parameters
    \sa abs_val_ciphertexts()
  */
  double abs_val_weights(CKKSEncoder& encoder, Decryptor& decryptor);

  //! Refresh the weight ciphertexts
  /*! 
    Decrypts and reencrypts the weights at the highest level to allow for 
    further calculation. 

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param encryptor An encryptor instance
    \sa EncWeightLayer::refresh_and_rotate()
   */
  void refresh(CKKSEncoder& encoder, Decryptor& decryptor, Encryptor& encryptor);
  
  //! Refresh and rotate the weight ciphertexts left
  /*! 
    Decrypts and reencrypts the weights to allow for further calculation.
    Also applies a rotation of chunks to the left, so that each submodel
    can interact with different packed training data.

    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param encryptor An encryptor instance
    \param num_packed The number of submodels packed in a ciphertext.
    \param rot The number of chunks to rotate to the left. To rotate right, use 
      num_packed - steps_right. Default is 1
    \sa refresh_and_rotate_vector()
  */
  void refresh_and_rotate(CKKSEncoder&, Decryptor&, Encryptor&,
                          size_t num_packed, int rot);
};

//! Refresh and rotate a vector of ciphertexts
/*!
  Refreshes and rotates a vector of ciphertexts. "Refreshing" refers to the act
  of decrypting and reencrypting a ciphertext at the highest level, thus allowing
  a "spent" ciphertext at a low level to be used for further computation. 

  \param to_refresh The ciphertexts to refresh and rotate
  \param encoder An encoder instance
  \param decryptor A decryptor instance
  \param encryptor An encryptor instance
  \param num_packed The number of chunks in the ciphertexts
  \param rot The number of chunks to rotate to the left. To rotate right, use 
      num_packed - steps_right. Default is 1.
*/
void refresh_and_rotate_vector(vector<Ciphertext>& to_refresh,
                               CKKSEncoder& encoder, Decryptor& decryptor,
                               Encryptor& encryptor, size_t num_packed, int rot = 1);

//! Sum the values in a vector of ciphertexts
/*!
  Computes and returns the sum of all the values in a vector of ciphertexts. 

  \param ciphertexts The ciphertext vector to sum
  \param encoder An encoder instance
  \param decryptor A decryptor instance
  \returns The sum of the values
*/
double sum_ciphertexts(vector<Ciphertext>& ciphertexts, CKKSEncoder& encoder,
                       Decryptor& decryptor);

//! Absolute value sum the values in a vector of ciphertexts
/*!
  Computes and returns the absolute value sum of all the values in a vector 
  of ciphertexts. 

  \param ciphertexts The ciphertext vector to sum
  \param encoder An encoder instance
  \param decryptor A decryptor instance
  \returns The absolute value sum of the values
*/
double abs_val_ciphertexts(vector<Ciphertext>& ciphertexts,
                           CKKSEncoder& encoder, Decryptor& decryptor);

//! Class for an encrypted neural net
/*!
  Stores an encrypted neural network, with multiple layers. Since only 
  single layer networks are implemented ( \sa neurallayers ) this is not
  currently usable. If you want to use a multi-layer network, this would
  be a useful class to implement.   
*/
class EncNeuralNet {
 private:
  vector<EncWeightLayer> layers;
  vector<size_t> layer_dims;
  vector<string> activation_functions;
  float learning_rate;
  size_t mini_batch_size;

 public:
  EncNeuralNet(vector<size_t> dims, float learn_rate);
};

//! Perform the softmax approximation given by PrivFT
/*! 
  The PrivFT paper suggests a polynomial to use to approximate softmax. This function
  applies this polynomial to the input, overwriting it. You must supply the encoded 
  coefficients. ( \sa generate_softmax_constants() )
  https://arxiv.org/abs/1908.06972

  \param input The ciphertext to apply the polynomial to, which will be overwritten 
      with the result
  \param constants The encoded coefficients
  \param evaluator An evaluator instance
  \param relin_keys A set of relinearization keys
*/
void softmax_approx_privft_inplace(Ciphertext& input,
                                   vector<Plaintext>& constants,
                                   Evaluator& evaluator,
                                   const RelinKeys& relin_keys);

//! Perform the softmax approximation given by PrivFT
/*! 
  The PrivFT paper suggests a polynomial to use to approximate softmax. This function
  applies this polynomial to the input and returns it as a different ciphertext. You 
  must supply the encoded coefficients. ( \sa generate_softmax_constants() )
  https://arxiv.org/abs/1908.06972

  \param input The ciphertext to apply the polynomial to
  \param constants The encoded coefficients
  \param evaluator An evaluator instance
  \param relin_keys A set of relinearization keys
  \param output The output of the polynomial
*/
void softmax_approx_privft(Ciphertext& input, vector<Plaintext>& constants,
                           Evaluator& evaluator, const RelinKeys& relin_keys,
                           Ciphertext& output);

//! Generate the softmax approximation coefficients given by PrivFT
/*! 
  The PrivFT paper suggests a polynomial to use to approximate softmax. This function
  creates these coefficients that you can then use to evaluate the polynomial.
  https://arxiv.org/abs/1908.06972

  \param scale The scale to set the plaintexts at
  \param encoder An encoder instance
  \param parms_ids The vector of parms_ids. Since you will need multiple different
      parms_ids to correctly orchestrate the evaluation, you must pass them all
  \param level The level at which you expect the input of the polynomial to be at
  \param constants The resulting encoded constants
  \sa softmax_approx_privft_inplace(), softmax_approx_privft()
*/
void generate_softmax_constants(size_t scale, CKKSEncoder& encoder,
                                vector<parms_id_type>& parms_ids, size_t level,
                                vector<Plaintext>& constants);

//! Generate coefficients for use in a homomorphic polynomial approximation
/*!
  Creates encoded coefficients to use to evaluate a homomorphic polynomial approximation.
  The vector of values should be passed in decreasing order of degree, and they
  will be returned in the same order. 

  Since this function is specialized for degree 5 polynomials, values should be of
  size 6.

  \param scale The scale to set the plaintexts at
  \param values The coefficients to encode, in decreasing order of degree
  \param encoder An encoder instance
  \param parms_ids The vector of parms_ids. Since you will need multiple different
      parms_ids to correctly orchestrate the evaluation, you must pass them all
  \param level The level at which you expect the input of the polynomial to be at
  \param constants The resulting encoded constants, in decreasing order of degree
  \sa evaluate_degree_five_polynomial(), evaluate_degree_five_polynomial_inplace()
  \throws invalid_argument if `values` is not size 6
*/
void generate_degree_five_constants(size_t scale, vector<double> values,
                                    CKKSEncoder& encoder,
                                    vector<parms_id_type>& parms_ids,
                                    size_t level, vector<Plaintext>& constants);

//! Evaluate a degree 5 polynomial homomorphically
/*! 
  Evaluates a degree 5 polynomial homomorphically, using the coefficients stored
  in `constants` in decreasing order of degree. The input will be overwritten with
  the result. The polynomial is applied slotwise to the input.
  
  \param input The ciphertext to apply the polynomial to
  \param constants The encoded coefficients, in decreasing order of degree
  \param evaluator An evaluator instance
  \param relin_keys A set of relinearization keys
  \sa generate_degree_five_constants()
*/
void evaluate_degree_five_polynomial_inplace(Ciphertext& input,
                                             vector<Plaintext>& constants,
                                             Evaluator& evaluator,
                                             const RelinKeys& relin_keys);

//! Evaluate a degree 5 polynomial homomorphically
/*! 
  Evaluates a degree 5 polynomial homomorphically, using the coefficients stored
  in `constants` in decreasing order of degree. The polynomial is applied slotwise 
  to the input.
  
  \param input The ciphertext to apply the polynomial to
  \param constants The encoded coefficients, in decreasing order of degree
  \param evaluator An evaluator instance
  \param relin_keys A set of relinearization keys
  \param output The output of the polynomial evaluation
  \sa generate_degree_five_constants()
*/
void evaluate_degree_five_polynomial(Ciphertext& input,
                                     vector<Plaintext>& constants,
                                     Evaluator& evaluator,
                                     const RelinKeys& relin_keys,
                                     Ciphertext& output);

#endif
