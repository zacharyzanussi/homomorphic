#ifndef IO_H
#define IO_H

// standard CPP libraries
#include <array>
#include <cmath>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

// standard C libraries
#include <getopt.h>

// JSON library
#include <nlohmann/json.hpp>

// logger
#include "log.h"

// namespaces
using namespace std;
using json = nlohmann::json;

//!  Struct for long_options getopt
/*!
  Contains all the parameters.
*/
static struct option long_options[] = {
    /*   NAME       ARGUMENT           FLAG  SHORTNAME */
    {"degree", required_argument, 0, 'n'},
    {"training", required_argument, 0, 'T'},
    {"testing", required_argument, 0, 't'},
    {"batchsize", required_argument, 0, 'b'},
    {"tiles", required_argument, 0, 's'},
    {"lrates", required_argument, 0, 'l'},
    {"regularization", required_argument, 0, 'r'},
    {"approximation", required_argument, 0, 'a'},
    {"momentum", required_argument, 0, 'm'},
    {"threads", required_argument, 0, 'd'},
    {"initialweights", required_argument, 0, 'g'},
    {"weightname", required_argument, 0, 'w'},
    {"weightslocation", required_argument, 0, 'e'},
    {"predictweights", required_argument, 0, 'p'},
    {"records", required_argument, 0, 'o'},
    {"trainfile", required_argument, 0, 'i'},
    {"testfile", required_argument, 0, 'f'},
    {"ciphertextlocation", required_argument, 0, 'c'},
    {"ciphertexttrain", required_argument, 0, 'J'},
    {"ciphertexttest", required_argument, 0, 'j'},
    {"epochs", required_argument, 0, 'E'},
    {"num_labels", required_argument, 0, 'M'},
    {"num_levels", required_argument, 0, 'N'},
    {"scale_bits", required_argument, 0, 'S'},
    {"initial_scale", required_argument, 0, 'I'},
    {"help", no_argument, 0, 'h'},
    {0, 0, 0, 0}};

//! Array of 3 doubles
/*! Defines an array of three doubles. */
typedef array<double, 3> array3;

//!  For dealing with JSON objects
/*!
  Base clase for manipulating JSON files.
*/
class JSONHandler {
 public:
  //!  Loads JSON file
  /*!
    Loads the JSON file
    \param filename the string containing the JSON filename
  */
  void LoadJSON(const string filename) {
    filename_ = filename;

    cout << endl << "Loading JSON " << filename_ << endl;

    // open the file
    ifstream file(filename_);
    if (file) {
      try {
        // parse the json into keys values
        json_params_ = json::parse(file);
        // parse for the specific model
        ParseJSON();
      } catch (json::parse_error& e) {
        cout << "message: " << e.what() << endl
             << "exception id: " << e.id << endl
             << "byte position of error: " << e.byte << endl;
        exit(-1);
      }
    } else {
      cout << endl << "Can't open file " << filename_ << endl << endl;
      exit(-1);
    }
  }

  //! Print
  /*! A convenient Print function must be defined. */
  virtual void Print() const = 0;

 protected:
  string filename_;  /*!< Stores the json filename */
  json json_params_; /*!< json object */

  //! Parse the JSON object
  /*! The Parse function must be defined according to the model. */
  virtual void ParseJSON() = 0;

  //! Parse a field
  /*! Takes a value from field/key (if exists) of the JSON object */
  template <typename T>
  void ParseField(const string field, T& var) {
    if (json_params_.contains(field)) {
      json_params_.at(field).get_to(var);
    }
  }
};

//!  Model's Parameters
/*!
  Stores the model's parameters
*/
class Parameters : public JSONHandler {
 public:
  //! Print
  /*! Print the model's parameters. */
  void Print() const {
    cout << endl;
    cout << endl << "\tPolynomial degree: " << n;
    cout << endl << "\tPolynomial modulus degree: " << poly_modulus_degree;
    cout << endl << "\tNumber of levels: " << num_levels;
    cout << endl << "\tScale bits: " << scale_bits;
    cout << endl << "\tInitial Scale: " << initial_scale;
    cout << endl << "\tScale: " << scale;
    const auto pchain = [](vector<int> vc) -> void {
      cout << endl << "\tSwitching chain: ";
      for (const auto c : vc) cout << c << " , ";
    };
    pchain(chain);
    cout << endl << "\tNumber of epochs: " << epochs;
    cout << endl << "\tTraining samples: " << num_examples;
    cout << endl << "\tTesting samples: " << num_test_examples;
    cout << endl << "\tBatch size: " << batch_size;
    cout << endl << "\tMaximum tile size: " << max_tile_size;
    cout << endl << "\tNumber of labels: " << num_labels;
    cout << endl << "\tNumber of features: " << num_features;
    cout << endl << "\tHash size: " << hash_size;
    const auto plr = [](array3 lr) -> void {
      cout << endl << "\tLearning rates: ";
      for (const auto l : lr) cout << l << " , ";
    };
    plr(learning_rate);
    cout << endl << "\tRegularization: " << regularization;
    cout << endl << "\tApproximation: " << approximation;
    cout << endl << "\tMomentum coefficient: " << momentum_coeff;
    cout << endl << "\tNumber of threads: " << num_threads;
    cout << endl << "\tFiles in: " << files;
    cout << endl << endl;
  }

  //! Updates scale and chain
  /*! It also calculates the poly_modulus_degree*/
  void UpdateScaleAndChain() {
    poly_modulus_degree = pow(2, n);
    chain.push_back(initial_scale);

    for (int i = 0; i < num_levels; i++) {
      chain.push_back(scale_bits);
    }
    chain.push_back(initial_scale);
    scale = pow(2.0, scale_bits);
  }

  size_t n;                   /*!< Polynomial degree */
  size_t poly_modulus_degree; /*!< Polynomial modulus degree */
  size_t epochs;              /*!< Number of epochs */
  size_t num_examples;        /*!< Number of training samples */
  size_t num_test_examples;   /*!< Number of testing samples */
  size_t batch_size;          /*!< Batch size */
  size_t max_tile_size;       /*!< Maximum tile size */
  int num_labels;             /*!< Number of labels */
  size_t num_features;        /*!< Number of features */
  size_t hash_size;           /*!< Hash size */
  array3 learning_rate;       /*!< Learning rates */
  double regularization;      /*!< Regularization */
  string approximation;       /*!< Aproximation name */
  double momentum_coeff;      /*!< Momentum coefficient*/
  size_t num_threads;         /*!< Number of threads*/
  string files;               /*!< JSON with filenames */
  vector<int> chain;          /*!< Switching chain */
  int num_levels;             /*!< Number of levels */
  int scale_bits;             /*!< Scale bits */
  int initial_scale;          /*!< Initial scale */
  double scale;               /*!< Scale */

 private:
  //! Parses the JSON and gets the parameters
  /*! \sa UpdateAndChain */
  void ParseJSON() {
    ParseField("Polynomial degree", this->n);
    ParseField("Epochs", this->epochs);
    ParseField("Training samples", this->num_examples);
    ParseField("Testing samples", this->num_test_examples);
    ParseField("Batch size", this->batch_size);
    ParseField("Maximum tile size", this->max_tile_size);
    ParseField("Number of labels", this->num_labels);
    ParseField("Number of features", this->num_features);
    ParseField("Number of levels", this->num_levels);
    ParseField("Scale bits", this->scale_bits);
    ParseField("Initial scale", this->initial_scale);
    ParseField("Hash size", this->hash_size);
    ParseField("Learning rates", this->learning_rate);
    ParseField("Regularization", this->regularization);
    ParseField("Approximation", this->approximation);
    ParseField("Momentum coefficient", this->momentum_coeff);
    ParseField("Number of threads", this->num_threads);
    ParseField("Files", this->files);
    UpdateScaleAndChain();
  }
};

//!  Files
/*!
  Specifies the files from json file
*/
class Files : public JSONHandler {
 public:
  //! Print
  /*! Print the input/output files. */
  void Print() const {
    cout << endl;
    cout << endl << "\tTrain file: " << train_file;
    cout << endl << "\tTest file: " << test_file;
    cout << endl << "\tInitial weight name: " << initial_weight_name;
    cout << endl << "\tWeights location: " << weights_location;
    cout << endl << "\tWeight name: " << weight_name;
    cout << endl << "\tPredict weight name: " << predict_weight_name;
    cout << endl << "\tRecords: " << records_file;
    cout << endl << "\tCiphertext Location: " << ciphertext_location;
    cout << endl << "\tCiphertext train name: " << ciphertext_train_name;
    cout << endl << "\tCiphertext test name: " << ciphertext_test_name;
    cout << endl << endl;
  }

  string initial_weight_name;   /*!< Initial weight name */
  string weight_name;           /*!< Weight name */
  string weights_location;      /*!< Weights location */
  string predict_weight_name;   /*!< Predict weight name */
  string train_file;            /*!< Train filename */
  string test_file;             /*!< Test filename */
  string records_file;          /*!< Records filename */
  string ciphertext_location;   /*!< Ciphertext location */
  string ciphertext_train_name; /*!< Ciphertext train name */
  string ciphertext_test_name;  /*!< Ciphertext test name */

 private:
  //! Parse the JSON and gets the file names
  void ParseJSON() {
    ParseField("Train file", this->train_file);
    ParseField("Test file", this->test_file);
    ParseField("Initial weights name", this->initial_weight_name);
    ParseField("Weights name", this->weight_name);
    ParseField("Weights location", this->weights_location);
    ParseField("Predict weights name", this->predict_weight_name);
    ParseField("Records file", this->records_file);
    ParseField("Ciphertext location", this->ciphertext_location);
    ParseField("Ciphertext train name", this->ciphertext_train_name);
    ParseField("Ciphertext test name", this->ciphertext_test_name);
  }
};

//! Gets value for an option
/*! Template definition
    \param char_value the char* value from command line
    \param value a reference to store the converted value
*/
template <typename T>
void GetValueOption(const char* char_value, T& value);

//! Gets value for an option of type size_t
/*! Template definition
    \param char_value the char* value from command line
    \param value a reference to store the converted value
*/
template <>
void GetValueOption(const char* char_value, size_t& value) {
  value = static_cast<size_t>(stoi(char_value));
}

template <>
void GetValueOption(const char* char_value, int& value) {
  value = static_cast<int>(stoi(char_value));
}

template <>
void GetValueOption(const char* char_value, double& value) {
  value = stod(char_value);
}

template <>
void GetValueOption(const char* char_value, string& value) {
  value = string(char_value);
}

//! Gets value for an option of type array of 3 doubles
/*! Template definition
    \param char_value the char* value from command line
    \param values a reference to store the array
*/
template <>
void GetValueOption(const char* char_value, array3& values) {
  stringstream ss(char_value);
  string value;
  int i = 0;
  while (getline(ss, value, ',')) {
    values[i] = stod(value);
    ++i;
  }
}

//!  Argument option parser
/*!
  Parses and store parameters from command line arguments
*/
class PSOptions {
 public:
  //! Constructor of PSOPtions
  /*!
    \param params a reference of a Parameters object, it
           will contain the parameters of the model.
    \param files a reference of a Files object, it will
           contain the files for I/O
    \param argc integer, number of argument options.
    \param argv pointer of pointer of char with the options.
  */
  PSOptions(Parameters& params, Files& files, int argc, char** argv)
      : params_(params), files_(files), argc_(argc), argv_(argv) {
    Parse();
  }

  string filename_; /*!< Input JSON filename */

 private:
  //!  Parses the command line options
  /*!
    Parses and store parameters from command line arguments
  */
  void Parse() {
    int c;
    int option_index = 0;
    while (
        (c = getopt_long(argc_, argv_, "n:T:t:b:s:l:r:a:m:d:g:w:e:p:o:i:f:c:N:S:I:h",
                         long_options, &option_index)) != -1) {
      switch (c) {
        case 'n':
          GetValueOption(optarg, params_.n);
          params_.UpdateScaleAndChain();
          break;
        case 'T':
          GetValueOption(optarg, params_.num_examples);
          break;
        case 't':
          GetValueOption(optarg, params_.num_test_examples);
          break;
        case 'b':
          GetValueOption(optarg, params_.batch_size);
          break;
        case 's':
          GetValueOption(optarg, params_.max_tile_size);
          break;
        case 'l':
          GetValueOption(optarg, params_.learning_rate);
          break;
        case 'r':
          GetValueOption(optarg, params_.regularization);
          break;
        case 'a':
          GetValueOption(optarg, params_.approximation);
          break;
        case 'm':
          GetValueOption(optarg, params_.momentum_coeff);
          break;
        case 'd':
          GetValueOption(optarg, params_.num_threads);
          break;
        case 'E':
          GetValueOption(optarg, params_.epochs);
          break;
        case 'M':
          GetValueOption(optarg, params_.num_labels);
          break;
        case 'N':
          GetValueOption(optarg, params_.num_levels);
          break;
        case 'S':
          GetValueOption(optarg, params_.scale_bits);
          break;
        case 'I':
          GetValueOption(optarg, params_.initial_scale);
          break;
        case 'g':
          GetValueOption(optarg, files_.initial_weight_name);
          break;
        case 'w':
          GetValueOption(optarg, files_.weight_name);
          break;
        case 'e':
          GetValueOption(optarg, files_.weights_location);
          break;
        case 'p':
          GetValueOption(optarg, files_.predict_weight_name);
          break;
        case 'o':
          GetValueOption(optarg, files_.records_file);
          break;
        case 'i':
          GetValueOption(optarg, files_.train_file);
          break;
        case 'f':
          GetValueOption(optarg, files_.test_file);
          break;
        case 'c':
          GetValueOption(optarg, files_.ciphertext_location);
          break;
        case 'J':
          GetValueOption(optarg, files_.ciphertext_train_name);
          break;
        case 'j':
          GetValueOption(optarg, files_.ciphertext_test_name);
          break;
        case 'h':
          Help();
          break;
        case '?':
          break;
        default:
          cout << "?? getopt returned character code 0 " << c << endl;
      }
    }
    // In case we have non option arguments, we take only
    // the first one as a our JSON file for parsing
    if (optind < argc_) {
      filename_ = argv_[optind];
      cout << optind << endl;
      WithJSON();
      optind++;  // Skip all the next non option arguments
    }
  };

  //!  Parses the command line options
  /*!
    Parses and store parameters from command line arguments
  */
  void WithJSON() {
    params_.LoadJSON(filename_);
    // Check if we have a json for i/o files specified
    // in the JSON parameters file
    if (!params_.files.empty()) {
      files_.LoadJSON(params_.files);
    }
  }

  //!  Print options in shell
  /*!
    Print the options automatically
    FIXME
  */
  void Help() const {
    cout << endl << " Usage:  " << argv_[0] << " [OPTIONS] [JSON FILE]";
    cout << endl << "  Runs a network encrypted test";
    for (const auto opt : long_options) {
      if (opt.name != 0) {
        cout << endl
             << "\t--" << opt.name << "\t\t-" << char(opt.val)
             << (opt.has_arg == 1 ? " value(required)" : "");
      }
    }
    cout << endl << endl << endl;
    exit(0);
  }

  Parameters& params_; /*!< Reference to a Parameters object */
  Files& files_;       /*!< Reference to a Files object */
  int argc_;           /*!< The number of command line arguments */
  char** argv_;        /*!< Contains each argument */
};

#ifdef TESTING
#include <boost/timer/timer.hpp>
using boost::timer::auto_cpu_timer;
using boost::timer::cpu_timer;
using boost::timer::cpu_times;

class MyTimer {
 public:
  MyTimer(const string& description = "") : description_(description) {
    ResetTimer();
  };

  MyTimer(const stringstream& ss) : description_(ss.str()) { ResetTimer(); };

  void ResetTimer() { start_time_.clear(); }

  void StartTimer() {
    cpu_.start();
    start_time_ = cpu_.elapsed();
  };

  void StopTimer() {
    current_time_ = cpu_.elapsed();
    TIMER << description_;
    TIMER << " - Elapsed CPU time: "
          << (current_time_.user - start_time_.user) * 1e-9;

    TIMER << " - Elapsed Wall time: "
          << (current_time_.wall - start_time_.wall) * 1e-9;
  };

 private:
  string description_;
  cpu_times start_time_;
  cpu_times current_time_;
  cpu_timer cpu_;
  cpu_timer cpu_wall_;
};
#endif  // TESTING

// define some macros for timing
#ifdef TESTING
#define timer(t, s) MyTimer t(s)
#define tstart(t) t.StartTimer()
#define tstop(t) t.StopTimer()
#define treset(t) t.ResetTimer()
#else  // TESTING
#define timer(t, s)
#define tstart(t)
#define tstop(t)
#define treset(t)
#endif  // TESTING

#endif  // IO_H