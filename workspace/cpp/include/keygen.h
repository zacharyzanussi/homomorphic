#ifndef KEYGEN_H
#define KEYGEN_H

// standard CPP libraries
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

// seal library
#include "seal/seal.h"

// namespaces
using namespace std;
using namespace seal;

//!  Key Generator class
/*!
  Base clase for manipulating public and secret keys.
  \extends KeyGenerator
*/
class KeyGen : public KeyGenerator {
 public:
  //! Instantiate the KeyGen with SEALContext and path
  /*!
    \param context a const reference to a seal context.
    \param prefix the path where keys are stored or read.
  */
  KeyGen(const SEALContext &context, string prefix = "")
      : KeyGenerator(context), prefix_(prefix), context_(context) {
    set_path();
  }

  //! Instantiate the KeyGen with SEALContext, secret key and path
  /*!
    \param context a const reference to a seal context.
    \param secret_key a const reference to the secret key.
    \param prefix the path which stores or reads the keys.
  */
  KeyGen(const SEALContext &context, const SecretKey &secret_key,
         string prefix = "")
      : KeyGenerator(context, secret_key_),
        prefix_(prefix),
        context_(context),
        secret_key_(secret_key) {
    sk_generated_ = true;
    set_path();
  }

  // We delete some constructors by default, following SEAL
  KeyGen(void) = delete;
  KeyGen(const KeyGen &copy) = delete;
  KeyGen &operator=(const KeyGen &assign) = delete;
  KeyGen(KeyGen &&source) = delete;
  KeyGen &operator=(KeyGen &&assign) = delete;

  //! Serialize keys and store them in prefix path
  /*!
    \param
    TODO FIXME the deserialized keys are not updated to the members
    of this object #public_key_ #relin_keys_ #gal_keys_
  */
  void Serialize(bool with_rks = false, bool with_gks = false) {
    if (!sk_generated_) {
      CreateSecretKey();
    }

    // We create the streams
    ofstream public_key_stream(public_filename_, ios::binary);
    ofstream secret_key_stream(secret_filename_, ios::binary);

    // Serialization and saves the keys (compression zstd for seal v3.6)
#ifdef SEAL36
    Serializable<PublicKey> public_key_ = KeyGenerator::create_public_key();
    public_key_.save(public_key_stream, compr_mode_type::zstd);
    secret_key_.save(secret_key_stream, compr_mode_type::zstd);
#else
    auto public_key = KeyGenerator::public_key();
    public_key_.save(public_key_stream);
    secret_key_.save(secret_key_stream);
#endif

    // closes the streams
    secret_key_stream.close();
    public_key_stream.close();

    // saves relin_keys
    if (with_rks) {
      // We create the stream
      ofstream relin_keys_stream(relin_filename_, ios::binary);
#ifdef SEAL36
      Serializable<RelinKeys> relin_keys_ = KeyGenerator::create_relin_keys();
      relin_keys_.save(relin_keys_stream, compr_mode_type::zstd);
#else
      Serializable<RelinKeys> relin_keys_ = KeyGenerator::relin_keys();
      relin_keys_.save(relin_keys_stream);
#endif
      relin_keys_stream.close();
    }

    // saves Galois keys
    if (with_gks) {
      // We create the stream
      ofstream gal_keys_stream(gal_filename_, ios::binary);
#ifdef SEAL36
      Serializable<GaloisKeys> gal_keys_ = KeyGenerator::create_galois_keys();
      gal_keys_.save(gal_keys_stream, compr_mode_type::zstd);
#else
      Serializable<GaloisKeys> gal_keys_ = KeyGenerator::galois_keys();
      gal_keys_.save(gal_keys_stream);
#endif
      gal_keys_stream.close();
    }
  }

  //! Load and DeSerialize secret keys
  /*!
   */
  void DeSerializeSecret() {
    // Open the stream
    ifstream secret_key_stream(secret_filename_, ios::binary);

    // load secret key from file WARNING FIXME TODO validation
#ifdef SEAL36
    secret_key_.load(context_, secret_key_stream);
    sk_generated_ = true;
#else
    // WARNING TODO FIXME check compatibility
    /*public_key.save(public_key_stream);
    secret_key.save(secret_key_stream);
    relin_keys.save(relin_keys_stream);
    gal_keys.save(gal_keys_stream);*/
#endif

    // close the stream
    secret_key_stream.close();
  }

  //! Load and DeSerialize secret keys
  /*!
      \param with_rks if true loads and deserializes relin_keys
      \param with_gks if true loads deserializes gal_keys
   */
  void DeSerialize(bool with_rks = false, bool with_gks = false) {
    // Deserialize secret
    DeSerializeSecret();
    // Deserialize public keys
    DeSerializePublic(with_rks, with_gks);
  }

  //! Load and DeSerialize public keys
  /*!
      \param with_rks if true loads and deserializes relin_keys
      \param with_gks if true loads deserializes gal_keys
   */
  void DeSerializePublic(bool with_rks = false, bool with_gks = false) {
    // Open the stream
    ifstream public_key_stream(public_filename_, ios::binary);
    // load keys from files
#ifdef SEAL36
    public_key_.load(context_, public_key_stream);
#else
    // WARNING TODO FIXME check compatibility
#endif
    // close the stream
    public_key_stream.close();

    if (with_rks) {
      // Open the stream
      cout << "Loading from " << relin_filename_ << endl;
      ifstream relin_keys_stream(relin_filename_, ios::binary);
      // load keys from files
#ifdef SEAL36
      relin_keys_.load(context_, relin_keys_stream);
#else
      // WARNING TODO FIXME check compatibility
#endif
      // close the stream
      relin_keys_stream.close();
    }

    if (with_gks) {
      // Open the stream
      cout << "Loading from " << gal_filename_ << endl;
      ifstream gal_keys_stream(gal_filename_, ios::binary);
      // load keys from files
#ifdef SEAL36
      gal_keys_.load(context_, gal_keys_stream);
#else
      // WARNING TODO FIXME check compatibility
#endif
      // close the streams
      gal_keys_stream.close();
    }
  }

  //! Create public keys - no serialization
  /*!
   */
  void CreatePublicKeys(bool with_public = false) {
    // if no secret key we create a new one
    if (!sk_generated_) {
      CreateSecretKey();
    }
#ifdef SEAL36
    KeyGenerator::create_relin_keys(relin_keys_);
    KeyGenerator::create_galois_keys(gal_keys_);
#else
    relin_keys_ = KeyGenerator::relin_keys_local();
    gal_keys_ = KeyGenerator::galois_keys_local();
#endif

    if (with_public) {
#ifdef SEAL36
      KeyGenerator::create_public_key(public_key_);
#else
      public_key_ = KeyGenerator::public_key();
#endif
    }
  }

  //! Create secret key - no serialization
  /*!
   */
  void CreateSecretKey() {
    secret_key_ = KeyGenerator::secret_key();
    sk_generated_ = true;
  }

  PublicKey public_key_; /*!< Stores the SEAL context */
  RelinKeys relin_keys_; /*!< Stores the relineatization keys */
  GaloisKeys gal_keys_;  /*!< Stores the Galois keys */

  string prefix_;          /*!< Stores the location of files */
  string public_filename_; /*!< Stores the public key path + filename */
  string secret_filename_; /*!< Stores the secret key path + filename */
  string relin_filename_;  /*!< Stores the relin key path + filename */
  string gal_filename_;    /*!< Stores the Galois key path + filename */

  const SecretKey &secret_key() const {
    if (!sk_generated_) {
      cout << "secret key has not been generated";
      std::terminate();
    }
    return secret_key_;
  }

  //! Create all keys in memory - no serialization
  /*!
   */
  void CreateKeys() {
    CreateSecretKey();
    CreatePublicKeys(true);
  }

 private:
  //! Adds the prefix to hardcoded file names for keys
  /*!
   */
  void set_path() {
    // hardcoded names FIXME TODO
    public_filename_ = prefix_ + "public.key";
    secret_filename_ = prefix_ + "secret.key";
    relin_filename_ = prefix_ + "relin.key";
    gal_filename_ = prefix_ + "gal.key";
  }

  SEALContext context_;       /*!< Stores the SEAL context */
  SecretKey secret_key_;      /*!< Stores the secret key */
  bool sk_generated_ = false; /*!< Sets if the secret key was generated */
};

#endif  // KEYGEN_H