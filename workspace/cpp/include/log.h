#ifndef LOG_H
#define LOG_H

#include <iostream>

// avoid clash between placeholders stl and boost v1.67
#define BOOST_BIND_NO_PLACEHOLDERS

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/thread/thread.hpp>
#include <exception>
#include <iostream>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;
namespace sinks = boost::log::sinks;
// using namespace logging::trivial;

// We define our own severity levels
enum severity_level { debug, info, timer, results, warning, error };

// https://www.boost.org/doc/libs/1_75_0/libs/log/example/doc/expressions_attr_fmt_tag.cpp
// The operator is used for regular stream formatting
std::ostream& operator<<(std::ostream& strm, severity_level level) {
  static const char* strings[] = {"debug",   "info",    "timer",
                                  "results", "warning", "error"};

  if (static_cast<std::size_t>(level) < sizeof(strings) / sizeof(*strings))
    strm << strings[level];
  else
    strm << static_cast<int>(level);

  return strm;
}

// Attribute value tag type
struct severity_tag;

// The operator is used when putting the severity level to log
logging::formatting_ostream& operator<<(
    logging::formatting_ostream& strm,
    logging::to_log_manip<severity_level, severity_tag> const& manip) {
  static const char* strings[] = {"debug",   "info",    "timer",
                                  "results", "warning", "error"};

  severity_level level = manip.get();
  if (static_cast<std::size_t>(level) < sizeof(strings) / sizeof(*strings))
    strm << strings[level];
  else
    strm << static_cast<int>(level);

  return strm;
}

namespace clog {
inline void init(std::string logname) {
  auto sink = logging::add_file_log(
      keywords::file_name = logname + ".log",
      keywords::auto_flush = true,  // Write after each call
      // This makes the sink to write log records that look like this:
      // YYYY-MM-DD HH:MI:SS: [info] A info severity message, etc
      keywords::format =
          (expr::stream << expr::format_date_time<boost::posix_time::ptime>(
                               "TimeStamp", "%Y-%m-%d %H:%M:%S")
                        << ": ["
                        << expr::attr<severity_level, severity_tag>("Severity")
                        << "] " << expr::smessage));
}

typedef src::severity_logger_mt<severity_level> logger_t;
}  // namespace clog

// initiate global logger
BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(log, clog::logger_t)

// some useful macros to do the logging
#define DBG BOOST_LOG_SEV(log::get(), debug)
#define INFO BOOST_LOG_SEV(log::get(), info)
#define TIMER BOOST_LOG_SEV(log::get(), timer)
#define RES BOOST_LOG_SEV(log::get(), results)
#define WARN BOOST_LOG_SEV(log::get(), warning)
#define ERR BOOST_LOG_SEV(log::get(), error)

#endif
