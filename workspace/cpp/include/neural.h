#ifndef NEURAL_H
#define NEURAL_H

#include <iostream>
//#include <vector>
//#include <string>
#include "utility.h"

using namespace std;


//! Class that stores data
/*!
  This class stores data to be used for training or testing. 
*/
class Data {
 private:
  vector<vector<double>> entries; /*!< the encoded data entries */
  vector<vector<double>> labels;  /*!< the encoded labels */
  size_t num_examples;            /*!< the number of examples in this `Data` instance; ie entries.size() */
  size_t num_features;            /*!< the number of input features; ie entries.at(0).size() */
  size_t num_labels;              /*!< the number of output labels; ie labels.at(0).size() */
  vector<string> descriptions;    /*!< the input strings, unencoded. */

 public:
  //! Load pre-encoded data from file
  /*!
    A specialized function for loading our synthetic scanner data as a
    `Data` instance. Data should have the following form:
    label,description,3gram,4gram,5gram,6gram
    
    The list of *grams should be integers separated by spaces. Only one actually
    needs to be filled in; the rest can be empty, but you should have the commas still.
    \param file_name name of the file that holds the data
    \param num_features number of input features. If hashor is nonzero, then this entry
      is all but ignored
    \param num_labels number of output labels, or classes
    \param num_examples number of entries to load in. 
    \param hashor size that the entries should be hashed down to. If this value is 0, then the 
      hashing vectorizor will not be used. 
    \param desc boolean, if `true` then we will load all the cleartext descriptions as well.
      Useful for diagnosing performance issues. 
  */
  Data(string file_name, size_t num_features, size_t num_labels,
       size_t num_examples, size_t hashor = 0,
       bool desc = false);  // Initialize from file
  //! Load pre-encoded data into a `Data` instance
  /*!
    With this, you can load any pre-encoded data. Other data members of the `Data` instance
    will be inferred from the input.
    \param batch a batch of data, encoded so that it's a vector of data vectors
    \param batch_labels the associated labels, encoded so that it's a vector of one-hot vectors
  */ 
  Data(vector<vector<double>>& batch, vector<vector<double>>& batch_labels);
  //! Default constructor (empty)
  /*!
    Default constructor is not defined.
  */
  Data();
  // One-hot encode and initialize??

  size_t get_num_examples() { return num_examples; } /*!< Returns the member num_examples */
  size_t get_num_features() { return num_features; } /*!< Returns the member num_features */
  size_t get_num_labels() { return num_labels; }     /*!< Returns the member num_labels */
  //! Retrieve a minibatch for training
  /*! 
    Retrieves a minibatch as a separate `Data` instance for use in training functions or otherwise.
    One problem with this implementation is that it duplicates data, rather than simply referencing
    it elsewhere (I think...)

    \param start_from value to start taking data from
    \param batch_size number of entries to take in batch
    \param batch `Data` instance that will be overwritten with the new batch
    \todo fix this so that it never runs off the end of the vector
  */
  void get_mini_batch(
      size_t start_from, size_t batch_size,
      Data& batch);
  void get_data(vector<vector<double>>& data) { data = this->entries; } /*!< Returns the data as a vector of vector of doubles */
  void get_labels(vector<vector<double>>& labels) { labels = this->labels; } /*!< Returns the labels as a vector of vector of doubles */
  //! Get the ith entry in the dataset
  /*!
    Returns the data related to the ith entry in the dataset.

    \param i which entry to retrieve
    \param entry the data corresponding to the ith entry
    \param label the label corresponding to the ith entry
    \param desc the desc corresponding to the ith entry
    \throws error if i is larger than the size of the data
  */
  void get_ith(size_t i, vector<double>& entry, vector<double>& label,
               string& desc) {
    entry = this->entries.at(i);
    label = this->labels.at(i);
    desc = this->descriptions.at(i);
  }
};

// class Caches {
//  private:
//   vector<vector<vector<double>>> caches;
//   size_t num_layers;
//   vector<size_t> layer_dimensions;

//  public:
// };


//! Class for a layer of weights
/*!
  This class encodes a layer of weights and it's activation function. A neural network
  consists of one or more layers of weights
*/
class WeightLayer {
  friend class NeuralNet;

 private:
  //! The weights/parameter matrix
  /*
    The weight matrix, encoded row wise, so that weights.size() == num_rows
    and weights.at(0).size() == num_cols. 
  */
  vector<vector<double>> weights; 
  size_t num_rows; /*!< The number of rows in the weight matrix */
  size_t num_cols; /*!< The number of columns in the weight matrix */
  string activation; /*!< The name of the activation function on this layer. 
                          Options are "sigmoid", "softmax", and "none". Default is "none". */

 public:
  //! Construct a random weight matrix
  /*!
    Constructor to create a new random weight matrix. 

    \param num_rows The number of rows in the generated matrix
    \param num_cols The number of columns in the generated matrix.
    \param random The method with which to generate the random values. Options
    are "normal", which will draw parameters from the normal distribution centred on
    0 with std dev 1 / sqrt(num_cols), and "uniform", which will draw parameters uniformly
    from [ -1 / sqrt(num_cols), 1 / sqrt(num_cols) ]. Default is "normal".
    \param activation The activation function to apply to this layer. 
    Options are "sigmoid", "softmax", "wide", "tight", and "none". Default is "none".
    \sa random_weight_matrix()
  */
  WeightLayer(int num_rows, int num_cols, string random = "normal",
              string activation = "none");  // Initialize with random values
  //! Construct a `WeightLayer` instance from a matrix
  /*!
    If you already have a weight matrix, use this to convert it to a 
    `WeightLayer` instance. Useful for loading a matrix from file or
    trying out a new initialization. Data members will be initialized based
    on the weights given, and activation will be set to "none" by default.

    This function will do no error checking to make sure you've given it a 
    rectangular array, so you make sure your rows are all the same length!
    \param weights The weight matrix to load in. 
  */
  WeightLayer(vector<vector<double>>& weights);

  
  void print_weights(size_t num = 0) { print_matrix(this->weights, num); } /*!< Print a corner of the matrix to cout */
  size_t get_num_rows() { return this->num_rows; } /*!< Returns the data member num_rows */
  size_t get_num_cols() { return this->num_cols; } /*!< Returns the data member num_cols */
  //! Update the weights with gradients
  /*! 
    Given the gradients as a `WeightLayer` instance, updates the weight matrix by 
    subtracting learning_rate times the gradients. Can also apply regularization.

    \param gradients The computed gradients, as a `WeightLayer` instance
    \param learning_rate The value to use for the learning rate
    \param regularization The coefficient for regularization to apply
    \sa update_gradients
    \throws invalid_argument if gradient's shape does not match that of weights
  */
  void update_weights(WeightLayer& gradients, double learning_rate,
                      double regularization);
  //! Update the weights with gradients
  /*! 
    Given the gradients as a `WeightLayer` instance, updates the weight matrix by 
    subtracting `learning_rate` times the gradients. Can also apply regularization.

    Does not check the shape of `gradients`, so be careful.

    \param gradients The computed gradients, as a vector of vectors
    \param learning_rate The value to use for the learning rate
    \param regularization The coefficient for regularization to apply
    \sa update_gradients
  */
  void update_weights(vector<vector<double>>& gradients, double learning_rate,
                      double regularization);

  //! Apply the activation function to an arbitary vector
  /*!
    Applies the activation function selected by this data instance to the vector input.
    Stores the output in output.

    \param input The vector of values to apply the activation to 
    \param output The output of the activation function
    \throws invalid_argument if activation function is not implemented
  */
  void apply_activation(const vector<double>& input, vector<double>& output);
  //! Propagate data past layer
  /*!
    Propagates a batch of data past this layer, applying activation function.

    \param data_in data to propagate past the layer
    \param data_out data post-propagation
    \throws invalid_argument if it catches an error from matrix_vector_product
    \sa matrix_vector_product()
  */
  void propagate_past(vector<vector<double>>& data_in,
                      vector<vector<double>>& data_out);
  //! Propagate a single data vector past
  /*!
    Propagate a single data entry past the linear part of the layer. Does NOT
    apply the activation function. Used in `predict_report`.

    \param data_in the vector to propagate past
    \param data_out the logits post-propagation
  */
  void propagate_past(vector<double>& data_in, vector<double>& data_out);
  //! Propagate a `Data` instance past layer
  /*! 
    Propagates a batch of data past this layer, applying activation function.

    \param data_in data to propagate past the layer
    \param data_out data post-propagation
  */
  void propagate_past(Data& data_in, vector<vector<double>>& activations);
  //! Propagate backward past the layer
  /*!
    Propagates data backward past this layer. 

    If back prop is (a - y) * x, then a is `activation`, y is `current_layer`, and
    x is `previous_layer`. 
    
    Currently only works for single layer networks.

    \param activation The activations to be propagated backward
    \param current_layer The current values of the current layer
    \param previous_layer The current values of the previous layer
    \param gradients The output for the computed gradients
    \sa compute_gradients(), softmax_backward_single()
  */
  void propagate_past_back(vector<vector<double>>& activation,
                           vector<vector<double>>& current_layer,
                           vector<vector<double>>& previous_layer,
                           vector<vector<double>>& gradients);

  //! Checks if the weights lie outside a range
  /*!
    Counts the number of weights that lie outside the range [min, max]. Also
    finds the larges and smallest values. Prints the values to cout.

    \param min minimum value
    \param max maximum value
  */
  void inspect_weights(double min, double max);
  
  double sum_weights();            /*!< Returns the sum of the weights */
  double abs_val_weights();        /*!< Returns the sum of the absolute value of the weights */
};

//! A class for a neural network
/*!
  This class implements a neural network, which consists of one or more layers of 
  weights with activations.

  \todo make this work for multiple layer networks
*/
class NeuralNet {
 private:
  vector<WeightLayer> layers;           /*!< The layers of the network */
  /*! The dimensions of the layers. If `layer_dims` has the form [n_1, n_2, n_3],
  then there are two layers and layers.at(0) is n_2 x n_1, layers.at(1) is n_3 x n_2
  */
  vector<size_t> layer_dims;            
  vector<string> activation_functions;  /*!< The activation functions of each layer */
  double learning_rate;                 /*!< The learning rate to apply each update */
  size_t mini_batch_size;               /*!< The size of each batch */
  double regularization;                /*!< The coefficient to use for regularization */
  double momentum_coeff;                /*!< The coefficient to use for momentum */

 public:
  //! Constructor for a neural network
  /*!
    Constructs layers of the sizes specified in `dims`. See the definition of the member
    `layer_dims` to see how to specify sizes. Fills in data about the network as well,
    which the network will later use in training. 

    Initiates using the `'normal'` mode. 

    \param dims vector of network dimensions. For a `n` layer network, requires `n+1` values
    \param learn_rate the learning rate
    \param regularization the regularization coefficient
    \param momentum_coeff the Nesterov momentum coefficient
    \param activation the activation function for the layers
    \sa WeightLayer()
  */ 
  NeuralNet(vector<size_t> dims, float learn_rate, double regularization,
            double momentum_coeff,
            string activation = "sigmoid");  

  void set_lr(double lr) { this->learning_rate = lr; } /*!< Set the learning rate to `lr` */
  void print_network(size_t num = 0); /*!< Pretty-print the network, showing the upper-left `num` values */
  //! Propagate data forward
  /*!
    Perform forward propagation on a batch of data `input`. Propagates the data past each layer,
    applying activation functions as necessary.

    As implemented, can propagate past more than one layer, however it does not return
    activation caches as would be needed to perform back-prop. Thus, at this time, this
    is not ready for gradient descent on multiple layers. 

    \param input The input data, as a `Data` instance
    \param activations The output of propagation
    \sa neurallayers
  */
  void propagate_forward(Data& input, vector<vector<double>>& activations); 
  //! Propagate the data backward to compute gradients
  /*!
    Perform backward propagation on a batch of data. Requires that `input` be the same 
    data that resulted in the output `activations`, and returns the values `gradients` 
    to be fed back into the network.

    As implemented, can only perform backpropagation past a single layer. Backprop past
    several layers requires caches of the activations at each layer, which is why this
    function's signature includes `input`. See the source file for a template for a class
    that would solve this problem, called "Caches".

    \param input Input data, required to compute gradients
    \param activations The activations corresponding to the data `input`
    \param gradients The resulting gradients
    \sa neurallayers, propagate_forward()
  */
  void propagate_backward(Data& input, vector<vector<double>>& activations,
                          vector<vector<double>>& gradients);

  //! Update gradients in all layers
  /*!
    Not implemented. Change `WeightLayer` to some implementation of `Caches` (see 
    source file) and implement to make this code support multi-layer networks  
  */
  void update_gradients(vector<WeightLayer>& gradients);

  //! Update gradients in a single layer
  /*!
    Given computed gradients, multiply by the learning rate and update the 
    weights using the formula 
    `W = phi - lambda gradients`

    \param gradients The computed gradients to use to update
    \throws invalid_argument If `gradients` is not the right size
  */
  void update_gradients(vector<vector<double>>& gradients);

  //! Subtract gradients from model
  /*!
    Given computed gradients with learning rate applied, subtract them from the 
    existing weights using the formula
    `W = phi - to_update`

    \param to_update Gradients, which have already been multiplied by the learning rate
  */
  void update_model(vector<vector<double>>& to_update);

  //! Apply Nesterov momentum before training
  /*!
    In Nesterov momentum, the momentum is subtracted from the weights *before* training.
    This function performs that operation; ie it replaces the weights `W` with the 
    "looked forward" weights `phi`, using `phi = W - gamma momentum`. 

    This function takes care of the multiplication by gamma and the subtraction for you.

    \param momentum The momentum values to apply for "looking forward"
  */
  void apply_momentum(vector<vector<double>>& momentum);

  // Takes the gradients and momentum and combines them into the next momentum
  // step momentum = gamma momentum + lambda gradients
  //! Update momentum after training
  /*!
    After computing the gradients, updates the momentum to get the value needed for 
    the next epoch, using the formula
    `momentum = gamma momentum + lambda gradients`

    \param gradients The computed gradients
    \param momentum The current momentum, which will be updated according to the above formula
    \throws invalid_argument if the gradients is empty
    \throws invalid_argument if the gradients and momentum dimensions don't match
  */
  void update_momentum(vector<vector<double>>& gradients,
                       vector<vector<double>>& momentum);

  //! Perform prediction on data
  /*!
    Given a `Data` instance, perform prediction using the weights in the network

    \param data The data to perform prediction on
    \return The number of correct predictions
  */
  int predict(Data& data);

  //! Train the network on data
  /*! 
    Not currently implemented.
  */
  void train(Data&);

  //! Inspect the network
  /*!
    Counts the number of weights that lie outside the range [min, max]. Also finds 
    the largest and smallest values. Prints the values to cout.

    Currently only implemented for a single layer network.

    \param min The minimum threshold you expect weights to be in
    \param max The maximum threshold you expect weights to be in
    \sa WeightLayer::inspect_weights()
  */
  void inspect_network(double min, double max);
  double sum_weights(); /*!< Computes and returns the sum of the weights in a single layer network */
};

//! Approximate the sigmoid function
/*! 
  Compute the "tight" polynomial approximation of the sigmoid function
  on the value `input`, given by 
  0.2551370334 + 0.1626882918 * input + 0.01417664232 * pow(input, 2) -
  0.003069479016 * pow(input, 3) - 0.0001701280363 * pow(input, 4) +
  0.00002415095529 * pow(input, 5) 

  This actually approximates the "shifted" sigmoid, which has it's y-intercept at 0.2

  \param input The input to the polynomial
  \return The "tight" polynomial applied on the value `input`
*/
double tight_polyn(double input); 
//! Approximate the sigmoid function
/*! 
  Compute the "wide" polynomial approximation of the sigmoid function 
  on the value `input`, given by  
  0.2658986066 + 0.07482567161 * input + 0.004045434629 * pow(input, 2) -
  0.0003215459574 * pow(input, 3) - 0.00001530544060 * pow(input, 4) +
  0.0000008110730052 * pow(input, 5)

  This actually approximates the "shifted" sigmoid, which has it's y-intercept at 0.2
  
  \param input The input to the polynomial
  \return The "wide" polynomial applied on the value `input`
*/
double wide_polyn(double input);  

//! Apply softmax activation
/*!
  Apply the softmax activation function. 

  `input` is the logits, the output of a linear layer, and `output` is the output of the
  activation.

  \param input The logits to apply softmax to
  \param output The output activation of the softmax
*/
void softmax(const vector<double>& input, vector<double>& output);

//! Apply (shifted) sigmoid activation
/*!
  Apply the shifted sigmoid activation function, componentwise. The sigmoid value is
  shifted so that the y-intercept is at 0.2. This makes sense when the output layer
  has 5 parameters, but feel free to change this value.

  `input` is the logits, the output of a linear layer, and `output` is the output of the
  activation.

  \param input The logits to apply sigmoid to
  \param output The output activation of the sigmoid
*/
void sigmoid(const vector<double>& input, vector<double>& output);

//! Apply the wide sigmoid approximation
/*!
  This function applies the wide polynomial approximation to sigmoid to logits.

  \param input the logits to apply to
  \param output the output of the activation function
  \sa wide_polyn()
*/
void wide_sigmoid_approx(const vector<double>& input, vector<double>& output);

//! Apply the tight sigmoid approximation
/*!
  This function applies the tight polynomial approximation to sigmoid to logits.

  \param input the logits to apply to
  \param output the output of the activation function
  \sa tight_polyn()
*/
void tight_sigmoid_approx(const vector<double>& input, vector<double>& output);

//! Propagate backwards past a softmax layer
/*!
  Used to propagate backward past a softmax layer, when using a log-loss cost function.
  Also used for sigmoid and cross-entropy, so maybe the function has a bad name.
  In fact, this is used with no activation function and MSE cost, so it's a real work-horse

  \param cache The activations coming out of this layer during forward prop
  \param dA The gradients of the activations propagated backward into this layer
  \param prev_gradient The computed gradients
*/
void softmax_backward_single(const vector<double>& cache,
                             const vector<double>& dA,
                             vector<double>& prev_gradient);

//! Negate a vector. 
/*!
  Negates a vector. Designed to be used at the first step of gradient descent.

  Not currently in use.

  \param activation The values to be negated
  \param quotient The result of negation
*/
void negate(vector<double>& activation, vector<double>& quotient);


//! Compute the gradients of a layer
/*!
  Given dZ, the gradient from the current layer, and activations, the cached
  activations (A) from the previous layer, compute gradients, or dW from the
  current layer, using the formula dW = dZ x activations, where x is the outer
  matrix product.

  \param dZ The gradients coming from the linear layer
  \param activations The cached activations from the previous layer
  \param gradients The resulting gradients of the current weight layer
*/
void compute_gradients(vector<double>& dZ, vector<double>& activations,
                       vector<vector<double>>& gradients);

//! Aggregate several gradients
/*!
  Gradients come out of the algorithm one at a time. This will combine them into
  a single gradient matrix.

  Simply takes the average of all the gradients

  \param gradients Several gradient matrices to be aggregated
  \param aggregate The resulting gradient matrix
*/
void aggregate_gradients(vector<vector<vector<double>>>& gradients,
                         vector<vector<double>>& aggregate);

// Given weights, the current weights in this layer, gradients, the computed
// gradients for this batch, learning_rate, the learning rate, compute
// new_weights, the new values of the weights for this layer
//! Compute the new weight values
/*!
  Compute the new weights after computing the gradients.

  new_weights = (1 - regularization) weights - learning_rate gradients

  \param weights The existing weights to be updated
  \param gradients The computed gradients
  \param learning_rate The learning rate to use in updating
  \param regularization The regularization coefficient to use (if applicable, 
                        use 0 for no regularization)
  \param new_weights The resulting weights
*/
void update_gradients(vector<vector<double>>& weights,
                      vector<vector<double>>& gradients, double learning_rate,
                      double regularization,
                      vector<vector<double>>& new_weights);

//! Initialize momentum
/*!
  Creates a matrix of zeros of the right size to use for momentum.

  \param momentum The resulting momentum matrix
  \param num_rows The number of rows required
  \param num_cols The number of columns desired
*/
void initialize_momentum(vector<vector<double>>& momentum, size_t num_rows,
                         size_t num_cols);

#endif

/*! \page neurallayers How to extend this code to multiple layers

  The biggest obstacle to this code working for multiple layers is the fact that 
  backpropagation requires the "cached" values from forward propagation. That is,
  to propagate past layer i, you need the activations after the ith layer, the gradients
  computed from the i+1th layer, and the activations that went into the ith layer. 

  In the source code file `neural.h`, there is a template for a class called `Caches`, 
  which was supposed to do this. Basically, all the forward and backward propagation
  functions would include in their signatures a vector of `Caches`, which would 
  automatically be filled and used to compute gradients. This is relatively 
  straightforward, but was not required for our use case and thus did not get completed.

  If you understand this code well enough to run it, it shouldn't be too hard to expand it
  to run with multiple layers!

  \sa propagate_backward(), propagate_forward(), Caches
*/