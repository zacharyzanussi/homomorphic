#ifndef SEALFUNCTIONS_H
#define SEALFUNCTIONS_H

#pragma once

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "seal/seal.h"

using namespace seal;
using namespace std;

// FIXME TODO hardcoded sec level
#define SEC_LEVEL sec_level_type::tc128

//! View the head of a vector
/*!
    Print the first n values of a vector to cout.

    \param vec A vector of values to print
    \param n The number of values to print. Default is 4
*/
void headOfVector(const vector<double>& vec, const int n = 4);

//! Peek at the chunks of a packed ciphertext
/*!
    Several vectors can be packed into the different "chunks" of a 
    ciphertext. Use this to view the first few values of each chunk.

    We assume that the chunks are uniform in size, and are some power of 2

    \param ct The ciphertext to peek at
    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param num_chunks The number of chunks in the ciphertext
    \param n The number of values to print in each chunk
    \param full_size Used to pad the results when printing matrices
*/
void peekAtChunks(const Ciphertext& ct, CKKSEncoder& encoder,
                  Decryptor& decryptor, size_t num_chunks, size_t n = 4,
                  size_t full_size = 0);

//! Print a report on certain ciphertext properties
/*!
    Print a list of relevant properties on a ciphertext to cout. 
    Useful for debugging.

    Prints the coefficient modulus size (CMS), scale, size, and parameter id.

    \param ct The ciphertext to report on
*/
void ciphertextReport(const Ciphertext&);

//! Print a report on certain plaintext properties
/*!
    Print a list of relevant properties on a plaintext to cout.
    Useful for debugging.

    Prints the scale and parameter id.

    \param pt The plaintext to report on
*/
void plaintextReport(const Plaintext&);

//! Stream operator for pid's
/*!
    A convenience function for viewing parms_id_type: in reality,
    these are just hash strings, but since they have their own type
    it's not easy to print them to cout. This stream operator allows
    the following syntax:
    cout << params_ids.at(2) << endl;

    \param os Required to overload <<
    \param pid The parms_id_type to print (it's a hash)
*/
ostream& operator<<(ostream& os, const parms_id_type& pid);

//! Encode zero-one plaintexts for performing packed summations
/*!
    If a ciphertext is packed with chunks, then to perform a cyclic
    summation (several rotations and additions, with the goal of 
    obtaining the sum of every chunk in every slot in the chunk) we
    need to zero out all but one value and then re-propagate the values
    through the ciphertext. 

    The "zeroing out" process requires multiplying by a plaintext (or
    ciphertext) with 1's at the first slot of every chunk and 0's otherwise.
    This function creates that plaintext for you.

    \param parms_id The parameter id to encode the plaintext to
    \param scale The scale to encode the plaintext to 
    \param encoder An encoder instance
    \param chunk_size The number of slots in a chunk
    \param zero_one The resulting encoded plaintext
*/
void encode_zero_one(const parms_id_type& parms_id, size_t scale, 
                     CKKSEncoder& encoder, size_t chunk_size,
                     Plaintext& zero_one);

//! Perform a cyclic summation left on a ciphertext, overwriting it
/*!
    Recursively adds toAdd back to itself, rotating left. If N is -1, will
    perform enough rotations so that the entire vector is summed. If not, then
    it will perform N rotations. Overwrites the input ciphertext with the result.

    EG If poly_mod_degree is 14 and N = 12, then this sums up a ciphertext that
    has two chunks. That is, it will return a ct with the sum of each half vector
    as the first value in each chunk, and garbage elsewhere.

    Use a zero-one multiplication, followed by a cyclic-sum right, to propagate
    just the sum values throughout the chunks. 

    \param toAdd The ciphertext to sum, which is overwritten
    \param galois_keys Galois keys to use for the rotation
    \param evaluator An evaluator instance
    \param N The number of rotation-additions to perform. If `N == -1`, then 
            it will perform the full summation. Else, it will start by rotating
            by 1 and adding, then rotating by 2 then adding, until it rotates by 
            2^N and adding. 
    \sa cyclic_add(), cyclic_add_right()
*/
void cyclic_add_inplace(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                        Evaluator& evaluator, int N = -1);

//! Perform a cyclic summation left on a ciphertext
/*!
    Recursively adds toAdd back to itself, rotating left. If N is -1, will
    perform enough rotations so that the entire vector is summed. If not, then
    it will perform N rotations. Returns the result in a new ciphertext.

    EG If poly_mod_degree is 14 and N = 12, then this sums up a ciphertext that
    has two chunks. That is, it will return a ct with the sum of each half vector
    as the first value in each chunk, and garbage elsewhere.

    Use a zero-one multiplication, followed by a cyclic-sum right, to propagate
    just the sum values throughout the chunks. 

    \param toAdd The ciphertext to sum
    \param galois_keys Galois keys to use for the rotation
    \param evaluator An evaluator instance
    \param result The resulting ciphertext
    \param N The number of rotation-additions to perform. If `N == -1`, then 
            it will perform the full summation. Else, it will start by rotating
            by 1 and adding, then rotating by 2 then adding, until it rotates by 
            2^N and adding. 
    \sa cyclic_add_inplace(), cyclic_add_right()
*/
void cyclic_add(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                Evaluator& evaluator, Ciphertext& result, int N = -1);

//! Perform a cyclic summation right on a ciphertext, overwriting it
/*!
    Recursively adds toAdd back to itself, rotating right. If N is -1, will
    perform enough rotations so that the entire vector is summed. If not, then
    it will perform N rotations. Overwrites the input with the result.

    EG If poly_mod_degree is 14 and N = 12, then this sums up a ciphertext that
    has two chunks. That is, it will return a ct with the sum of each half vector
    as the first value in each chunk, and garbage elsewhere.

    Use this after a cyclic-sum left and zero-one multiplication to propagate the summed
    values throughout the chunks, when using chunked ciphertexts.

    \param toAdd The ciphertext to sum, which is overwritten with the result
    \param galois_keys Galois keys to use for the rotation
    \param evaluator An evaluator instance
    \param N The number of rotation-additions to perform. If `N == -1`, then 
            it will perform the full summation. Else, it will start by rotating
            by 1 and adding, then rotating by 2 then adding, until it rotates by 
            2^N and adding. 
    \sa cyclic_add_right(), cyclic_add()
*/
void cyclic_add_inplace_right(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                              Evaluator& evaluator, int N = -1);

//! Perform a cyclic summation right on a ciphertext
/*!
    Recursively adds toAdd back to itself, rotating right. If N is -1, will
    perform enough rotations so that the entire vector is summed. If not, then
    it will perform N rotations. The result is returned as a new ciphertext.

    EG If poly_mod_degree is 14 and N = 12, then this sums up a ciphertext that
    has two chunks. That is, it will return a ct with the sum of each half vector
    as the first value in each chunk, and garbage elsewhere.

    Use this after a cyclic-sum left and zero-one multiplication to propagate the summed
    values throughout the chunks, when using chunked ciphertexts.

    \param toAdd The ciphertext to sum
    \param galois_keys Galois keys to use for the rotation
    \param evaluator An evaluator instance
    \param result The resulting ciphertext
    \param N The number of rotation-additions to perform. If `N == -1`, then 
            it will perform the full summation. Else, it will start by rotating
            by 1 and adding, then rotating by 2 then adding, until it rotates by 
            2^N and adding. 
    \sa cyclic_add_inplace_right(), cyclic_add()
*/
void cyclic_add_right(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                      Evaluator& evaluator, Ciphertext& result, int N = -1);

//! Encode a vector into a plaintext
/*!
    This function wraps the encode process provided by SEAL. It's not necessary by 
    any means, but having a unified notation for encoding and decrypting was useful
    at some point.

    Be careful that the vector is not longer than slot_count.

    \param vec The values to encode
    \param scale The scale to use in encoding
    \param encoder An encoder instance
    \param destination The resulting plaintext
*/
void encode(const vector<double>& vec, const double scale, CKKSEncoder& encoder,
            Plaintext& destination);

//! Decode a plaintext into a vector
/*!
    Decodes a plaintext into a vector of values. Since a plaintext decoding returns a 
    vector with slot_count entries, no matter how many values are encoded, any value that
    is less than error at the end of the vector will be removed. However, there is no 
    guarantee that the resulting vector will be only useful values; you must ensure that 
    yourself.

    \param plaintext The plaintext to decode
    \param encoder An encoder instance
    \param destination The resulting decoded vector
    \param error The threshold for removing trailing values. 
*/
void decode(const Plaintext& plaintext, CKKSEncoder& encoder,
            vector<double>& destination, double error);

// Takes a single plaintext and returns an encryption of it
//! Encrypt a plaintext
/*! 
    A wrapper for SEAL's encryption function. Not strictly necessary, but at some 
    point it was useful to have a unified encoding/encrypting suite.

    \param plaintext The plaintext to encrypt
    \param encryptor An encryptor instance
    \param ciphertext The resulting ciphertext
*/
void encrypt(const Plaintext& plaintext, const Encryptor& encryptor,
             Ciphertext& ciphertext);

//! Encrypt a vector directly to ciphertext
/*!
    Encrypts a vector directly to ciphertext. Useful for saving code when encrypting.
    Handles the encoding process for you.

    Be careful that the vector is not longer than slot_count.

    \param vec The values to encrypt
    \param scale The scale to use in encoding
    \param encoder An encoder instance
    \param encryptor And encryptor instance
    \param ciphertext The resulting ciphertext
*/
void encrypt(const vector<double>& vec, const double scale,
             CKKSEncoder& encoder, const Encryptor& encryptor,
             Ciphertext& ciphertext);

//! Decrypt a ciphertext into a plaintext
/*! 
    Wraps SEAL's decryption process.

    \param ciphertext The ciphertext to decrypt
    \param decryptor A decryptor instance
    \param plaintext The resulting plaintext after decryption
*/  
void decrypt(const Ciphertext& ciphertext, Decryptor& decryptor,
             Plaintext& plaintext);

//! Decrypt a ciphertext into a vector
/*!
    Decrypts a ciphertext directly to a vector. Since a plaintext decoding returns a 
    vector with slot_count entries, no matter how many values are encoded, any value that
    is less than error at the end of the vector will be removed. However, there is no 
    guarantee that the resulting vector will be only useful values; you must ensure that 
    yourself.

    Default value for error is 1e-5; this might be too large for some parameter sets.

    \param ciphertext The ciphertext to decrypt
    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param vec The resulting vector of decrypted values, trimmed of trailing small values
    \param error The error threshold for trimming trailing values
*/
void decrypt(const Ciphertext& ciphertext, CKKSEncoder& encoder,
             Decryptor& decryptor, vector<double>& vec, double error = 1e-5);

//! Peek at the first values in a ciphertext
/*! 
    Prints the first n values in a ciphertext to cout.

    \param ct The ciphertext to peek at
    \param encoder An encoder instance
    \param decryptor A decryptor instance
    \param n The number of values to peek at. Default 4
*/
void peekAtCiphertext(const Ciphertext& ct, CKKSEncoder& encoder,
                      Decryptor& decryptor, int n = 4);

//! Saves Encryption Parameters to file
/*!
    \param parms a reference to EncryptionParameters
    \param filename filename and path of the parameters file
*/
void saveParams(EncryptionParameters& parms, const string filename);

//! Loads Encryption Parameters to file
/*!
    \param parms a reference to EncryptionParameters
    \param filename filename and path of the parameters file
*/
void loadParams(EncryptionParameters& parms, const string filename);

#endif  // !SEALFUNCTIONS_H

/* Here are the functions for timing. Make sure to add #include <chrono>
        chrono::high_resolution_clock::time_point time_start, time_end;
        chrono::microseconds time_diff;

        time_start = chrono::high_resolution_clock::now();
        time_end = chrono::high_resolution_clock::now();
        time_diff = chrono::duration_cast<chrono::microseconds>(time_end -
   time_start);
*/