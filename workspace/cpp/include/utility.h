#ifndef UTILITY_H
#define UTILITY_H

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <type_traits>
#include <vector>

using namespace std;

//! Pretty-print a matrix to cout
/*!
  Given a matrix (that is, a vector of vector of doubles) pretty-print the num
  x num upper left block to cout.

  \param matrix The matrix to print
  \param num The number of values to print, default to the whole matrix
*/
void print_matrix(vector<vector<double>>& matrix, size_t num = 0);

//! Generate a random weight matrix
/*! 
  Generate a random `rows` x `cols` matrix and store it in `matrix`. Overwrites
  `matrix`. If random == 'normal', use LeCun initialization; that is, normal
  distribution with mean 0 and stddev 1 / sqrt(num_cols), where n is the number of
  input units. This is the recommmended setting. If random == 'uniform', then
  it initializes with uniform random values absolutely bounded by 1 / sqrt(num_cols)

  \param rows The number of rows in the generated matrix
  \param cols The number of columns in the generated matrix.
  \param matrix The resulting matrix
  \param random The method with which to generate the random values. Options
    are "normal", which will draw parameters from the normal distribution centred on
    0 with std dev 1 / sqrt(num_cols), and "uniform", which will draw parameters uniformly
    from [ -1 / sqrt(num_cols), 1 / sqrt(num_cols) ]. Default is "normal".
  \param random_seed If true, will randomize the output. If false, will use a fixed
    seed, for reproducibility. Warning; false might not work! Test it. Default is true.
  \throws invalid_argument if given an invalid random number generator
*/
void random_weight_matrix(size_t rows, size_t cols,
                          vector<vector<double>>& matrix,
                          string random = "normal", bool random_seed = true);

//! Generate a random weight matrix
/*! 
  Generate a random `rows` x `cols` matrix packed with `num_packed` submatrices
  and store it in `matrix`. Overwrites `matrix`. If random == 'normal', use LeCun 
  initialization; that is, normal distribution with mean 0 and stddev 
  1 / sqrt(num_cols), where n is the number of input units. This is 
  the recommmended setting. If random == 'uniform', then it initializes 
  with uniform random values absolutely bounded by 1 / sqrt(num_cols).

  The submatrices are packed so that the resulting matrix is 
  rows x (cols * num_packed); that is, they are stacked horizontally.

  \param rows The number of rows in each generated submatrix
  \param cols The number of columns in each generated submatrix.
  \param num_packed The number of submatrices to pack
  \param matrix The resulting matrix
  \param random The method with which to generate the random values. Options
    are "normal", which will draw parameters from the normal distribution centred on
    0 with std dev 1 / sqrt(num_cols), and "uniform", which will draw parameters uniformly
    from [ -1 / sqrt(num_cols), 1 / sqrt(num_cols) ]. Default is "normal".
  \param random_seed If true, will randomize the output. If false, will use a fixed
    seed, for reproducibility. Warning; false might not work! Test it. Default is true.
  \throws invalid_argument if given an invalid random number generator
*/
void random_packed_weight_matrix(size_t rows, size_t cols, size_t num_packed,
                                 vector<vector<double>>& matrix,
                                 string random = "normal",
                                 bool random_seed = true);

//! Get a random value from a normal distribution
/*!
  Sample a normal distribution with a given mean and standard deviation.

  The `rand` parameter is meant to be able to set the seed for reproducibility;
  this does not work well when set to false, so use true unless you fix it 
  (the problem is that every call gets the same random seed, so that every 
  random value ends up being identical).

  \param mean The mean value for the normal distribution
  \param stddev The standard deviation for the normal distribution
  \param rand If false, seed with a fixed value for reproducibilty.
    Currently poorly implemented; use true instead. Default is true.
*/
float get_normal_random(float mean, float stddev, bool rand = true);

//! Get a random value from a uniform distribution
/*!
  Sample a uniform distribution with a given upper and lower bound.

  The `rand` parameter is meant to be able to set the seed for reproducibility;
  this does not work well when set to false, so use true unless you fix it 
  (the problem is that every call gets the same random seed, so that every 
  random value ends up being identical).

  \param min The lower bound for the uniform distribution
  \param max The upper bound for the uniform distribution
  \param rand If false, seed with a fixed value for reproducibilty.
    Currently poorly implemented; use true instead. Default is true.
*/
float get_uniform_random(float min, float max, bool rand = true);

//! Pretty-print a vector
/*!
  Prints the head of a vector to cout. If `num == 0`, prints the whole thing.

  Will work with any data type so long as `cout << t` is implemented, where 
  `t` is an instance of `T`.

  \param vect The vector to print
  \param num The number of values to print. Default is 0
*/
template <typename T>
void print_vector(const vector<T>& vect, size_t num = 0) {
  if (vect.size() == 0) {
    cout << "Vector is empty" << endl;
    return;
  }
  size_t count = 1;
  for (const auto& val : vect) {
    cout << val << " ";
    if (count == num) {
      cout << "...";
      break;
    }
    count++;
  }
  cout << endl;
}

//! Helper function to print the upper left corner of each submatrix
/*!
  \param vect One row of the matrix
  \param num_chunks The number of chunks in the matrix; that is, the number
    of submatrices packed into it
  \param num The number of values to print. Default is 5
  \param oneline Whether or not to print it all on one line. Default is false
  \param chunk_size The number of values in each chunk. Default is 0
  \param full_size Dunno
*/
void print_chunks(const vector<double>& vect, size_t num_chunks, size_t num = 5,
                  bool oneline = false, size_t chunk_size = 0,
                  size_t full_size = 0);

//! Print the upper left corner of each submatrix
/*!
  Pretty print the chunks of each matrix to cout. Lines them up all nice. 
  Great job Ben!

  \param mat The packed matrix to print
  \param num_chunks The number of chunks in the matrix
  \param num The size of the corner to print, default is 5
*/
void print_chunks(const vector<vector<double>>& mat, size_t num_chunks,
                  size_t num = 5);

//! Compute the inner product of two vectors
/*! 
  Computes and returns the inner product of two vectors of doubles.

  \param vec1 The first vector in the product
  \param vec2 The second vector in the product
  \returns The calculated inner product
  \throws invalid_argument if the vectors are different sizes
*/
double vector_innerprod(const vector<double>& vec1, const vector<double>& vec2);

//! Compute the product of a matrix and a vector
/*!
  Computes the product of a matrix with a vector. Returns the result as a new 
  vector. 

  \param matrix The matrix to multiply
  \param vec The vector to multiply
  \param result The resulting vector
  \throws invalid_argument if `matrix` is malformed or if the number of columns
    does not match the size of vec
*/
void matrix_vector_product(const vector<vector<double>>& matrix,
                           const vector<double>& vec, vector<double>& result);

//! Loads a single entry into a vector, without overwriting it's contents
/*!
  A specialized function for loading a single line of our synthetic scanner 
  data. Data should have the following form:
  label,description,3gram,4gram,5gram,6gram

  This function appends the new entry to the back of the current one; it does
  not overwrite any data. This is a backend function; you are probably looking 
  for `load_data_line()` or `load_data_packed()`.

  \param line A product description entry, in the form described above
  \param vect Destination for the encoded vector. The new entries will be
    appended to the end of this. 
  \param number_ngrams total number of ngrams. Not
    used unless hashor == 0. 
  \param hashor size that each data entry will be hashed down to. If 
    hashor == 0, no hashing will occur. 
  \param normalized boolean value; if true, vector will be normalized by 
    it's hamming weight so that it's sum is roughly 1
  \sa load_data_line(), load_data_packed()
*/
void load_data(string& line, vector<double>& vect, size_t number_ngrams,
               size_t hashor = 0, bool normalize = false);

//! Loads a single entry into a vector, overwriting it's contents
/*!
  A specialized function for loading a single line of our synthetic scanner 
  data. Data should have the following form:
  label,description,3gram,4gram,5gram,6gram

  The parameter `vect` will be erased and overwritten; if you want to append
  to it, try `load_data()`

  \param line a product description entry, in the form described above
  \param vect destination for the encoded vector. This will be overwritten.
  \param number_ngrams total number of ngrams. Not used unless hashor == 0.
  \param hashor size that each data entry will be hashed down to. If hashor ==
    0, no hashing will occur. 
  \param normalized boolean value; if true, vector
    will be normalized by it's hamming weight so that it's sum is roughly 1
*/
void load_data_line(string& line, vector<double>& vect, size_t number_ngrams,
                    size_t hashor = 0, bool normalize = false);

//! Packs multiple entries into a vector, overwriting it's contents
/*!
  A specialized function for loading several lines of our synthetic scanner 
  data into a single packed vector. Data should have the following form:
  label,description,3gram,4gram,5gram,6gram

  The parameter `vect` will be erased and overwritten with a packed vector.

  \param lines a vector of product description entries, each in the form given
    in reduced.csv, which will be packed into a single vector 
  \param vect destination for the encoded vector. This will be overwritten. 
  \param number_ngrams total number of ngrams. Not used unless hashor == 0. 
  \param hashor size that each data entry will be hashed down to. If hashor == 0, 
    no hashing will occur. 
  \param normalized boolean value; if true, vector will be normalized by it's 
    hamming weight so that it's sum is roughly 1
*/
void load_data_packed(vector<string>& lines, vector<double>& vect,
                      size_t number_ngrams, size_t hashor = 0,
                      bool normalize = false);

//! Load in a label vector
/*! 
  Sometimes, the labels need to be one-hot encoded. This function will do that
  for you. Data should have the following form:
  label,description,3gram,4gram,5gram,6gram

  \param line a product description entry, in the form described above
  \param vect destination for the encoded vector. This will be overwritten.
  \param number_label The number of labels  
*/
void load_label_line(string& line, vector<double>& vect, size_t number_label);

//! Load in the product description
/*!
  Sometimes it's nice to keep the product description after encoding. This will 
  extract it for you. Data should have the following form:
  label,description,3gram,4gram,5gram,6gram

  \param line A product description entry, in the form described above
  \param desc The returned description
*/
void load_description(string line, string& desc);

//! Find the argument of the largest value in a vector
/*!
  Computes the argmax of a vector, returning the location of the largest value.

  \param vect The vector to compute argmax on
*/
template <typename T>
int argmax(vector<T> vect) {
  // std::max_element(vect.begin(), vect.end()) - vect.begin()
  return std::distance(vect.begin(),
                       std::max_element(vect.begin(), vect.end()));
}

//! Compute some statistics on a vector
/*!
  Gives a 'histogram' of the elements in a vector, printing a list of the distinct
  elements and the number of each

  \param vec The vector to look at
*/
void statsOnVector(vector<int> vec);

//! Tally the correct activations
/*!
  Helper function for prediction. Compares activations and the one-hot encoded
  labels and returns the number of activations that predict the correct label.

  \param activations The activations to measure 
  \param labels The correct one-hot encoded labels
  \returns The number of correct predictions
*/
int tally_activations(vector<vector<double>>& activations,
                      vector<vector<double>>& labels);

//! Check if two values are close
/*!
  Helper function for checking if two values are close. Usually used to compare
  floats or doubles, as equality is always tricky there.
*/
// from https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y, int ulp) {
  // the machine epsilon has to be scaled to the magnitude of the values used
  // and multiplied by the desired precision in ULPs (units in the last place)
  return std::fabs(x - y) <=
             std::numeric_limits<T>::epsilon() * std::fabs(x + y) * ulp
         // unless the result is subnormal
         || std::fabs(x - y) < std::numeric_limits<T>::min();
}

//! Check if a file exists, and throw a meaningful error if it doesn't
/*! 
  A compact way to check if a file exists before you use it.
  Throws an error that mentions file_name to assist in debugging.
  
  \param file_name the name of the file to check for 
  \throws invalid_argument if the file is not found
  */
void check_for_file(string file_name);

#endif