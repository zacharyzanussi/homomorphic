//! client.cpp
/*!
Performs the client's side of the text classification protocol;
that is, encodes and encrypts the data before it is sent off to 
the cloud. 

This code supports packing, that is, encoding multiple data points
into a single ciphertext. This reduces the number of ciphertexts
necessary to encode a dataset. This is especially useful if your
dataset has a small number of features.
*/
#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include <omp.h>

//#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

int main(int argc, char** argv) {
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  Parameters params;
  Files files;

  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);

  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << " * Beginning a run of the client code with input parameters* "
       << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << "      Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

/*! 
Here we load the encryption instance and the keys. These were
generated by generator.cpp. We only need the public key;
we actually load the secret key here in case you decide to
run the deserialization and decrypting process that is 
included at the end of the file. You can comment out the 
secret key stuff if you want.

In production, this code would be run by the client or data
holder. They would have received the public key and encryption
parameters from the data consumer, and would also run any data
preprocessing scripts that we would have them apply.
*/ 
  EncryptionParameters eparms;

  loadParams(eparms, files.ciphertext_location + "parms.enc");

#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  KeyGen keygen(context, files.ciphertext_location);

  keygen.DeSerializeSecret();

  keygen.DeSerializePublic();

  Encryptor encryptor(context, keygen.public_key_);
  Decryptor decryptor(context, keygen.secret_key());

  CKKSEncoder encoder(context);
  cout << "Seal instance loaded!" << endl << endl;
  vector<parms_id_type> parms_ids;

#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    parms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  cout << "Collected " << parms_ids.size() << " parms_ids:" << endl;


  string file_name = files.train_file;
  string test_file_name = files.test_file;
  string ciphertext_location = files.ciphertext_location;
  string ciphertext_train_name = files.ciphertext_train_name;
  string ciphertext_test_name = files.ciphertext_test_name;

  // ------------------------------- SET PARAMETERS
  // --------------------------------------- Load in the data and the weights
  size_t num_examples = params.num_examples;
  size_t max_tile_size = params.max_tile_size;
  size_t num_labels = params.num_labels;
  size_t hash_size = params.hash_size;
  size_t slot_count = encoder.slot_count();
  size_t scale = params.scale;

  size_t act_depth = 0;

  size_t vector_size;
  if (hash_size == 0) {
    vector_size = params.num_features + 1;
  } else {
    vector_size = hash_size;
  }

  if (slot_count % vector_size != 0) {
    cout << "******* Chosen hash size or number of features + 1 does not evenly divide slot count: there "
            "might be errors *****"
         << endl;
  }
  size_t num_packed = slot_count / vector_size;
  size_t num_tiles = size_t(ceil(num_examples / max_tile_size / num_packed));

  cout << endl << "--- Client ready to begin packing and serializing" << endl;
  cout << "Packing " << num_packed << " entries into each ciphertext" << endl;
  cout << "Requested " << num_examples << " entries, divided into " << num_tiles
       << " tiles with " << max_tile_size << " ciphertexts each" << endl;

  if (filesystem::create_directory(files.ciphertext_location)) {
    cout << "Created directory " << files.ciphertext_location << endl;
  } else {
    cout << "Using existing directory " << files.ciphertext_location
         << " to store the ciphertexts" << endl;
  }

/*!
Here, we actually perform the encryption. The data in this case 
needs to have a very particular form to work with our encoding
function; Data should have the following form:
  label,description,3gram,4gram,5gram,6gram
label should be an integer, description is the actual plaintext
message to be encrypted. 3gram etc are space-separated lists of
integers, refering to which ngrams in the dictionary are present 
in the description. Here's a simple (made up) example:
  1,milk,34 35,155,,
This means that the description "milk" has label 1. The 3grams are
'mil' and 'ilk', which correspond to ngrams 34 and 35 in the dictionary.
'milk' is a 4gram with dictionary value 155. There are no 5 or 6grams 
in this description. These data points are one-hot encoded; that is, 
we would create a vector for this entry that has the 34, 35, and 155
coordinate as a 1, and the rest 0. Additionally, if `hash_size` is
not 0, then we use a hash vectorizor, so that we would really encode
h(34), h(35), h(155) as 1 in the vector, where h is the hash function. 

Data is encoded in "tiles", meaning sets of ciphertexts. They are 
saved like this so that cloud can load in and deal with a single
tile at a time, without worrying about running out of ram. If you 
are getting `out_of_memory` errors, try reducing the tile size.

WARNING: While this is set to be parallelized, there might be an 
issue when the `num_threads > 1`. We never got a chance to debug this.
If you are worried, just set `num_threads = 1`. 

The tiles that are saved are listed in a file with suffix 
`training_info.txt`. This file has the numbers assigned to each 
tile so that the cloud can easily iterate through the files. 

If you'd like to change the way this is encoded, you just need to
change the `load_data` function in `utility.cpp`. This shouldn't 
be too hard. 
*/
  {
    // ***************** Here is the part that saves the data ***************
    // Make the information file:
    ofstream info_file(ciphertext_location + "training_info.txt");
    info_file << "In this directory are a series of tiles of data for training "
                 "a neural network"
              << endl;
    info_file << "The context was loaded using the parameters in "
              << options.filename_ << endl;
    info_file << "*****";

    check_for_file(file_name);
    ifstream data_stream(file_name);
    string line;
    getline(data_stream, line);

    // Here is code for saving data tiles to file.
    cout << endl << "Preparing to save training data tiles to file!" << endl;

    #pragma omp parallel for num_threads(params.num_threads)
    for (size_t i = 0; i < num_tiles; ++i) {

      cout << "Tile " << i << "..." << endl;
      EncData data(data_stream, num_packed, params.num_features, params.hash_size, num_labels,
                   max_tile_size, encoder, scale, encryptor, parms_ids.at(0),
                   parms_ids.at(1), parms_ids.at(act_depth + 1 + 1), false);
      data.peekAtFirstChunks(encoder, decryptor);  // Peek at the data
      string num = string(4 - to_string(i).length(), '0') + to_string(i);
      info_file << endl << num;
      data.serialize(ciphertext_location + ciphertext_train_name + num,
                     i == 0 ? true : false);
    }
    info_file.close();
    data_stream.close();
    cout << "Done saving! " << endl << endl;
  }

/*! 
This file includes some sample code for loading the data
back in. This can be used as a model for adapting other code,
or it can be used to debug any serialization/deserialization 
issues. 
*/
  // {
  //   // Here is code for loading those data tiles to file.
  //   cout << "Preparing to load data tiles from file;" << endl;

  //   ifstream info_file(ciphertext_location + "_info.txt");
  //   string line;
  //   while (line != "*****") {
  //     getline(info_file, line);
  //   }

  //   while (getline(info_file, line)) {
  //     ifstream entry_stream(ciphertext_location + line + ".ct");
  //     ifstream label_stream(ciphertext_location + line + "_labels.txt");
  //     ifstream pool_stream(ciphertext_location + "0000_label_pool.ct"); //
  //     Label pool is always the same, so we just load the first one

  //     EncData data(entry_stream, label_stream, pool_stream, context,
  //     hash_size, num_labels);

  //     // Do something with the data
  //     data.peekAtFirstChunks(encoder, decryptor);
  //     ///
  //   }

  //   cout << "Done loading!" << endl;
  // }

}

