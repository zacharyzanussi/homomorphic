#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "log.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

// --------------------------------- DISPATCH FUNCTIONS

void accumulates(const vector<Ciphertext>& act, size_t num_labels,
                 Evaluator& evaluator, vector<Ciphertext>& gradients) {
  for (size_t i = 0; i < num_labels; i++) {
    evaluator.add_inplace(gradients[i], act[i]);
  }
};

void batch(const Ciphertext& data_entry,
           const vector<int>& data_labels, const EncWeightLayer& weights,
           const vector<Ciphertext>& label_pool, size_t num_labels,
           size_t num_packed, const Parameters& params, const KeyGen& keygen,
           Evaluator& evaluator, const Plaintext& learning_rate_pt,
           vector<Ciphertext>& gradients, dispatch_queue& acc) {
  // Creates new memory pool
  MemoryPoolHandle my_pool = MemoryPoolHandle::New();
  // store old profile
  auto old_prof = MemoryManager::SwitchProfile(
      make_unique<MMProfFixed>(std::move(my_pool)));
  auto old_pool = old_prof->get_pool(mm_prof_opt::mm_force_global);

  //DBG << epoch << "-switch";
  //DBG << epoch << "-1-memory pool:" << my_pool.alloc_byte_count();
  //DBG << epoch << "-1-memory pool count:" << my_pool.pool_count();
  //DBG << epoch << "-1-old memory pool:"
  //    << static_cast<double>(old_pool.alloc_byte_count()) / 1048576.0;
  //DBG << epoch << "-1-old memory pool count:" << old_pool.pool_count();

  vector<Ciphertext> outct(weights.get_num_rows());

  weights.linear_forward(data_entry, evaluator, keygen.gal_keys_,
                         keygen.relin_keys_, outct);

  for (size_t i = 0; i < outct.size(); ++i) {
    for (size_t chunk = 0; chunk < num_packed; ++chunk) {
      if (data_labels.at(chunk) == static_cast<int>(i)) {
        evaluator.sub_inplace(outct.at(i), label_pool.at(chunk * 2 + 1));
      } else {
        evaluator.sub_inplace(outct.at(i), label_pool.at(chunk * 2));
      }
    }
  }

  Ciphertext red_data_entry;
  evaluator.mod_switch_to_next(data_entry, red_data_entry);
  evaluator.multiply_plain_inplace(red_data_entry, learning_rate_pt);
  evaluator.rescale_to_next_inplace(red_data_entry);
  red_data_entry.scale() = params.scale;

  for (auto& ct : outct) {
    // Ciphertext single_gradient;
    evaluator.multiply_inplace(ct, red_data_entry);
    evaluator.relinearize_inplace(ct, keygen.relin_keys_);
    evaluator.rescale_to_next_inplace(ct);
    ct.scale() = params.scale;
  }

  //DBG << epoch << "-2-memory pool:" << my_pool.alloc_byte_count();
  //DBG << epoch << "-2-memory pool count:" << my_pool.pool_count();
  //DBG << epoch << "-2-old memory pool:"
  //    << static_cast<double>(old_pool.alloc_byte_count()) / 1048576.0;
  //DBG << epoch << "-2-old memory pool count:" << old_pool.pool_count();

  // release memory for this ciphertext
  red_data_entry.release();

  // peekAtCiphertext(gradients.at(0), encoder, decryptor);
  acc.dispatch([outct, num_labels, &evaluator, &gradients] {
    accumulates(outct, num_labels, evaluator, gradients);
  });

  //DBG << epoch << "-before switch";
  //DBG << epoch << "-3-memory pool:" << my_pool.alloc_byte_count();
  //DBG << epoch << "-3-memory pool count:" << my_pool.pool_count();
  //DBG << epoch << "-3-old memory pool:"
  //    << static_cast<double>(old_pool.alloc_byte_count()) / 1048576.0;
  //DBG << epoch << "-3-old memory pool count:" << old_pool.pool_count();
  // restore profile
  MemoryManager::SwitchProfile(std::move(old_prof));

  //DBG << epoch << "-after switch";
  //DBG << epoch << "-4-memory pool:" << my_pool.alloc_byte_count();
  //DBG << epoch << "-4-memory pool count:" << my_pool.pool_count();
  //DBG << epoch << "-4-old memory pool:"
  //    << static_cast<double>(old_pool.alloc_byte_count()) / 1048576.0;
  //DBG << epoch << "-4-old memory pool count:" << old_pool.pool_count();
  // destroy all ciphertexts and force a reallocation
  // vector<Ciphertext>().swap(outct);
};

int main(int argc, char** argv) {
#ifdef TESTING
  auto_cpu_timer t;
#endif
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  Parameters params;
  Files files;

  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);

  // init logger
  clog::init(files.weights_location + files.weight_name);
  logging::add_common_attributes();

  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *     Performing training run               " << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << " *     Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

  INFO << "Training new model " << files.ciphertext_train_name;

  EncryptionParameters eparms;

  // Load encryption parameters
  loadParams(eparms, files.ciphertext_location + "parms.enc");

  // SEALContext type changed between versions
#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  // keygen object gets the context and location of files
  KeyGen keygen(context, files.ciphertext_location);

  // Loads secret and public key
  // true, true will load stored relin_keys and gal_keys
  INFO << "Deserializing keys";
  // keygen.DeSerialize(false, false);
  keygen.DeSerialize(true, true);

  // keygen.CreatePublicKeys(false);

  // all the keys are in memory now
  INFO << "Keys loaded!";

  size_t num_labels = params.num_labels;
  size_t hash_size = params.hash_size;

  Decryptor decryptor(context, keygen.secret_key());
  Encryptor encryptor(context, keygen.public_key_);
  Evaluator evaluator(context);
  CKKSEncoder encoder(context);
  INFO << "Seal instance loaded!";

  vector<parms_id_type> eparms_ids;

// context has a different type in SEAL 36
#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    eparms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  INFO << "Collected " << eparms_ids.size() << " parms_ids";

  INFO << "We will be using " << params.num_examples
       << " train entries in batches of " << params.batch_size
       << " and loaded in tiles of " << params.max_tile_size << ", with "
       << params.num_test_examples << " test examples ";
  INFO << "Hashing to " << params.hash_size << " with momentum "
       << params.momentum_coeff << ", regularization " << params.regularization
       << " and " << params.epochs << " epochs";

// different type for context
#ifdef SEAL36
  auto current_context = context.first_context_data()->next_context_data();
#else
  auto current_context = context->first_context_data()->next_context_data();
#endif

  ifstream in_info_file(files.ciphertext_location + "training_info.txt");
  string line;
  string last_line = "*****";
  if (argc > 2) {
    last_line = string(argv[2]);
    INFO << "Beginning with the tile after" << last_line;
  }
  while (line != last_line) {
    getline(in_info_file, line);
  }

  size_t act_depth = 0;
  size_t depth = act_depth + 3;
  size_t num_packed = encoder.slot_count() / params.hash_size;

  EncWeightLayer weights;
  int starting_epoch = 0;
  INFO << "Preparing weights:";

  // path and filename for weights
  string input_weights_file_name =
      files.weights_location + files.initial_weight_name;
  if (filesystem::create_directory(files.weights_location)) {
    INFO << "Created directory " << files.weights_location;
  } else {
    INFO << "Using existing directory " << files.weights_location
         << " to store weights";
  }
  // check if weight file exists, if not we generate the weights
  INFO << "Attempting to load weights from " << input_weights_file_name
       << "... ";
  if (filesystem::exists(input_weights_file_name)) {
    INFO << "Weights found, loading in.";
    weights.load(input_weights_file_name, context);
    // weights.print_parms_id();
    for (size_t i = 0; i < eparms_ids.size(); i++) {
      if (weights.get_parms_id() == eparms_ids.at(i)) {
        starting_epoch = i / depth;
      }
    }
    INFO << "We start at epoch " << starting_epoch;
  } else {  // Make some new ones
    INFO << "Weights not found.";
    INFO << "Generating new weights, with ";
    INFO << " --- rows:      " << params.num_labels;
    INFO << " --- columns:   " << params.hash_size;
    INFO << " --- submodels: " << num_packed;
    weights = EncWeightLayer(params.num_labels, params.hash_size, num_packed,
                             params.scale, encoder, encryptor);
    string untrained_weights_name =
        files.weights_location + files.weight_name + "_untrained.wt";
    INFO << "Done! Saving to " << untrained_weights_name;
    weights.save(untrained_weights_name);
    weights.peekAtMatrixChunks(encoder, decryptor, num_packed, 4);
  }

  string predict_extension =
      files.predict_weight_name.substr(files.predict_weight_name.size() - 3);
  string predict_file_name = files.weights_location + files.predict_weight_name;
  bool save_weight_names = false;
  if (predict_extension == "txt") {
    INFO << "Predict weight name has a .txt extension. We will save the names "
            "of all weight files to "
         << predict_file_name;
    save_weight_names = true;
    // ofstream predict_file(predict_file_name, ios::trunc); // Erase the
    // predict file predict_file.close();
  }

  INFO << "------------------------------------------------";
  INFO << " - Beginning a test of the training process, using the old dispatch "
          "queues";

  // Initialize gradients so they can be in the accumulator scope
  // vector<Ciphertext> gradients;

  // -------------------------Encode momentum ciphertexts --------------
  vector<Ciphertext> momentum;
  string momentum_file_name = files.weights_location + files.weight_name + "_momentum.ct";
  if (filesystem::exists(momentum_file_name)) {
    INFO << "Loading momentum from file: " << momentum_file_name;
    ifstream momentum_file(momentum_file_name, ios::binary);
    while (1) {
      try {
        Ciphertext momentum_ct;
        momentum_ct.load(context, momentum_file);
        momentum.push_back(momentum_ct);
      } catch (exception& e) {
        break;
      }
    }
    momentum_file.close();
    if (momentum.size() != params.num_labels) {
      ERR << "Loaded momentum does not have the expected number of "
             "ciphertexts: it has "
          << momentum.size();
      return 1;
    }
    if (weights.coeff_modulus_size() > momentum.at(0).coeff_modulus_size()) {
      ERR << "Weights are at level " << weights.coeff_modulus_size()
          << " and loaded momentum is at "
          << momentum.at(0).coeff_modulus_size()
          << ": momentum needs to be refreshed";
      return 1;
    } else if (weights.coeff_modulus_size() <
               momentum.at(0).coeff_modulus_size()) {
      INFO << "Mod switching momentum to match weights at level "
           << weights.coeff_modulus_size();
      for (auto& ct : momentum) {
        evaluator.mod_switch_to_inplace(ct, weights.get_parms_id());
      }
    }
  } else {
    INFO << "No momentum file found; generating from scratch";
    for (size_t i = 0; i < num_labels; i++) {
      Ciphertext mom;
      encryptor.encrypt_zero(weights.get_parms_id(), mom);
      mom.scale() = params.scale;
      momentum.push_back(mom);
    }
  }
  // -------------------------------------------------------------------

  timer(t2, "Encrypted Gradient Descent timer");
  // There can be multiple tiles in an epoch, so we first loop over epoch
  for (size_t epoch = starting_epoch; epoch < params.epochs; ++epoch) {
    INFO << " ============= Beginning epoch " << epoch << "! ============= ";

    INFO << "Applying pre-gd momentum... ";
    // --------------- Encode the momentum ----------
    if (!almost_equal(params.momentum_coeff, 0.0, 2)) {
      // Step 1 and step 10
      weights.apply_momentum(evaluator, momentum);
    }
    //--------------------------------------
    INFO << "Done!";

    INFO << "Encoding the learning rate... ";
    // --------------- Encode learning rate ----------
    double lr_over_bs_val = params.learning_rate.at(0) / params.batch_size;
    streamsize prec = cout.precision();
    cout.precision(17);
    INFO << "Using a learning rate of " << lr_over_bs_val;
    cout.precision(prec);
    Plaintext ls_over_bs_pt;
    encoder.encode(lr_over_bs_val, eparms_ids.at(epoch * depth + 1),
                   params.scale, ls_over_bs_pt);
    // -----------------------------------------------
    INFO << "Done!";

    INFO << "Encoding the gradient accumulator... ";
    // ----------------------------- ENCODE GRADIENT ACCUMULATOR

    Ciphertext grad;
    encryptor.encrypt_zero(eparms_ids.at(epoch * depth + 3), grad);
    grad.scale() = params.scale;
    vector<Ciphertext> gradients;
    for (size_t i = 0; i < num_labels; i++) {
      gradients.emplace_back(grad);
    }
    grad.release();

    // -----------------------------------------
    INFO << "Done!";

    INFO << "Encoding the zero one plaintext... ";
    Plaintext zo;
    encode_zero_one(eparms_ids.at(epoch * depth + 1), params.scale, encoder,
                    params.hash_size, zo);
    weights.set_zero_one(zo);
    // release memory
    zo.release();
    INFO << "Done!";

    tstart(t2);

    INFO << " ---- Beginning training! ---- ";
    double num_tiles = (double)params.batch_size / (double)params.max_tile_size;
    INFO << " There are " << (int)num_tiles << " tiles in this batch";
    for (size_t tile = 0; tile < num_tiles; ++tile) {
      getline(in_info_file, line);

      check_for_file(files.ciphertext_location + files.ciphertext_train_name +
                     line + ".ct");
      check_for_file(files.ciphertext_location + files.ciphertext_train_name +
                     line + "_labels.txt");
      check_for_file(files.ciphertext_location + files.ciphertext_train_name +
                     "0000_label_pool.ct");
      ifstream entry_stream(files.ciphertext_location +
                            files.ciphertext_train_name + line + ".ct");
      ifstream label_stream(files.ciphertext_location +
                            files.ciphertext_train_name + line + "_labels.txt");
      ifstream pool_stream(files.ciphertext_location +
                           files.ciphertext_train_name + "0000_label_pool.ct");

      INFO << " - Loading in tile " << line << "... ";
      EncData in_data(entry_stream, label_stream, pool_stream, context,
                      hash_size, num_labels);
      in_data.mod_switch_to(evaluator, eparms_ids.at(epoch * depth));
      INFO << "Done!";

      INFO << " - Grabbing the label pool and mod_switching... ";
      // ---------------- Grab the label pool -----------
      vector<Ciphertext> label_pool;
      in_data.get_label_pool(label_pool);
      for (auto& ct : label_pool) {
        evaluator.mod_switch_to_inplace(ct, eparms_ids.at(epoch * depth + 2));
      }
      // ------------------------------------------------
      INFO << "Done!";

      {
        dispatch_queue acc(" ---- Accumulator ---- ", 1);
        {
          dispatch_queue q(" --- Tile " + line + " --- ", params.num_threads);
          INFO << " - Using " << params.num_threads << " threads!";
          for (size_t i = 0; i < in_data.get_num_examples(); ++i) {
            Ciphertext data_entry;
            in_data.get_an_entry(i, data_entry);
            vector<int> chunk_labels;
            in_data.retrieve_labels(i, chunk_labels);

            q.dispatch([data_entry, chunk_labels, &label_pool,
                        &ls_over_bs_pt, num_labels, num_packed, &params,
                        &keygen, &evaluator, &weights, &gradients, &acc] {
              batch(data_entry, chunk_labels, weights, label_pool,
                    num_labels, num_packed, params, keygen, evaluator,
                    ls_over_bs_pt, gradients, acc);
            });
          }
        }
      }
      INFO << "Finished tile!";
      // destroy ciphertexts
      // vector<Ciphertext>().swap(label_pool);

      // Check if we've reached the end of the info file
      if (in_info_file.eof()) {
        // Reset in_info_file to the beginning
        in_info_file.clear();
        in_info_file.seekg(0);
        while (line != "*****") {
          getline(in_info_file, line);
        }
      }
    }
    INFO << "Done training! ";
    tstop(t2);

    INFO << "Computing momentum value... ";
    Plaintext momentum_pt;
    // Encode gamma
    encoder.encode(params.momentum_coeff, eparms_ids.at(epoch * depth + 2),
                   params.scale, momentum_pt);
    for (size_t i = 0; i < num_labels; i++) {
      // Step 6
      evaluator.mod_switch_to_inplace(momentum.at(i),
                                      eparms_ids.at(epoch * depth + 2));
      // Step 7
      evaluator.multiply_plain_inplace(momentum.at(i), momentum_pt);
      evaluator.rescale_to_next_inplace(momentum.at(i));
      momentum.at(i).scale() = params.scale;
      // Step 9
      evaluator.add_inplace(momentum.at(i), gradients.at(i));
    }
    // release memory
    momentum_pt.release();
    INFO << "Done!";

    INFO << "Mod switching weights and updating... ";
    weights.mod_switch_to(evaluator, eparms_ids.at(epoch * depth + 3));
    weights.update_gradients(evaluator, gradients);
    INFO << "Done!";

    // FIXME weights should be named after epoch, not line

    string weight_file_name = files.weight_name + "_epoch" +
                              string(2 - to_string(epoch).length(), '0') +
                              to_string(epoch) + ".wt";
    INFO << "Saving weights to " << files.weights_location + weight_file_name
         << "... ";
    weights.save(files.weights_location + weight_file_name);

    if (save_weight_names) {
      ofstream predict_file(predict_file_name, ios::app);
      predict_file << weight_file_name << endl;
      predict_file.close();
      INFO << "Recorded weight name";
    }

    INFO << "Done!";
  }

  INFO << "Finished training! Looks like we are exiting gracefully!";

  INFO << "Saving momentum ciphertexts to " << momentum_file_name;
  ofstream momentum_file(momentum_file_name, ios::binary);
  for (auto& ct : momentum) {
    ct.save(momentum_file);
  }
  momentum_file.close();

  return 0;
}
