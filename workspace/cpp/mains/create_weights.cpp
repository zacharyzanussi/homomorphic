#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

int main(int argc, char** argv) {
#ifdef TESTING
  auto_cpu_timer t;
#endif
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  cout << "One" << endl;
  Parameters params;
  Files files;
  cout << "Two" << endl;
  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);
  cout << "Three" << endl;
  
  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *     Creating new weights               " << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << " *     Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

  EncryptionParameters eparms;

  // Load encryption parameters
  loadParams(eparms, files.ciphertext_location + "parms.enc");

  // SEALContext type changed between versions
#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  // keygen object gets the context and location of files
  KeyGen keygen(context, files.ciphertext_location);

  // Loads secret and public key
  // true, true will load stored relin_keys and gal_keys
  cout << "Deserializing keys" << endl;
  keygen.DeSerialize(false, false);
  //   keygen.DeSerialize(true, true);

  // keygen.CreatePublicKeys(false);

  // all the keys are in memory now
  cout << "Keys loaded!" << endl << endl;

  Decryptor decryptor(context, keygen.secret_key());
  Encryptor encryptor(context, keygen.public_key_);
  Evaluator evaluator(context);
  CKKSEncoder encoder(context);
  cout << "Seal instance loaded!" << endl << endl;

  vector<parms_id_type> eparms_ids;

// context has a different type in SEAL 36
#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    eparms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  cout << "Collected " << eparms_ids.size() << " parms_ids:" << endl;

  size_t vector_size;
  bool using_hashing;
  if (params.hash_size == 0) {
    using_hashing = true;
    vector_size = params.num_features + 1;
  } else {
    using_hashing = false;
    vector_size = params.hash_size;
  }

  cout << "We will be using " << params.num_examples
       << " train entries in batches of " << params.batch_size
       << " and loaded in tiles of " << params.max_tile_size << ", with "
       << params.num_test_examples << " test examples " << endl;
  if (using_hashing) {
    cout << "Hashing to " << vector_size;
  } else {
    cout << "No hashing, using vectors of size " << vector_size;
  }
  cout << " with momentum "
       << params.momentum_coeff << ", regularization " << params.regularization
       << " and " << params.epochs << " epochs" << endl
       << endl;

// different type for context
#ifdef SEAL36
  auto current_context = context.first_context_data()->next_context_data();
#else
  auto current_context = context->first_context_data()->next_context_data();
#endif

  size_t act_depth = 0;
  size_t depth = act_depth + 3;
  size_t num_packed = encoder.slot_count() / vector_size;

  EncWeightLayer weights;
  cout << endl << "Preparing weights:" << endl;

  // path and filename for weights
  if (filesystem::create_directory(files.weights_location)) {
    cout << "Created directory " << files.weights_location << endl;
  } else {
    cout << "Using existing directory " << files.weights_location
         << " to store weights" << endl;
  }

  if (argc < 3) {
    cout << "Please give the starting epoch as an integer after the json "
            "file...  Returning error"
         << endl
         << endl;
    return 1;
  }

  size_t initial_epoch = atoi(argv[2]);
  size_t initial_level = initial_epoch * depth;

  // check if weight file exists, if not we generate the weights
  //   cout << "Attempting to load weights from " << input_weights_file_name <<
  //   "... "; if (filesystem::exists(input_weights_file_name)) {
  //     cout << "Weights found, loading in." << endl;
  //     weights.load(input_weights_file_name, context);
  //     // weights.print_parms_id();
  //     for (size_t i = 0; i < eparms_ids.size(); i++) {
  //       if (weights.get_parms_id() == eparms_ids.at(i)) {
  //         starting_epoch = i / depth;
  //       }
  //     }
  //     cout << "We start at epoch " << starting_epoch << endl;
  //   } else {  // Make some new ones
  cout << "Generating new weights, with " << endl;
  cout << " --- rows:      " << params.num_labels << endl;
  cout << " --- columns:   " << vector_size << endl;
  cout << " --- submodels: " << num_packed << endl;
  cout << " --- level:     " << initial_level << endl;
  cout << " --- epoch:     " << initial_epoch << endl;
  weights = EncWeightLayer(params.num_labels, vector_size, num_packed,
                           params.scale, encoder, encryptor, "uniform");
  weights.mod_switch_to(evaluator, eparms_ids.at(initial_level));

  string untrained_weights_name =
      files.weights_location + files.initial_weight_name;
  cout << "Done! Saving to " << untrained_weights_name << endl;
  weights.save(untrained_weights_name);
  weights.peekAtMatrixChunks(encoder, decryptor, num_packed, 4);
  cout << endl;

  string momentum_file_name = files.weights_location + files.weight_name + "_momentum.ct";
  cout << "We also create an initial momentum and save it to "
       << momentum_file_name << endl;

  vector<Ciphertext> momentum;
  for (int i = 0; i < params.num_labels; i++) {
    Ciphertext mom;
    encryptor.encrypt_zero(weights.get_parms_id(), mom);
    mom.scale() = params.scale;
    momentum.push_back(mom);
  }
  ofstream momentum_file(momentum_file_name, ios::binary);
  for (auto& ct : momentum) {
    ct.save(momentum_file);
  }
  momentum_file.close();
  cout << "Done!" << endl;
}
