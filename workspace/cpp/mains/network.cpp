#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "json.hpp"
#include "neural.h"

using namespace std;

int main(int argc, char **argv) {
  if (argc < 8) {
    cout << "Arguments: num_ngrams, num_labels, batch_size, learning_rate, "
            "epochs, record_name, data_set, (hashor), (activation), "
            "(regularization), (momentum), (LR2)"
         << endl;
    cout << "Sample run: ./network 14212 5 256 5 10 garbage.txt 0 8192 sigmoid "
            "0 0.95"
         << endl;
    return 1;
  }  // Hi there

  cout << " ************************************************** " << endl;
  cout << " ***   Beginning a new cleartext network test   *** " << endl;
  cout << " ***   with parameters                          *** " << endl;
  cout << " ***   ngram lab batch LR epoch record set hash activ regl momentum"
       << endl
       << " ***   ";
  for (int i = 1; i < argc; i++) {
    cout << argv[i] << " ";
  }
  cout << endl
       << " ************************************************** " << endl;

  //// Get input parameters
  size_t num_ngrams = atoi(argv[1]);
  size_t input_dim = num_ngrams + 1;
  size_t num_labels = atoi(argv[2]);
  size_t num_examples = 39995;
  double num_test_examples = 10000;

  size_t batch_size = atoi(argv[3]);
  double learning_rate = atof(argv[4]);
  size_t epochs = atoi(argv[5]);
  string record_name = argv[6];
  string data_set = argv[7];
  size_t hashor = 0;
  double regularization = 0;
  string activation = "sigmoid";
  double momentum_coeff = 0;
  double learning_rate_two = learning_rate;
  size_t switch_lr = 1000;
  if (argc > 8) {
    hashor = atoi(argv[8]);
    cout << "Hashing to " << hashor << endl;
  }
  if (argc > 9) {
    activation = argv[9];
  }
  if (argc > 10) {
    regularization = atof(argv[10]);
    cout << "Using L2 regularization with parameter " << regularization << endl;
  }
  if (argc > 11) {
    momentum_coeff = atof(argv[11]);
    cout << "Using momentum with coefficient " << momentum_coeff << endl;
  }
  if (argc > 12) {
    learning_rate_two = atof(argv[12]);
    cout << "Switching to learning rate " << learning_rate_two << " at epoch "
         << switch_lr << endl;
  }
  if (hashor > 0) {
    input_dim = hashor;
  }
  ////
  cout << "Using activation function " << activation << endl;
  cout << "Performing " << epochs << " epochs" << endl;
  // Load in data
  /// LOCAL
  // string file_name =
  // "/home/zzanussi/Documents/data/homomorphic/napcs_label/reduced/k_fold/k-fold_train_"
  // + data_set + ".csv";
  // string file_name =
  // /"/home/zzanussi/Documents/data/homomorphic/napcs_label/reduced/k_fold/end_test.csv";
  // string test_file_name =
  // "/home/zzanussi/Documents/data/homomorphic/napcs_label/reduced/k_fold/k-fold_test_"
  // + data_set + ".csv"; string record_file_name =
  // "/home/zzanussi/Dropbox/work/homomorphic/workspace/cpp/results/" +
  // record_name;
  //// --------- LOCAL PYTORCH -----------------
  // string file_name = "/data/pytorch_training.csv";
  // string test_file_name = "/data/pytorch_testing.csv";
  // string record_file_name = "/ct/test/weights/clear_records.csv";
  //// --------- CLOUD PYTORCH -----------------
  string file_name = "/data/k_fold/pytorch_training.csv";
  string test_file_name = "/data/k_fold/pytorch_testing.csv";
  string record_file_name = "/home/cleartext/clear_records.csv";
  /// CLOUD
  // string file_name = "/home/data/k_fold/k-fold_train_" + data_set + ".csv";
  // string test_file_name = "/home/data/k_fold/k-fold_test_" + data_set +
  // ".csv"; string record_file_name = "/home/work/results/" + record_name;
  // string file_name =
  //     "/home/ben/extra/StatCan/HE/data/reduced/k_fold/k-fold_train_" +
  //     data_set + ".csv";
  // string test_file_name =
  //     "/home/ben/extra/StatCan/HE/data/reduced/k_fold/k-fold_test_" + data_set +
  //     ".csv";
  // string record_file_name =
  //     "/home/ben/extra/StatCan/HE/data/reduced/results/" + record_name;

  cout << "Loading data" << endl;

  auto start = chrono::high_resolution_clock::now();
  cout << " - Train data" << endl;
  Data train_data(file_name, num_ngrams, num_labels, num_examples, hashor);
  auto end = chrono::high_resolution_clock::now();
  cout << "Loaded " << train_data.get_num_examples() << " entries" << endl;
  auto duration_load = chrono::duration_cast<chrono::milliseconds>(end - start);
  cout << " - Test data" << flush;
  Data test_data(test_file_name, num_ngrams, num_labels, num_test_examples,
                 hashor);
  cout << endl
       << "Loaded " << test_data.get_num_examples() << " entries" << endl
       << endl;

  // initialize network
  NeuralNet network({input_dim, num_labels}, learning_rate, regularization,
                    momentum_coeff, activation);
  network.print_network(3);
  cout << endl;

  network.inspect_network(-8, 10);
  cout << "Sum of the weights is " << network.sum_weights() << endl;

  // opening prediction
  cout << "Opening prediction" << endl;
  double correct = network.predict(test_data);
  cout << " - Number of correct predictions out of " << num_test_examples
       << ": " << correct << endl
       << endl;

  // Data garbage;
  // train_data.get_mini_batch(39990, 20, garbage);
  // cout << garbage.get_num_examples() << endl;
  // vector<vector<double>> dat;
  // garbage.get_data(dat);
  // for (auto & vec : dat) {
  //     print_vector(vec, 10);
  // }
  vector<double> accuracies = {correct / (double)num_test_examples};
  vector<double> weights_sums = {network.sum_weights()};

  vector<vector<double>> momentum;
  initialize_momentum(momentum, num_labels, input_dim);

  cout << "Beginning training: " << endl;
  start = chrono::high_resolution_clock::now();
  double correct_half, accuracy_half;
  for (size_t epoch = 0; epoch < epochs; epoch++) {
    cout << " - Epoch " << epoch << endl;
    vector<vector<double>> save_weights;
    if (epoch != 0) {
      network.apply_momentum(momentum);
    }
    if (epoch == switch_lr) {
      network.set_lr(learning_rate_two);
      cout << "Switched to learning rate " << learning_rate_two << endl;
    }
    Data batch;
    vector<vector<double>> activations, gradients;
    for (size_t i = 0; i < num_examples / batch_size; i++) {
      train_data.get_mini_batch(i * batch_size, batch_size, batch);
      // cout << batch.get_num_examples() << " " << flush;
      network.propagate_forward(batch, activations);
      network.propagate_backward(batch, activations, gradients);
      network.update_gradients(gradients);
      network.update_momentum(gradients, momentum);
    }
    if (num_examples % batch_size != 0) {
      train_data.get_mini_batch(floor(num_examples / batch_size) * batch_size,
                                batch_size, batch);
      // cout << " finally, " << batch.get_num_examples() << " " << flush;
      network.propagate_forward(batch, activations);
      network.propagate_backward(batch, activations, gradients);
      network.update_gradients(gradients);
      network.update_momentum(gradients, momentum);
    }

    // if (epoch >= (double) epochs / 2 && epoch < (double) epochs / 2 + 1)  {
    correct_half = network.predict(test_data);
    accuracy_half = correct_half / num_test_examples;
    accuracies.push_back(accuracy_half);
    cout << " -- " << accuracy_half << "% accuracy at epoch " << epoch << endl;
    network.inspect_network(-8, 10);
    cout << "Sum of the weights is " << network.sum_weights() << endl << endl;
    weights_sums.push_back(network.sum_weights());

    //}
  }
  end = chrono::high_resolution_clock::now();
  cout << "Finished training!" << endl;
  auto duration_train =
      chrono::duration_cast<chrono::milliseconds>(end - start);
  double correct_end = network.predict(test_data);
  double accuracy_end = correct_end / num_test_examples;
  // accuracies.push_back(accuracy_end);
  cout << "Number of correct predictions out of " << num_test_examples << ": "
       << correct_end << ", " << accuracy_end << "%" << endl;
  cout << "Training took " << duration_train.count() << " milliseconds" << endl;
  cout << "Loading the dataset took " << duration_load.count()
       << " milliseconds" << endl;

  ofstream results(record_file_name, ios::app);
  stringstream result_stream;
  result_stream << data_set << "," << epochs << "," << activation << ","
                << hashor << "," << num_examples << "," << batch_size << ","
                << learning_rate << "," << learning_rate_two << ","
                << regularization << "," << momentum_coeff << ","
                << duration_load.count() << "," << duration_train.count()
                << ",{";
  for (auto &accur : accuracies) {
    result_stream << accur << ",";
  }
  result_stream << "},{";
  for (auto &sum : weights_sums) {
    result_stream << (int)sum << ",";
  }
  result_stream << "},";
  auto end_time = chrono::system_clock::to_time_t(chrono::system_clock::now());
  result_stream << ctime(&end_time);

  cout << "Printing record " << result_stream.str() << " to "
       << record_file_name << endl;
  results << result_stream.str();
  // set,epochs,activ,hash,num_ex,batch,LR,LR2,regular,momentum,load_time,train_time,accuracy,model_weight,date

  // network.print_network(10);
  return 0;
}