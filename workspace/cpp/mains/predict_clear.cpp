#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "log.h"
#include "neural.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

int main(int argc, char** argv) {
#ifdef TESTING
  auto_cpu_timer t;
#endif
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  Parameters params;
  Files files;

  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);

  // init logger
  clog::init(files.weights_location + files.predict_weight_name);
  logging::add_common_attributes();

  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << "      Performig a prediction (clear)      " << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << "      Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

  EncryptionParameters eparms;

  // Load encryption parameters
  loadParams(eparms, files.ciphertext_location + "parms.enc");

  // SEALContext type changed between versions
#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  // keygen object gets the context and location of files
  KeyGen keygen(context, files.ciphertext_location);

  // Loads secret and public key
  // true, true will load stored relin_keys and gal_keys
  INFO << "Deserializing keys" << endl;
  // keygen.DeSerialize(true, true);
  keygen.DeSerialize(false, false);

  // keygen.CreatePublicKeys(false);

  // all the keys are in memory now
  cout << "Keys loaded!" << endl << endl;

  size_t num_labels = params.num_labels;
  size_t vector_size;
  bool using_hashing;
  if (params.hash_size == 0) {
    using_hashing = true;
    vector_size = params.num_features + 1;
  } else {
    using_hashing = false;
    vector_size = params.hash_size;
  }
  size_t hash_size = params.hash_size;

  Decryptor decryptor(context, keygen.secret_key());
  Encryptor encryptor(context, keygen.public_key_);
  Evaluator evaluator(context);
  CKKSEncoder encoder(context);
  INFO << "Seal instance loaded!" << endl;

  vector<parms_id_type> eparms_ids;

// context has a different type in SEAL 36
#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    eparms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  INFO << "Collected " << eparms_ids.size() << " parms_ids:";

  INFO << " ---------------------------------------------------- ";
  INFO << " -     Beginning prediction, in the cleartext       - " << endl;

  INFO << "Beginning the testing protocol. While we are loading in encrypted "
          "weights, we will "
          "unencrypt and use cleartext testing data. This will be faster, and "
          "we won't have to "
          "worry about the algorithm for now. Keep in mind that the true "
          "prediction process needs "
          "to take place in the ciphertext"
       << endl;

  string extension =
      files.predict_weight_name.substr(files.predict_weight_name.size() - 3);
  vector<string> predict_names;
  if (extension == ".wt") {
    INFO << "Predict weight name appears to have a .wt extension: running "
            "prediction on a single set of weights";
    predict_names.push_back(files.predict_weight_name);
  } else if (extension == "txt") {
    INFO << "Predict weight name appears to have a .txt extension: running "
            "prediction on the contents of this file";
    string weights_list_name =
        files.weights_location + files.predict_weight_name;
    if (!filesystem::exists(weights_list_name)) {
      ERR << "Didn't find any file at " << weights_list_name
          << ": returning error";
      return 1;
    }
    ifstream weights_list(weights_list_name);
    string weights_name;
    while (getline(weights_list, weights_name)) {
      predict_names.push_back(weights_name);
    }
    weights_list.close();
    INFO << "Found " << predict_names.size()
         << " weights to perform prediction on";
  } else {
    ERR << "Did not recognize Predict weight name with extension " << extension
        << ": returning error";
    return 1;
  }

  string record_file_name = files.weights_location + files.records_file;
  ofstream record_file(record_file_name, ios::app);
  INFO << "Recording to " << record_file_name;

  INFO << "Loading data... ";
  Data test_data(files.test_file, params.num_features, num_labels, params.num_test_examples,
                 hash_size);
  size_t num_entries = test_data.get_num_examples();
  INFO << "Loaded " << num_entries << " entries to predict on";

  for (auto& predict_name : predict_names) {
    string weights_file_name = files.weights_location + predict_name;
    INFO << "Attempting to load weights from " << weights_file_name;
    EncWeightLayer weights;
    if (filesystem::exists(weights_file_name)) {
      weights.load(weights_file_name, context);
      weights.set_num_packed(encoder.slot_count() / vector_size);
      cout << "Weights loaded! Take a peek:" << endl;
      weights.peekAtMatrixChunks(encoder, decryptor);
    } else {
      ERR << "Couldn't find " << weights_file_name << ": try again!";
      return 1;
    }

    vector<vector<double>> weight_vecs;
    weights.decrypt(encoder, decryptor, weight_vecs);

    vector<WeightLayer> submodels;
    for (size_t i = 0; i < encoder.slot_count(); i += vector_size) {
      vector<vector<double>> submodel_vec;
      for (size_t j = 0; j < num_labels; ++j) {
        vector<double> subrow(weight_vecs.at(j).begin() + i,
                              weight_vecs.at(j).begin() + i + vector_size);
        submodel_vec.push_back(subrow);
      }
      WeightLayer submodel(submodel_vec);
      submodels.push_back(submodel);
    }

    vector<double> sums, abs_sums;
    for (auto& submodel : submodels) {
      sums.push_back(submodel.sum_weights());
      abs_sums.push_back(submodel.abs_val_weights());
    }

    // for (auto & submodel : submodels) {
    //   submodel.print_weights(4);
    // }

    INFO << "Computing predictions for the submodels... ";
    vector<vector<vector<double>>> all_activations;
    for (auto& submodel : submodels) {
      vector<vector<double>> activations;
      submodel.propagate_past(test_data, activations);
      all_activations.push_back(activations);
    }

    // cout << endl << "test size " << params.num_test_examples;
    // cout << endl << "num entries " << num_entries;

    INFO << "Computing the ensemble predictions... ";
    vector<vector<double>> combined_activations;
    for (size_t j = 0; j < params.num_test_examples;
         ++j) {  // iterate over the rows
      // for (size_t j = 0; j < num_entries; ++j) { // iterate over the rows
      vector<double> row_sum;
      for (size_t k = 0; k < num_labels; ++k) {  // iterate over all values
        double sum = 0;
        for (size_t i = 0; i < all_activations.size();
             ++i) {  // iterate over all models
          sum += all_activations.at(i).at(j).at(k);
        }
        row_sum.push_back(sum);
      }
      combined_activations.push_back(row_sum);
    }

    // Now, use tally_predictions to figure out the accuracy of each model and
    // the ensemble, and return the results
    INFO << "Tallying predictions for the submodels... ";
    vector<vector<double>> labels;
    test_data.get_labels(labels);
    vector<int> sub_tallies;
    for (auto& act : all_activations) {
      int tally = tally_activations(act, labels);
      sub_tallies.push_back(tally);
    }

    int ensemble_tally = tally_activations(combined_activations, labels);

    RES << " ******** Final results are in: ********";
    string res;
    for (auto& val : sub_tallies) {
      res += to_string(val) + " ";
    }
    RES << "The four sub models got " << res << " entries correct, out of "
        << params.num_test_examples << " total";
    RES << "The combined model got " << ensemble_tally << " correct";

    vector<double> percs;
    res.clear();
    for (auto& val : sub_tallies) {
      double perc = 100 * val / static_cast<double>(num_entries);
      res += to_string(perc) + "% ";
      percs.push_back(perc);
    }
    double perc = 100 * ensemble_tally / static_cast<double>(num_entries);
    res += "and " + to_string(perc) + "%.";
    RES << "The percentages are " << res;
    percs.push_back(perc);

    res.clear();
    for (auto& val : sums) {
      res += to_string(val) + " ";
    }
    RES << "Sums of weights: " << res;

    res.clear();
    for (auto& val : abs_sums) {
      res += to_string(val) + " ";
    }
    RES << "Absolute values of weights: " << res;
    INFO << " ***************************************" << endl << endl;

    for (auto& perc : percs) {
      record_file << perc << ",";
    }
    record_file << "\t";
    for (auto& sum : sums) {
      record_file << sum << ",";
    }
    record_file << "\t";
    for (auto& sum : abs_sums) {
      record_file << sum << ",";
    }
    record_file << endl;
  }
}