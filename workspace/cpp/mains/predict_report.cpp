#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "log.h"
#include "neural.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

int main(int argc, char** argv) {
#ifdef TESTING
  auto_cpu_timer t;
#endif
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  Parameters params;
  Files files;

  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);

  // init logger
  clog::init(files.weights_location + "report_" + files.predict_weight_name);
  logging::add_common_attributes();

  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << "      Performing a prediction report               " << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << "      Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

  EncryptionParameters eparms;

  // Load encryption parameters
  loadParams(eparms, files.ciphertext_location + "parms.enc");

  // SEALContext type changed between versions
#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  // keygen object gets the context and location of files
  KeyGen keygen(context, files.ciphertext_location);

  // Loads secret and public key
  // true, true will load stored relin_keys and gal_keys
  INFO << "Deserializing keys" << endl;
  // keygen.DeSerialize(true, true);
  keygen.DeSerialize(false, false);

  // keygen.CreatePublicKeys(false);

  // all the keys are in memory now
  cout << "Keys loaded!" << endl << endl;

  size_t num_labels = params.num_labels;
  size_t vector_size;
  bool using_hashing;
  if (params.hash_size == 0) {
    using_hashing = true;
    vector_size = params.num_features + 1;
  } else {
    using_hashing = false;
    vector_size = params.hash_size;
  }
  size_t hash_size = params.hash_size;

  Decryptor decryptor(context, keygen.secret_key());
  Encryptor encryptor(context, keygen.public_key_);
  Evaluator evaluator(context);
  CKKSEncoder encoder(context);
  INFO << "Seal instance loaded!" << endl;

  vector<parms_id_type> eparms_ids;

// context has a different type in SEAL 36
#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    eparms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  INFO << "Collected " << eparms_ids.size() << " parms_ids:";

  INFO << " ---------------------------------------------------- ";
  INFO
      << " -     Beginning prediction, in the cleartext, with a report       - "
      << endl;

  INFO << "Beginning the testing protocol, reporting data on which entries "
          "were correctly or incorrectly classified."
       << endl;

  string extension =
      files.predict_weight_name.substr(files.predict_weight_name.size() - 3);
  string predict_name;
  if (extension == ".wt") {
    INFO << "Predict weight name appears to have a .wt extension: running "
            "prediction on a single set of weights";
    predict_name = files.weights_location + files.predict_weight_name;
  } else if (extension == "txt") {
    INFO << "Predict weight name appears to have a .txt extension: running "
            "prediction on the last set of weights in this file";
    ifstream predict_file(files.weights_location + files.predict_weight_name);
    string weight_name, prev_weight_name;
    while (getline(predict_file, weight_name)) {
      prev_weight_name = weight_name;
    }
    predict_name = files.weights_location + prev_weight_name;
    predict_file.close();
  } else {
    ERR << "Did not recognize Predict weight name with extension " << extension
        << ": returning error";
    return 1;
  }

  INFO << "Loading data... ";
  Data test_data(files.test_file, params.num_features, num_labels, params.num_test_examples,
                 hash_size, true);
  double num_entries = test_data.get_num_examples();

  INFO << "Attempting to load weights from " << predict_name;
  EncWeightLayer weights;
  if (filesystem::exists(predict_name)) {
    weights.load(predict_name, context);
    weights.set_num_packed(encoder.slot_count() / vector_size);
    cout << "Weights loaded! Take a peek:" << endl;
    weights.peekAtMatrixChunks(encoder, decryptor);
  } else {
    ERR << "Couldn't find " << predict_name << ": try again!";
    return 1;
  }

  vector<vector<double>> weight_vecs;
  weights.decrypt(encoder, decryptor, weight_vecs);

  vector<WeightLayer> submodels;
  for (size_t i = 0; i < encoder.slot_count(); i += vector_size) {
    vector<vector<double>> submodel_vec;
    for (size_t j = 0; j < num_labels; ++j) {
      vector<double> subrow(weight_vecs.at(j).begin() + i,
                            weight_vecs.at(j).begin() + i + vector_size);
      submodel_vec.push_back(subrow);
    }
    WeightLayer submodel(submodel_vec);
    submodels.push_back(submodel);
  }

  string correct_file_name = files.weights_location + "correct.csv";
  string incorrect_file_name = files.weights_location + "incorrect.csv";
  ofstream correct_file(correct_file_name);
  ofstream incorrect_file(incorrect_file_name);
  INFO << "Logging to " << correct_file_name << " and " << incorrect_file_name;

  int correct = 0;
  int incorrect = 0;
  for (size_t i = 0; i < num_entries; ++i) {
    vector<double> entry, label;
    string desc;
    test_data.get_ith(i, entry, label, desc);
    // cout << desc << endl;
    vector<int> submodel_predictions;
    vector<vector<double>> submodel_activations;
    for (auto& submodel : submodels) {
      vector<double> activations;
      submodel.propagate_past(entry, activations);
      submodel_predictions.push_back(argmax(activations));
      submodel_activations.push_back(activations);
    }
    vector<double> ensemble_activations;
    for (size_t lab = 0; lab < num_labels; ++lab) {
      double sum = 0;
      for (size_t model = 0; model < submodels.size(); ++model) {
        sum += submodel_activations.at(model).at(lab);
      }
      ensemble_activations.push_back(sum);
    }
    int ensemble_prediction = argmax(ensemble_activations);
    int true_label = argmax(label);

    string ensemble_act_str = "{";
    for (auto& val : ensemble_activations) {
      stringstream s;
      s << fixed << setprecision(2) << val;
      ensemble_act_str += s.str() + ",";
    }
    ensemble_act_str += "}";

    string submodel_pred_strings = "{";
    for (auto& val : submodel_predictions) {
      submodel_pred_strings += to_string(val) + ",";
    }
    submodel_pred_strings += "}";

    if (ensemble_prediction == true_label) {
      correct_file << true_label << "," << ensemble_prediction << "," << desc
                   << " ," << ensemble_act_str << "," << submodel_pred_strings
                   << endl;
      ++correct;
    } else {
      incorrect_file << true_label << "," << ensemble_prediction << "," << desc
                     << " ," << ensemble_act_str << "," << submodel_pred_strings
                     << endl;
      ++incorrect;
    }
  }
  correct_file.close();
  incorrect_file.close();
  INFO << "Obtained " << correct << " correct and " << incorrect
       << " incorrect";
}