#include <filesystem>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>

#include "dispatch.hpp"
#include "enc_neural.h"
#include "io.h"
#include "keygen.h"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

int main(int argc, char** argv) {
#ifdef TESTING
  auto_cpu_timer t;
#endif
  // Instantiate Parameters and Files objects
  // They will contain the model's parameters and
  // files for I/O
  Parameters params;
  Files files;

  // Parse command line options and JSON files and
  // store them in params and files
  PSOptions options(params, files, argc, argv);

  auto start_date =
      chrono::system_clock::to_time_t(chrono::system_clock::now());
  ;

  cout << " *********************************************************** "
       << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *     Refreshing weights               " << endl;
  params.Print();
  files.Print();
  cout << endl;
  cout << " *     Start time: " << ctime(&start_date) << endl;
  cout << " *                                                         * "
       << endl;
  cout << " *********************************************************** "
       << endl;

  EncryptionParameters eparms;

  // Load encryption parameters
  loadParams(eparms, files.ciphertext_location + "parms.enc");

  // SEALContext type changed between versions
#ifdef SEAL36
  SEALContext context(eparms, true, SEC_LEVEL);
#else
  auto context = SEALContext::Create(eparms, true, SEC_LEVEL);
#endif

  // keygen object gets the context and location of files
  KeyGen keygen(context, files.ciphertext_location);

  // Loads secret and public key
  // true, true will load stored relin_keys and gal_keys
  cout << "Deserializing keys" << endl;
  keygen.DeSerialize(false, false);
  //   keygen.DeSerialize(true, true);

  // keygen.CreatePublicKeys(false);

  // all the keys are in memory now
  cout << "Keys loaded!" << endl << endl;

  Decryptor decryptor(context, keygen.secret_key());
  Encryptor encryptor(context, keygen.public_key_);
  Evaluator evaluator(context);
  CKKSEncoder encoder(context);
  cout << "Seal instance loaded!" << endl << endl;

  vector<parms_id_type> eparms_ids;

// context has a different type in SEAL 36
#ifdef SEAL36
  auto count_context = context.first_context_data();
#else
  auto count_context = context->first_context_data();
#endif

  while (count_context) {
    eparms_ids.push_back(count_context->parms_id());
    count_context = count_context->next_context_data();
  }
  cout << "Collected " << eparms_ids.size() << " parms_ids:" << endl;

  size_t vector_size;
  bool using_hashing;
  if (params.hash_size == 0) {
    using_hashing = true;
    vector_size = params.num_features + 1;
  } else {
    using_hashing = false;
    vector_size = params.hash_size;
  }
  int rot = 1;
  if (argc >= 3) {
    rot = atoi(argv[2]);
    cout << endl << "Using a rotation of " << rot << endl;
  } else {
    cout << endl << "Using default rotation of 1 to the left. " << 
                    "Enter an integer after the json to change this. " << 
                    "To rotate right, use num_chunks - steps_right" << endl;
  }
  cout << endl;

  cout << "We will be using " << params.num_examples
       << " train entries in batches of " << params.batch_size
       << " and loaded in tiles of " << params.max_tile_size << ", with "
       << params.num_test_examples << " test examples " << endl;
  if (using_hashing) {
    cout << "Hashing to " << vector_size;
  } else {
    cout << "No hashing, using vectors of size " << vector_size;
  }
  cout << " with momentum "
       << params.momentum_coeff << ", regularization " << params.regularization
       << " and " << params.epochs << " epochs" << endl
       << endl;

// different type for context
#ifdef SEAL36
  auto current_context = context.first_context_data()->next_context_data();
#else
  auto current_context = context->first_context_data()->next_context_data();
#endif

  size_t num_packed = encoder.slot_count() / vector_size;

  EncWeightLayer weights;
  cout << endl << "Preparing weights:" << endl;

  string extension =
      files.predict_weight_name.substr(files.predict_weight_name.size() - 3);
  string weights_to_refresh;
  if (extension == ".wt") {
    cout << "Predict weight name has a .wt extension: refreshing it!" << endl;
    weights_to_refresh = files.weights_location + files.predict_weight_name;
  } else if (extension == "txt") {
    cout << "Predict weight name has a .txt extension: refreshing the last "
            "element in the list!"
         << endl;
    ifstream predict_file(files.weights_location + files.predict_weight_name);
    string weight_name, prev_weight_name;
    while (getline(predict_file, weight_name)) {
      prev_weight_name = weight_name;
    }
    weights_to_refresh = files.weights_location + prev_weight_name;
    predict_file.close();
  }

  // path and filename for weights
  if (!filesystem::exists(weights_to_refresh)) {
    cout << "Can't find weights " << weights_to_refresh
         << ": returning in disgrace" << endl;
    return 1;
  }

  cout << "Found weights " << weights_to_refresh << ": preparing to refresh!"
       << endl;

  weights.load(weights_to_refresh, context);

  cout << endl << "Take a peek: " << endl;
  weights.peekAtMatrixChunks(encoder, decryptor, num_packed, 4);

  string rot_str;
  if (rot > 0) {
    rot_str = " rotating " + to_string(abs(rot)) + " chunk left";
  } else {
    rot_str = " not rotating";
  }
  cout << "Refreshing and" << rot_str << endl;
  weights.refresh_and_rotate(encoder, decryptor, encryptor, num_packed, rot);

  cout << endl << "Compare with the refreshed weights: " << endl;
  weights.peekAtMatrixChunks(encoder, decryptor, num_packed, 4);

  // Remove the ".wt" from the end
  weights_to_refresh.erase(weights_to_refresh.size() - 3);
  string refreshed_weights_name = weights_to_refresh + "_refreshed.wt";
  cout << endl << "Saving to " << refreshed_weights_name << endl;
  weights.save(refreshed_weights_name);
  cout << "Saved!" << endl;

  string momentum_file_name = files.weights_location /*+ files.weight_name + "_" */ + "momentum.ct";
  if (!filesystem::exists(momentum_file_name)) {
    return 0;
  }
  cout << "Found momentum at " << momentum_file_name
       << ": refreshing and rotating" << endl;

  ifstream momentum_file(momentum_file_name, ios::binary);
  vector<Ciphertext> momentum;
  while (1) {
    try {
      Ciphertext ct;
      ct.load(context, momentum_file);
      momentum.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }
  momentum_file.close();

  refresh_and_rotate_vector(momentum, encoder, decryptor, encryptor,
                            num_packed, rot);

  ofstream momentum_file_o(momentum_file_name, ios::binary);
  for (auto& ct : momentum) {
    ct.save(momentum_file_o);
  }
  momentum_file.close();
  cout << "Refreshed weights saved to " << momentum_file_name << endl;
}