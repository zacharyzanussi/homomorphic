Here is a compilation of the results files that we have available.
I'm still not sure how to make it available across branches. 

Results files that existed before this readme in branch tiling_hack;
- cloud_cipher_records.csv
- cloud_cipher_regularization_records.csv
- cloud_cipher_thanksgiving_records.csv
- results_1.csv

Documented Results: 
- cloud_cipher_nesterov.csv
    Results of the first tests with Nesterov. Consists of tests with 1000 train
    with a variety of nesterov values. 