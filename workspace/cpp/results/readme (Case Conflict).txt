In this file, we document the different results files we have.

Sample columns for a more recent test;
set,epochs,activation,hashor,batch_size,learning_rate,regularization,duration_load,duration_train,accuracies,weights,end_time

 --- cloud_clear_batch.csv
  Trying to determine if a different batch size would improve the results in cleartext.
  Data normalization was used here; it seems like this is a major hindrance in the 
  clear, but necessary in the cipher.

 --- clear_nesterov.csv
  Testing the new nesterov stuff and trying to do some hyperparameter tuning.
  Uses momentum values of 0, 0.8, 0.9, and 0.95, and LR of 3.5, 5, and 7.5.

 --- clear_results.csv
  Assorted tests of the cleartext network, ranging over several weeks (maybe months). Always has
  full data set and hash size 8192. Includes some hyperparameter tuning as well as many of the 
  tests where we used data normalization, which we ultimately scrapped. 

 --- cloud_cipher_records.csv
  Some early records from the ciphertext experiments. Mainly parameter tuning, but this is before I found the "5x subtract" bug
  so these are probably not very useful anymore.

 --- cloud_cipher_thanksgiving_records.csv
  Records from the big test we did over Thanksgiving weekend. Ranging from 1000 to 25000 train set, most of these tests
  had gradient exploding and thus aren't that useful anymore.

 --- local_cipher_records.csv 
  A few local cipher results.

 --- results_1.csv
  Empty