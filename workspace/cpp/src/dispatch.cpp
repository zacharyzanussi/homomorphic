#include "dispatch.hpp"

#include <iostream>
#include <thread>

dispatch_queue::dispatch_queue(std::string name, size_t thread_cnt)
    : name_{std::move(name)}, threads_(thread_cnt) {
  // printf("\nCreating dispatch queue: %s", name_.c_str());
  // printf("\nDispatch threads: %zu", thread_cnt);

  for (size_t i = 0; i < threads_.size(); i++) {
    // This constructor takes the function that should be run, but since it's
    // a member function, the object on which it acts, this, must be passed as
    // an argument. So this thread runs
    // this->dispatch_queue::dispatch_thread_handler()
    threads_[i] = std::thread(&dispatch_queue::dispatch_thread_handler, this);
  }
}

dispatch_queue::~dispatch_queue() {
  // printf("\n%s Destructor: Destroying dispatch threads...", name_.c_str());

  // Signal to dispatch threads that it's time to wrap up
  std::unique_lock<std::mutex> lock(lock_);
  quit_ = true;
  lock.unlock();
  cv_.notify_all();

  // Wait for threads to finish before we exit

  for (size_t i = 0; i < threads_.size(); i++) {
    if (threads_[i].joinable()) {
      threads_[i].join();
      // printf("\n%s Destructor: Thread %zu joined", name_.c_str(), i);
    }
  }
}

void dispatch_queue::dispatch(const fp_t& op) {
  std::unique_lock<std::mutex> lock(lock_);
  q_.push(op);
  // std::cout << this->name_ << std::endl;

  // Manual unlocking is done before notifying, to avoid waking up
  // the waiting thread only to block again (see notify_one for details)
  lock.unlock();
  cv_.notify_one();
}

void dispatch_queue::dispatch(fp_t&& op) {
  std::unique_lock<std::mutex> lock(lock_);
  q_.push(std::move(op));
  // std::cout << this->name_ << std::endl;

  // Manual unlocking is done before notifying, to avoid waking up
  // the waiting thread only to block again (see notify_one for details)
  lock.unlock();
  cv_.notify_one();
}

void dispatch_queue::dispatch_thread_handler(void) {
  std::unique_lock<std::mutex> lock(lock_);

  do {
    // Wait until we have data or a quit signal
    cv_.wait(lock, [this] { return (q_.size() || quit_); });

    // after wait, we own the lock
    if (/* !quit_ && */ q_.size()) {
      auto op = std::move(q_.front());
      q_.pop();

      // unlock now that we're done messing with the queue
      lock.unlock();

      op();

      lock.lock();
    }
  } while (!quit_ || !q_.empty());
}
