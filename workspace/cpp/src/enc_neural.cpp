#include "enc_neural.h"

#include <algorithm>
#include <fstream>
#include <sstream>
#include <vector>

#include "dispatch.hpp"
#include "sealFunctions.h"
#include "utility.h"

using namespace std;
using namespace seal;

// class dispatch_queue;
// dispatch_queue::dispatch_queue(std::string name, size_t thread_cnt = 1);
// void dispatch_queue::dispatch(fp_t&& op);

// EncData::EncData() {
//     this->entries = vector<Ciphertext>;

// }

EncData::EncData(ifstream& file, size_t hashor, size_t num_labels,
                 size_t num_examples, CKKSEncoder& encoder, size_t scale,
                 Encryptor& encryptor, const parms_id_type& data_parms_id,
                 const parms_id_type& label_parms_id) {
  string line;
  // Load the entries
  size_t num_lines = 0;
  
  // swapped the comparison, usually if first condition is false
  // no need to evaluate the second, which unnecessarily makes advance one
  // line in the filestream, which is dropped
  while ((num_lines < num_examples) && getline(file, line)) {
    if (line.length() > 0) {
      vector<double> entry;
      load_data_line(line, entry, num_features, hashor, false);

      Plaintext pt;
      encoder.encode(entry, data_parms_id, scale, pt);
      Ciphertext ct;
      encryptor.encrypt(pt, ct);

      entries.push_back(ct);
      // load_label_line(line, label, num_labels);
      // labels.push_back(label);

      // atoi ensures that we capture only the first digits in line
      labels.push_back(atoi(&line[0]));
      num_lines++;
    }
  }

  this->num_examples = entries.size();
  this->num_features = hashor;
  this->num_labels = num_labels;
  this->num_packed = 1;

  // Load the labels
  vector<double> ones_vec;
  ones_vec.resize(hashor, 1);
  Plaintext zeros_pt, ones_pt;
  Ciphertext zeros, ones;

  encoder.encode(0, label_parms_id, scale, zeros_pt);
  encoder.encode(ones_vec, label_parms_id, scale, ones_pt);
  encryptor.encrypt(zeros_pt, zeros);
  encryptor.encrypt(ones_pt, ones);
  label_pool.push_back(zeros);
  label_pool.push_back(ones);
}

// Packing constructor
EncData::EncData(ifstream& file, size_t num_packed, size_t num_features, size_t hashor,
                 size_t num_labels, size_t num_ciphertexts,
                 CKKSEncoder& encoder, size_t scale, Encryptor& encryptor,
                 const parms_id_type& data_parms_id,
                 const parms_id_type& zero_one_parms_id,
                 const parms_id_type& label_parms_id, bool normalize) : num_features(num_features) {
  size_t slot_count = encoder.slot_count();
  if (hashor * num_packed > slot_count) {
    throw invalid_argument(
        "Hashor * num_packed exceeds the number of slots available");
  }

  size_t num_loaded = 0;
  string line;
  vector<string> lines;
  while (getline(file, line) && num_loaded < num_ciphertexts) {
    if (line.length() > 0) {
      lines.push_back(line);
      if (lines.size() == num_packed) {
        vector<double> packed_entry;
        load_data_packed(lines, packed_entry, num_features, hashor, normalize);

        Plaintext pt;
        encoder.encode(packed_entry, data_parms_id, scale, pt);
        Ciphertext ct;
        encryptor.encrypt(pt, ct);

        entries.push_back(ct);

        // Put all the labels consecutively into the labels parameter
        for (auto& line : lines) {
          labels.push_back(atoi(&line[0]));
        }
        ++num_loaded;
        lines.clear();
      }
    }
  }

  this->num_examples = entries.size();
  this->num_features = hashor;
  this->num_labels = num_labels;
  this->num_packed = num_packed;

  size_t chunk_size = slot_count / num_packed;

  // Encode the zero-one plaintext
  vector<double> zerosOnes;
  encode_zero_one(zero_one_parms_id, scale, encoder, chunk_size, zero_one);

  // Encode the several label pools
  for (size_t i = 0; i < num_packed; ++i) {
    vector<double> ones_chunk, zeros_chunk;
    for (size_t chunk = 0; chunk < num_packed; ++chunk) {
      for (size_t slot = 0; slot < chunk_size; ++slot) {
        if (chunk == i) {
          ones_chunk.push_back(1);
        } else {
          ones_chunk.push_back(0);
        }
        zeros_chunk.push_back(0);
      }
    }
    Plaintext ones_chunk_pt, zeros_chunk_pt;
    encoder.encode(zeros_chunk, label_parms_id, scale, zeros_chunk_pt);
    encoder.encode(ones_chunk, label_parms_id, scale, ones_chunk_pt);
    Ciphertext ones_chunk_ct, zeros_chunk_ct;
    encryptor.encrypt(zeros_chunk_pt, zeros_chunk_ct);
    encryptor.encrypt(ones_chunk_pt, ones_chunk_ct);
    label_pool.push_back(zeros_chunk_ct);
    label_pool.push_back(ones_chunk_ct);
  }
}

EncData::EncData(string file_name, size_t hashor, size_t num_labels,
                 size_t num_examples, CKKSEncoder& encoder, size_t scale,
                 Encryptor& encryptor, const parms_id_type& data_parms_id,
                 const parms_id_type& label_parms_id, int burn_in) {
  ifstream data(file_name, ios::in);
  string line;
  for (int i = 0; i < burn_in + 1; i++) {
    getline(data, line);
  }
  *this = EncData(data, hashor, num_labels, num_examples, encoder, scale,
                  encryptor, data_parms_id, label_parms_id);
}

EncData::EncData(vector<Ciphertext>& entries, vector<int>& labels,
                 vector<Ciphertext>& label_pool, size_t num_features,
                 size_t num_labels) {
  if (entries.size() != labels.size()) {
    throw invalid_argument("Number of entries does not match number of labels");
  }
  this->entries = entries;
  this->labels = labels;
  this->label_pool = label_pool;

  this->num_examples = entries.size();
  this->num_features = num_features;
  this->num_labels = num_labels;
}

#ifndef SEAL36
EncData::EncData(ifstream& entries_stream, ifstream& labels_stream,
                 ifstream& pool_stream,
                 const std::shared_ptr<seal::SEALContext>& context,
                 size_t hash_size, size_t num_labels) {
  this->deserialize(entries_stream, labels_stream, pool_stream, context,
                    hash_size, num_labels);
}
#else
EncData::EncData(ifstream& entries_stream, ifstream& labels_stream,
                 ifstream& pool_stream, const SEALContext& context,
                 size_t hash_size, size_t num_labels) {
  this->deserialize(entries_stream, labels_stream, pool_stream, context,
                    hash_size, num_labels);
}
#endif

string EncData::to_string() {
  stringstream s;
  s << "An EncData instance containing:" << endl;
  s << " -- Entries: " << entries.size() << endl;
  s << " -- Label pool ciphertexts: " << label_pool.size() << endl;
  s << " -- Labels: " << labels.size() << endl;
  s << " -- num_packed: " << num_packed << endl;
  return s.str();
}

void EncData::first_and_last(CKKSEncoder& encoder, Decryptor& decryptor,
                             int n) {
  peekAtCiphertext(this->entries.at(0), encoder, decryptor, n);

  peekAtCiphertext(this->entries.at(this->num_examples - 1), encoder, decryptor,
                   n);
}

void bare_load(const string file_name, const size_t hashor,
               const size_t num_examples, CKKSEncoder& encoder,
               const size_t scale, Encryptor& encryptor,
               const parms_id_type& data_parms_id,
               const parms_id_type& label_parms_id, vector<Ciphertext>& entries,
               vector<int>& labels, vector<Ciphertext>& label_pool,
               int burn_in) {
  entries.clear();
  entries.reserve(num_examples);
  labels.clear();
  labels.reserve(num_examples);
  label_pool.clear();

  ifstream data(file_name, ios::in);
  string line, cols;
  vector<double> entry;
  // vector<int> label;
  getline(data, cols);

  for (int i = 0; i < burn_in; i++) {
    getline(data, line);
  }

  // Load the entries
  size_t num_lines = 0;
  while (getline(data, line) && num_lines < num_examples) {
    if (line.length() >= 1) {
      load_data_line(line, entry, 14212, hashor);
      Plaintext pt;
      encoder.encode(entry, data_parms_id, scale, pt);
      Ciphertext ct;
      encryptor.encrypt(pt, ct);
      entries.push_back(ct);
      // load_label_line(line, label, num_labels);
      // labels.push_back(label);

      labels.push_back(atoi(&line[0]));
      num_lines++;
    }
  }
  data.close();

  // Load the labels
  // For now, we will just load
  vector<double> ones_vec;
  ones_vec.resize(hashor, 1);
  Plaintext zeros_pt, ones_pt;
  Ciphertext zeros, ones;

  encoder.encode(0, label_parms_id, scale, zeros_pt);
  encoder.encode(ones_vec, label_parms_id, scale, ones_pt);
  encryptor.encrypt(zeros_pt, zeros);
  encryptor.encrypt(ones_pt, ones);
  label_pool.push_back(zeros);
  label_pool.push_back(ones);
}

void EncData::peekAtFirstChunks(CKKSEncoder& encoder, Decryptor& decryptor,
                                int n) {
  peekAtChunks(this->entries.at(0), encoder, decryptor, this->num_packed, n);
}

void EncData::retrieve_labels(size_t which_ct, vector<int>& chunk_labels) {
  chunk_labels.clear();

  for (size_t i = which_ct * num_packed; i < (which_ct + 1) * num_packed; ++i) {
    chunk_labels.push_back(this->labels.at(i));
  }
}

void EncData::mod_switch_to(Evaluator& evaluator, parms_id_type& parms_id) {
  for (auto& val : this->entries) {
    evaluator.mod_switch_to_inplace(val, parms_id);
  }
}

void EncData::serialize(string file_prefix, bool save_pool) {
  // cout << " - Saving the entries...";
  ofstream ofile(file_prefix + ".ct", ios::binary);
  for (auto& ciphertext : this->entries) {
    ciphertext.save(ofile);
  }
  ofile.close();
  // cout << " Saved" << endl;

  // cout << " - Saving the labels (cleartext, no obscuring)...";
  ofstream label_file(file_prefix + "_labels.txt");
  for (auto& lab : this->labels) {
    label_file << lab << endl;
  }
  label_file.close();
  // cout << " Saved" << endl;

  if (save_pool) {
    // cout << " - Saving the label pool...";
    ofstream label_pool_file(file_prefix + "_label_pool.ct", ios::binary);
    for (auto& lab : this->label_pool) {
      lab.save(label_pool_file);
    }
    label_pool_file.close();
    // cout << " Saved to " << endl;
  }
}

#ifndef SEAL36
// FIXME not updated to load only a subset of the data
void EncData::deserialize(ifstream& entries_stream, ifstream& labels_stream,
                          ifstream& pool_stream,
                          const std::shared_ptr<seal::SEALContext>& context,
                          size_t hash_size, size_t num_labels) {
  // ifstream in_entry_stream(file_prefix + ".ct", ios::binary);
  this->entries.clear();
  while (true) {
    try {
      Ciphertext ct;
      ct.load(context, entries_stream);

      this->entries.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }
  // in_entry_stream.close();

  // ifstream in_label_stream(file_prefix + "_labels.txt");
  this->labels.clear();
  string lab;
  while (getline(labels_stream, lab)) {
    this->labels.push_back(stoi(lab));
  }
  // in_label_stream.close();

  // if (update_pool || this->label_pool.empty()) {
  // ifstream in_label_pool_stream(file_prefix + "_label_pool.ct", ios::binary);
  this->label_pool.clear();
  while (true) {
    try {
      Ciphertext ct;
      ct.load(context, pool_stream);
      this->label_pool.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }
  // in_label_pool_stream.close();
  // }
  int div_num_packed = this->labels.size() / this->entries.size();
  // if (int(div_num_packed) != div_num_packed) {
  if (this->labels.size() % this->entries.size() != 0) {
    throw invalid_argument(
        "Number of entries does not match number of labels for any value of "
        "num_packed");
  }

  this->num_examples = this->entries.size();
  this->num_features = hash_size;
  this->num_labels = num_labels;
  this->num_packed = size_t(div_num_packed);
}
#else
void EncData::deserialize(ifstream& entries_stream, ifstream& labels_stream,
                          ifstream& pool_stream, const SEALContext& context,
                          size_t hash_size, size_t num_labels) {
  this->entries.clear();
  while (true) {
    try {
      Ciphertext ct;
      ct.load(context, entries_stream);

      this->entries.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }

  this->labels.clear();
  string lab;
  while (getline(labels_stream, lab)) {
    this->labels.push_back(stoi(lab));
  }

  this->label_pool.clear();
  while (true) {
    try {
      Ciphertext ct;
      ct.load(context, pool_stream);
      this->label_pool.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }

  if (this->labels.size() == 0) {
    throw invalid_argument("label size is zero");
  }
  if (this->entries.size() == 0) {
    throw invalid_argument("entries size is zero");
  }
  if (this->labels.size() % this->entries.size() != 0) {
    throw invalid_argument(
        "Number of entries does not match number of labels for any value of "
        "num_packed");
  }
  this->num_examples = this->entries.size();
  this->num_features = hash_size;
  this->num_labels = num_labels;
  this->num_packed = this->labels.size() / this->entries.size();
}
#endif

EncWeightLayer::EncWeightLayer() {}

EncWeightLayer::EncWeightLayer(const size_t num_rows, const size_t num_cols,
                               const size_t num_packed, const size_t scale,
                               CKKSEncoder& encoder, Encryptor& encryptor,
                               const string random, bool random_seed,
                               const string activation)
    : num_rows(num_rows),
      num_cols(num_cols),
      num_packed(num_packed),
      activation(activation) {
  vector<vector<double>> matrix;
  random_packed_weight_matrix(num_rows, num_cols, num_packed, matrix, random,
                              random_seed);
  // print_chunks(matrix, num_packed);
  weights.clear();
  weights.reserve(num_rows);

  for (auto& mat : matrix) {
    Ciphertext ct;
    encrypt(mat, scale, encoder, encryptor, ct);
    weights.push_back(ct);
  }
}

EncWeightLayer::EncWeightLayer(const size_t num_rows, const size_t num_cols,
                               const size_t scale, CKKSEncoder& encoder,
                               Encryptor& encryptor, const string random,
                               bool random_seed, const string activation)
    : num_rows(num_rows),
      num_cols(num_cols),
      num_packed(1),
      activation(activation) {
  vector<vector<double>> matrix;
  random_weight_matrix(num_rows, num_cols, matrix, random, random_seed);
  Ciphertext ct;
  weights.resize(num_rows);
  for (size_t i = 0; i < num_rows; i++) {
    encrypt(matrix[i], scale, encoder, encryptor, ct);
    weights[i] = ct;
  }
}

EncWeightLayer::EncWeightLayer(const EncWeightLayer& layer) {
  this->num_rows = layer.num_rows;
  this->num_cols = layer.num_cols;
  this->activation = layer.activation;
  this->weights.clear();

  Ciphertext copy;
  for (auto& ct : layer.weights) {
    copy = ct;
    this->weights.push_back(copy);
  }
}

void EncWeightLayer::print_weights(CKKSEncoder& encoder, Decryptor& decryptor,
                                   size_t num) {
  size_t stop = num;
  for (auto& row : this->weights) {
    // for (const auto& val : row) {
    //     cout << val << " ";
    //     if (count == stop && num != 0) {
    //         cout << "...";
    //         break;
    //     }
    //     count++;
    // }
    peekAtCiphertext(row, encoder, decryptor, stop);
    // cout << endl;
    stop--;
    if (stop == 0 && num != 0) {
      cout << "..." << endl;
      break;
    }
  }
}

void EncWeightLayer::peekAtMatrixChunks(CKKSEncoder& encoder,
                                        Decryptor& decryptor, size_t num_chunks,
                                        size_t num) {
  if (num_chunks == 0) {
    num_chunks = this->num_packed;
  }
  size_t stop = num;
  for (auto& row : this->weights) {
    peekAtChunks(row, encoder, decryptor, num_chunks, stop, num);
    stop--;
    if (stop == 0 && num != 0) {
      cout << "..." << endl;
      break;
    }
  }
}

void EncWeightLayer::decrypt(CKKSEncoder& encoder, Decryptor& decryptor,
                             vector<vector<double>>& weights) {
  weights.clear();

  for (auto& ct : this->weights) {
    Plaintext pt;
    decryptor.decrypt(ct, pt);
    vector<double> dec_weights;
    encoder.decode(pt, dec_weights);
    weights.push_back(dec_weights);
  }
}

// Not implemented, because currently we send a single ciphertext through at a
// time. It would be nice to eventually have this functionality, but it would
// probably have to encapsulate the entire GD process
/*void EncWeightLayer::propagate_forward(EncData& data, Evaluator& evaluator,
vector<Ciphertext>& output) { output.clear(); size_t num_ex =
data.get_num_examples(); output.resize(num_ex);

    vector<Ciphertext> dat;
    data.get_entries(dat);

    for (size_t i = 0; i < num_ex; i++) {

    }
}*/

void multiply_and_relin(const Ciphertext& data,
                        const vector<Ciphertext>& weights, Evaluator& evaluator,
                        const GaloisKeys& galois_keys,
                        const RelinKeys& relin_keys,
                        vector<Ciphertext>& output) {
  for (size_t i = 0; i < weights.size(); i++) {
    evaluator.multiply(data, weights.at(i), output.at(i));
    evaluator.relinearize_inplace(output.at(i), relin_keys);
    cyclic_add_inplace(output.at(i), galois_keys, evaluator);
    evaluator.rescale_to_next_inplace(output.at(i));
    output.at(i).scale() = data.scale();
  }
}

void multiply_and_relin_packed(const Ciphertext& data,
                               const vector<Ciphertext>& weights,
                               size_t num_packed, Evaluator& evaluator,
                               const GaloisKeys& galois_keys,
                               const RelinKeys& relin_keys,
                               const Plaintext& zero_one,
                               vector<Ciphertext>& output) {
  size_t num_rots = log2(data.poly_modulus_degree()) - 1 - log2(num_packed);
  for (size_t i = 0; i < weights.size(); i++) {
    // ciphertextReport(weights.at(i));
    // cout << "1" << endl;
    evaluator.multiply(data, weights.at(i), output.at(i));
    // cout << "2" << endl;
    evaluator.relinearize_inplace(output.at(i), relin_keys);
    // cout << "3" << endl;
    evaluator.rescale_to_next_inplace(output.at(i));
    // cout << "4" << endl;
    output.at(i).scale() = data.scale();
    // Replace with chunk add
    // cout << "5" << endl;
    cyclic_add_inplace(output.at(i), galois_keys, evaluator, num_rots);
    // Zero-one multiplication
    // ciphertextReport(weights.at(i));
    // plaintextReport(zero_one);
    // cout << "6" << endl;
    evaluator.multiply_plain_inplace(output.at(i), zero_one);
    // Chunk add backward.
    // cout << "7" << endl;
    cyclic_add_inplace_right(output.at(i), galois_keys, evaluator, num_rots);
    // cout << "8" << endl;
    evaluator.rescale_to_next_inplace(output.at(i));
    output.at(i).scale() = data.scale();
    // ciphertextReport(weights.at(i));
  }
}

void EncWeightLayer::linear_forward(const Ciphertext& data,
                                    Evaluator& evaluator,
                                    const GaloisKeys& galois_keys,
                                    const RelinKeys& relin_keys,
                                    vector<Ciphertext>& output) const {
  /*output.clear();
  size_t num_rows = this->num_rows;
  output.resize(num_rows);*/

  if (this->num_packed == 1) {
    multiply_and_relin(data, this->weights, evaluator, galois_keys, relin_keys,
                       output);
  } else {
    multiply_and_relin_packed(data, this->weights, this->num_packed, evaluator,
                              galois_keys, relin_keys, this->zero_one, output);
  }
}

void EncWeightLayer::mod_switch_to(Evaluator& evaluator,
                                   parms_id_type& parms_id) {
  for (auto& val : this->weights) {
    evaluator.mod_switch_to_inplace(val, parms_id);
  }
}

// this method doesnt change the object must be const
void EncWeightLayer::mod_switch_to_and_copy(Evaluator& evaluator,
                                            parms_id_type& parms_id,
                                            vector<Ciphertext>& result) const {
  result.clear();

  // for (auto& val : const_cast<EncWeightLayer*>(this)->weights) {
  for (auto& val : this->weights) {
    Ciphertext ct;
    evaluator.mod_switch_to(val, parms_id, ct);
    result.push_back(ct);
  }
}

void EncWeightLayer::update_gradients(Evaluator& evaluator,
                                      const vector<Ciphertext>& gradients) {
  for (size_t i = 0; i < this->num_rows; i++) {
    evaluator.sub_inplace(this->weights.at(i), gradients.at(i));
  }
}

void EncWeightLayer::apply_momentum(Evaluator& evaluator,
                                    const vector<Ciphertext>& momentum) {
  for (size_t i = 0; i < this->num_rows; i++) {
    evaluator.sub_inplace(this->weights.at(i), momentum.at(i));
  }
}

double EncWeightLayer::predict(EncData& data, Evaluator& evaluator,
                               Decryptor& decryptor, CKKSEncoder& encoder,
                               const GaloisKeys& gal_keys,
                               const RelinKeys& relin_keys) {
  vector<Ciphertext> cts, acts;
  data.get_entries(cts);
  double total = data.get_num_examples();
  vector<int> labels;
  data.get_labels(labels);

  double correct = 0;
  for (int i = 0; i < total; i++) {
    this->linear_forward(cts.at(i), evaluator, gal_keys, relin_keys, acts);
    vector<double> clear_activations;
    for (auto& act : acts) {
      Plaintext pt;
      decryptor.decrypt(act, pt);
      vector<double> dec;
      encoder.decode(pt, dec);
      clear_activations.push_back(dec[0]);
    }
    int curr = -1;
    curr = argmax(clear_activations);
    if (labels[i] == curr) {
      correct++;
    }
  }
  return correct;
}

double EncWeightLayer::piece_of_predict(const Ciphertext& ct, int label,
                                        Evaluator& evaluator,
                                        Decryptor& decryptor,
                                        CKKSEncoder& encoder,
                                        const GaloisKeys& gal_keys,
                                        const RelinKeys& relin_keys) {
  vector<Ciphertext> acts;

  this->linear_forward(ct, evaluator, gal_keys, relin_keys, acts);
  vector<double> activations;
  for (auto& act : acts) {
    Plaintext pt;
    decryptor.decrypt(act, pt);
    vector<double> dec;
    encoder.decode(pt, dec);
    activations.push_back(dec.at(0));
  }

  int curr = -1;
  curr = argmax(activations);
  // cout << "label: " << label << " curr: " << curr << endl;
  if (curr == -1) {
    cout << label << ": ";
    print_vector(activations);
  }
  if (label == curr) {
    return 1.0;
  }

  return 0.0;
}

double EncWeightLayer::predict_parallel(EncData& data, Evaluator& evaluator,
                                        Decryptor& decryptor,
                                        CKKSEncoder& encoder,
                                        const GaloisKeys& gal_keys,
                                        const RelinKeys& relin_keys,
                                        size_t threads) {
  vector<Ciphertext> cts, acts;
  data.get_entries(cts);
  double total = data.get_num_examples();
  vector<int> labels;
  data.get_labels(labels);

  double correct = 0.0;
  {
    dispatch_queue q("Parallel Prediction Queue", threads);
    function<void(int, int)> fun = [&](int ct, int label) {
      int val = this->piece_of_predict(cts.at(ct), label, evaluator, decryptor,
                                       encoder, gal_keys, relin_keys);
      correct += val;
      // cout << "Got " << val << " this time" << endl;
      // cout << "correct is " << correct << endl;
    };
    for (int i = 0; i < total; i++) {
      int label = labels.at(i);
      /// q.dispatch([ct, label, fun, &correct] {fun(ct, label);});
      q.dispatch([label, fun, i] { fun(i, label); });
    }
  }
  // cout << "Out of loop " << endl;
  // int j;
  // cin >> j;
  // cout << "Correct is " << correct << endl;
  return correct;
}

void EncWeightLayer::save(string file_name) {
  ofstream file(file_name, ios::binary);

  for (auto& ct : this->weights) {
    ct.save(file);
  }

  file.close();
}

#ifndef SEAL36
void EncWeightLayer::load(string file_name,
                          shared_ptr<seal::SEALContext> context) {
  ifstream file(file_name, ios::binary);
  vector<Ciphertext> weights;

  while (1) {
    Ciphertext ct;
    try {
      ct.load(context, file);
      weights.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }

  // This function assumes that there is only one entry per ciphertext!
  this->weights = weights;
  this->num_rows = weights.size();
  this->num_cols =
      context->first_context_data()->parms().poly_modulus_degree() / 2;
  this->activation = "sigmoid";

  file.close();
}
#else
void EncWeightLayer::load(string file_name, const SEALContext& context) {
  ifstream file(file_name, ios::binary);
  vector<Ciphertext> weights;

  while (1) {
    Ciphertext ct;
    try {
      ct.load(context, file);
      weights.push_back(ct);
    } catch (exception& e) {
      break;
    }
  }

  // This function assumes that there is only one entry per ciphertext!
  this->weights = weights;
  this->num_rows = weights.size();
  this->num_cols =
      context.first_context_data()->parms().poly_modulus_degree() / 2;
  this->activation = "sigmoid";

  file.close();
}
#endif

double EncWeightLayer::sum_weights(CKKSEncoder& encoder, Decryptor& decryptor) {
  return sum_ciphertexts(this->weights, encoder, decryptor);
}

double EncWeightLayer::abs_val_weights(CKKSEncoder& encoder,
                                       Decryptor& decryptor) {
  return abs_val_ciphertexts(this->weights, encoder, decryptor);
}

void EncWeightLayer::refresh(CKKSEncoder& encoder, Decryptor& decryptor,
                             Encryptor& encryptor) {
  this->refresh_and_rotate(encoder, decryptor, encryptor, 1, 0);
}

void EncWeightLayer::refresh_and_rotate(CKKSEncoder& encoder,
                                        Decryptor& decryptor,
                                        Encryptor& encryptor,
                                        size_t num_packed,
                                        int rot) {
  refresh_and_rotate_vector(weights, encoder, decryptor, encryptor, num_packed, rot);
}

void refresh_and_rotate_vector(vector<Ciphertext>& to_refresh,
                               CKKSEncoder& encoder, Decryptor& decryptor,
                               Encryptor& encryptor, size_t num_packed, int rot) {
  vector<vector<double>> dec_weights;
  for (auto& weight : to_refresh) {
    Plaintext pt;
    decryptor.decrypt(weight, pt);
    vector<double> vec;
    encoder.decode(pt, vec);
    dec_weights.push_back(vec);
  }

  size_t scale = to_refresh.at(0).scale();
  to_refresh.clear();

  // rotate
  // vector<vector<double>> rot_weights;
  if (num_packed != 1 && rot != 0) {
    size_t rot_by = encoder.slot_count() / num_packed * rot;
    if (rot > 0) {
      for (auto& weight : dec_weights) {
        rotate(weight.begin(), weight.begin() + rot_by, weight.end());
      }
    } else {
      for (auto& weight : dec_weights) {
        rotate(weight.begin(), weight.end() + rot_by, weight.end());
      }
    }
  }

  for (auto& weight : dec_weights) {
    Plaintext pt;
    encoder.encode(weight, scale, pt);
    Ciphertext ct;
    encryptor.encrypt(pt, ct);
    to_refresh.push_back(ct);
  }
}

double sum_ciphertexts(vector<Ciphertext>& ciphertexts, CKKSEncoder& encoder,
                       Decryptor& decryptor) {
  double sum = 0;

  for (auto& ct : ciphertexts) {
    Plaintext pt;
    decryptor.decrypt(ct, pt);
    vector<double> vec;
    encoder.decode(pt, vec);
    for (auto& val : vec) {
      sum += val;
    }
  }

  return sum;
}

double abs_val_ciphertexts(vector<Ciphertext>& ciphertexts,
                           CKKSEncoder& encoder, Decryptor& decryptor) {
  double sum = 0;

  for (auto& ct : ciphertexts) {
    Plaintext pt;
    decryptor.decrypt(ct, pt);
    vector<double> vec;
    encoder.decode(pt, vec);
    for (auto& val : vec) {
      sum += abs(val);
    }
  }

  return sum;
}

void softmax_approx_privft_inplace(Ciphertext& input,
                                   vector<Plaintext>& constants,
                                   Evaluator& evaluator,
                                   const RelinKeys& relin_keys) {
  // 1/8 x^2 + 1/2 x + 1/4
  Ciphertext sqd, lin, output;
  evaluator.multiply(input, input, sqd);
  evaluator.relinearize_inplace(sqd, relin_keys);
  evaluator.rescale_to_next_inplace(sqd);  // L - 2

  evaluator.multiply_plain_inplace(sqd, constants.at(0));
  evaluator.rescale_to_next_inplace(sqd);  // L - 3
  sqd.scale() = input.scale();

  evaluator.multiply_plain(input, constants.at(1), lin);
  evaluator.rescale_to_next_inplace(lin);     // L - 2
  evaluator.mod_switch_to_next_inplace(lin);  // L - 3
  lin.scale() = input.scale();

  evaluator.add(sqd, lin, output);
  evaluator.add_plain_inplace(output, constants.at(2));

  input = output;
}

void softmax_approx_privft(Ciphertext& input, vector<Plaintext>& constants,
                           Evaluator& evaluator, const RelinKeys& relin_keys,
                           Ciphertext& output) {
  output = input;

  softmax_approx_privft_inplace(output, constants, evaluator, relin_keys);
}

void generate_softmax_constants(size_t scale, CKKSEncoder& encoder,
                                vector<parms_id_type>& parms_ids, size_t level,
                                vector<Plaintext>& constants) {
  constants.clear();
  Plaintext over2_pt, over4_pt, over8_pt;
  double factor = 100.0;

  encoder.encode(1.0 / 2.0 / factor, parms_ids.at(level), scale, over2_pt);
  encoder.encode(1.0 / 8.0 / factor, parms_ids.at(level + 1), scale, over8_pt);
  encoder.encode(1.0 / 2.0 / factor, parms_ids.at(level + 2), scale, over4_pt);

  constants.push_back(over8_pt);
  constants.push_back(over2_pt);
  constants.push_back(over4_pt);
}

// provide values in decreasing order of degree, and they will be returned in
// the same order.
void generate_degree_five_constants(size_t scale, vector<double> values,
                                    CKKSEncoder& encoder,
                                    vector<parms_id_type>& parms_ids,
                                    size_t level,
                                    vector<Plaintext>& constants) {
  constants.clear();
  Plaintext a5, a4, a3, a2, a1, a0;
  if (values.size() != 6)
    throw invalid_argument("Expected 6 values for a degree 5 polynomial");

  encoder.encode(values.at(0), parms_ids.at(level + 1), scale, a5);
  encoder.encode(values.at(1), parms_ids.at(level + 2), scale, a4);
  encoder.encode(values.at(2), parms_ids.at(level), scale, a3);
  encoder.encode(values.at(3), parms_ids.at(level + 1), scale, a2);
  encoder.encode(values.at(4), parms_ids.at(level + 1), scale, a1);
  encoder.encode(values.at(5), parms_ids.at(level + 3), scale, a0);

  constants.push_back(a5);
  constants.push_back(a4);
  constants.push_back(a3);
  constants.push_back(a2);
  constants.push_back(a1);
  constants.push_back(a0);
}

void evaluate_degree_five_polynomial_inplace(Ciphertext& input,
                                             vector<Plaintext>& constants,
                                             Evaluator& evaluator,
                                             const RelinKeys& relin_keys) {
  Ciphertext inp_leveled, inp_sqd;
  evaluator.multiply(input, input, inp_sqd);
  evaluator.relinearize_inplace(inp_sqd, relin_keys);
  evaluator.rescale_to_next_inplace(inp_sqd);
  inp_sqd.scale() = input.scale();
  evaluator.mod_switch_to_next(input, inp_leveled);

  Ciphertext a1x;
  evaluator.multiply_plain(inp_leveled, constants.at(4), a1x);
  evaluator.rescale_to_next_inplace(a1x);
  a1x.scale() = input.scale();

  Ciphertext a3x3;
  evaluator.multiply_plain(input, constants.at(2), a3x3);
  evaluator.rescale_to_next_inplace(a3x3);
  a3x3.scale() = input.scale();
  evaluator.multiply_inplace(a3x3, inp_sqd);
  evaluator.relinearize_inplace(a3x3, relin_keys);
  evaluator.rescale_to_next_inplace(a3x3);
  a3x3.scale() = input.scale();

  Ciphertext a2x2;
  evaluator.multiply_plain(inp_sqd, constants.at(3), a2x2);
  evaluator.rescale_to_next_inplace(a2x2);
  a2x2.scale() = input.scale();

  Ciphertext lower_terms;
  evaluator.add_many({a1x, a2x2, a3x3}, lower_terms);
  a1x.release();
  a2x2.release();
  a3x3.release();
  evaluator.mod_switch_to_next_inplace(lower_terms);

  Ciphertext inp_fourth;
  evaluator.multiply(inp_sqd, inp_sqd, inp_fourth);
  inp_sqd.release();
  evaluator.relinearize_inplace(inp_fourth, relin_keys);
  evaluator.rescale_to_next_inplace(inp_fourth);
  inp_fourth.scale() = input.scale();

  Ciphertext a5x5;
  evaluator.multiply_plain(inp_leveled, constants.at(0), a5x5);
  evaluator.rescale_to_next_inplace(a5x5);
  a5x5.scale() = input.scale();
  evaluator.multiply_inplace(a5x5, inp_fourth);
  evaluator.relinearize_inplace(a5x5, relin_keys);
  evaluator.rescale_to_next_inplace(a5x5);
  a5x5.scale() = input.scale();

  Ciphertext a4x4;
  evaluator.multiply_plain(inp_fourth, constants.at(1), a4x4);
  evaluator.rescale_to_next_inplace(a4x4);
  a4x4.scale() = input.scale();
  evaluator.add_plain_inplace(a4x4, constants.at(5));

  Ciphertext output;
  evaluator.add_many({lower_terms, a4x4, a5x5}, output);

  input = output;
}

void evaluate_degree_five_polynomial(Ciphertext& input,
                                     vector<Plaintext>& constants,
                                     Evaluator& evaluator,
                                     const RelinKeys& relin_keys,
                                     Ciphertext& output) {
  output = input;

  evaluate_degree_five_polynomial_inplace(output, constants, evaluator,
                                          relin_keys);
}
