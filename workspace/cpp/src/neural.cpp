#include "neural.h"

#include <cmath>
#include <fstream>
#include <functional>
#include <sstream>

using namespace std;

Data::Data(string file_name, size_t num_features, size_t num_labels,
           size_t num_examples, size_t hashor, bool desc)
    : num_features(num_features), num_labels(num_labels) {
  ifstream data(file_name, ios::in);
  string line, cols;
  vector<double> entry, label;
  getline(data, cols);

  size_t num_lines = 0;
  while (getline(data, line) && num_lines < num_examples) {
    if (line.length() >= 1) {
      load_data_line(line, entry, num_features, hashor, false);
      entries.push_back(entry);
      load_label_line(line, label, num_labels);
      labels.push_back(label);
      if (desc == true) {
        string desc;
        load_description(line, desc);
        descriptions.push_back(desc);
        // cout << desc << endl;
      }
      num_lines++;
    }
  }
  data.close();
  this->num_examples = entries.size();
  if (hashor != 0) {
    this->num_features = hashor;
  }
}

Data::Data(vector<vector<double>>& batch,
           vector<vector<double>>& batch_labels) {
  num_examples = batch.size();
  num_features = batch[0].size();
  num_labels = batch_labels[0].size();

  entries = batch;
  labels = batch_labels;
}

Data::Data() {}

void Data::get_mini_batch(size_t start_from, size_t batch_size, Data& batch) {
  size_t end = start_from + batch_size;
  if (end > this->get_num_examples()) {
    end = this->get_num_examples();
  }
  vector<vector<double>> entries(this->entries.begin() + start_from,
                                 this->entries.begin() + end);
  vector<vector<double>> labels(this->labels.begin() + start_from,
                                this->labels.begin() + end);

  batch = Data(entries, labels);
}

WeightLayer::WeightLayer(int num_rows, int num_cols, string random,
                         string activation) {
  this->num_rows = num_rows;
  this->num_cols = num_cols;
  this->activation = activation;

  random_weight_matrix(num_rows, num_cols, weights, random);
}

WeightLayer::WeightLayer(vector<vector<double>>& weights) {
  this->weights = weights;
  this->num_cols = weights.at(0).size();
  this->num_rows = weights.size();
  this->activation = "none";
}

void WeightLayer::update_weights(WeightLayer& gradients, double learning_rate,
                                 double regularization) {
  if (this->num_cols != gradients.get_num_cols() ||
      this->num_rows != gradients.get_num_rows()) {
    throw invalid_argument("WeightLayers do not match: cols are " +
                           to_string(this->num_cols) + " and " +
                           to_string(gradients.get_num_cols()) +
                           " while rows are " + to_string(this->num_rows) +
                           " and " + to_string(gradients.get_num_rows()));
  }

  vector<vector<double>> new_weights;
  update_gradients(this->weights, gradients.weights, learning_rate,
                   regularization, new_weights);
  this->weights = new_weights;
}

void WeightLayer::update_weights(vector<vector<double>>& gradients,
                                 double learning_rate, double regularization) {
  vector<vector<double>> new_weights;
  update_gradients(this->weights, gradients, learning_rate, regularization,
                   new_weights);
  this->weights = new_weights;
}

void WeightLayer::apply_activation(const vector<double>& input,
                                   vector<double>& output) {
  if (this->activation == "softmax") {
    softmax(input, output);
  } else if (this->activation == "sigmoid") {
    sigmoid(input, output);
  } else if (this->activation == "wide") {
    wide_sigmoid_approx(input, output);
  } else if (this->activation == "tight") {
    tight_sigmoid_approx(input, output);
  } else if (this->activation == "none") {
    output = input;
  } else {
    throw invalid_argument("Activation " + this->activation + " not supported");
  }
}

void WeightLayer::propagate_past(vector<vector<double>>& data_in,
                                 vector<vector<double>>& data_out) {
  data_out.clear();
  data_out.reserve(data_in.size());

  vector<double> result, active;
  for (const auto& data : data_in) {
    try {
      matrix_vector_product(this->weights, data, result);
    } catch (exception& e) {
      cout << e.what() << endl;
      throw invalid_argument(
          "Exception during matrix_vector_product in "
          "WeightLayer::propagate_past");
    }
    this->apply_activation(result, active);
    data_out.push_back(active);
  }  // I think I will try to change this function to be parallel.
}

void WeightLayer::propagate_past(vector<double>& data_in,
                                 vector<double>& data_out) {
  matrix_vector_product(this->weights, data_in, data_out);
}

void WeightLayer::propagate_past(Data& data_in,
                                 vector<vector<double>>& activations) {
  vector<vector<double>> data;
  data_in.get_data(data);
  this->propagate_past(data, activations);
}

void WeightLayer::propagate_past_back(vector<vector<double>>& activation,
                                      vector<vector<double>>& current_layer,
                                      vector<vector<double>>& previous_layer,
                                      vector<vector<double>>& gradients) {
  gradients.clear();

  vector<double> intermediate;
  vector<vector<double>> grad;
  vector<vector<vector<double>>> grads;
  for (size_t i = 0; i < activation.size(); i++) {
    softmax_backward_single(activation[i], current_layer[i], intermediate);
    compute_gradients(intermediate, previous_layer[i], grad);
    grads.push_back(grad);
  }
  aggregate_gradients(grads, gradients);
}

void WeightLayer::inspect_weights(double min, double max) {
  int count = 0;
  double maxest = 0.0;
  double minest = 0.0;

  for (auto& row : this->weights) {
    for (auto& val : row) {
      if (val > max || val < min) {
        count++;
      }
      if (val > maxest) {
        maxest = val;
      }
      if (val < minest) {
        minest = val;
      }
    }
  }

  cout << "There are " << count << " values outside the range (" << min << ", "
       << max << ")." << endl;
  cout << "The largest is " << maxest << " and the smallest is " << minest
       << endl;
}

double WeightLayer::sum_weights() {
  double sum = 0;

  for (auto& vec : this->weights) {
    for (auto& val : vec) {
      sum += val;
    }
  }

  return sum;
}

double WeightLayer::abs_val_weights() {
  double sum = 0;

  for (auto& vec : this->weights) {
    for (auto& val : vec) {
      sum += abs(val);
    }
  }

  return sum;
}

NeuralNet::NeuralNet(vector<size_t> dims, float learn_rate,
                     double regularization, double momentum_coeff,
                     string activation)
    : layer_dims(dims),
      learning_rate(learn_rate),
      regularization(regularization),
      momentum_coeff(momentum_coeff) {
  if (dims.size() < 2) {
    cout << "not enough dims to make a network" << endl;
    return;
  }
  layers.clear();

  for (size_t i = 1; i < dims.size(); i++) {
    WeightLayer weights(dims[i], dims[i - 1], "normal", activation);
    layers.push_back(weights);
    activation_functions.push_back(activation);
  }
}

void NeuralNet::print_network(size_t num) {
  size_t num_layers = this->layer_dims.size() - 1;
  cout << "A NeuralNet with " << num_layers << " layers" << endl;
  for (size_t i = 0; i < num_layers; i++) {
    cout << " - Layer " << i << " is " << layer_dims[i + 1] << " by "
         << layer_dims[i] << " with activation " << activation_functions[i]
         << endl;
    print_matrix(this->layers[i].weights, num);
  }
}

void NeuralNet::propagate_forward(Data& input,
                                  vector<vector<double>>& activations) {
  vector<vector<double>> intermediate_in, intermediate_out;
  input.get_data(intermediate_in);
  // for (int i = 0; i < this->layers.size(); i++) {
  for (auto& weights : this->layers) {
    try {
      weights.propagate_past(intermediate_in, intermediate_out);
    } catch (exception& e) {
      cout << e.what() << endl;
      throw invalid_argument(
          "Exception during propagate_past in NeuralNet::propagate_forward");
    }
    intermediate_in = intermediate_out;
  }
  activations = intermediate_out;
}

void NeuralNet::propagate_backward(Data& input,
                                   vector<vector<double>>& activations,
                                   vector<vector<double>>& gradients) {
  gradients.clear();
  // This function is not ready for a multilayer network
  vector<vector<double>> current_layer, previous_layer;
  input.get_labels(current_layer);
  input.get_data(previous_layer);
  this->layers[0].propagate_past_back(activations, current_layer,
                                      previous_layer, gradients);
}

void NeuralNet::update_gradients(vector<WeightLayer>& gradients) {
  if (this->layers.size() != gradients.size()) {
    throw invalid_argument("Incorrect number of gradients " +
                           to_string(gradients.size()) + ": expected " +
                           to_string(this->layers.size()));
  }
  for (size_t i = 0; i < this->layers.size(); i++) {
    this->layers[i].update_weights(gradients[i], this->learning_rate,
                                   this->regularization);
  }
}

void NeuralNet::update_gradients(vector<vector<double>>& gradients) {
  // if (this->layers.size() != gradients.size()) {
  //     throw invalid_argument("Incorrect number of gradients " +
  //     to_string(gradients.size()) + ": expected " +
  //     to_string(this->layers.size()));
  // }
  this->layers[0].update_weights(gradients, this->learning_rate,
                                 this->regularization);
}

void NeuralNet::update_model(vector<vector<double>>& to_update) {
  // Currently only implemented for single layer networks

  this->layers[0].update_weights(to_update, 1, 0);
}

void NeuralNet::apply_momentum(vector<vector<double>>& momentum) {
  if (almost_equal(this->momentum_coeff, 0.0, 2)) {
    return;
  }

  this->layers[0].update_weights(momentum, this->momentum_coeff, 0);
}

void NeuralNet::update_momentum(vector<vector<double>>& gradients,
                                vector<vector<double>>& momentum) {
  if (almost_equal(this->momentum_coeff, 0.0, 2)) {
    return;
  }

  if (gradients.size() == 0) {
    throw invalid_argument("In update_momentum: Gradients is empty!");
  }

  if (gradients.size() != momentum.size() ||
      gradients.at(0).size() != momentum.at(0).size()) {
    string error_message = string(
                               "In update_momentum: gradients and momentum "
                               "dimensions do not match:\n") +
                           "gradients is " + to_string(gradients.size()) + "x" +
                           to_string(gradients.at(0).size()) + "\n" +
                           "while momentum is " + to_string(momentum.size()) +
                           "x" + to_string(momentum.at(0).size());
    throw invalid_argument(error_message);
  }

  for (size_t i = 0; i < gradients.size(); i++) {
    for (size_t j = 0; j < gradients.at(0).size(); j++) {
      momentum.at(i).at(j) = this->momentum_coeff * momentum.at(i).at(j) +
                             this->learning_rate * gradients.at(i).at(j);
    }
  }
}

int NeuralNet::predict(Data& data) {
  vector<vector<double>> activations;
  vector<vector<double>> labels;
  data.get_labels(labels);
  this->propagate_forward(data, activations);
  // int curr = -1;
  // for (int i = 0; i < total; i++) {
  //     curr = argmax(activations[i]);
  //     if (labels[i][curr] == 1) {
  //         correct++;
  //     }
  // }
  return tally_activations(activations, labels);
}

void NeuralNet::inspect_network(double min, double max) {
  this->layers.at(0).inspect_weights(min, max);
}

double NeuralNet::sum_weights() { return this->layers.at(0).sum_weights(); }

double tight_polyn(double input) {
  // double p = input / 16;
  // return 0.255137 + 2.60301 * p + 3.62922 * pow(p, 2) - 12.5726 * pow(p, 3)
  // - 11.1495 * pow(p, 4) + 25.3241 * pow(p, 5);

  // double p = input / 8;
  // return 0.255137 + 1.30151 * p + 0.907305 * pow(p, 2) - 1.57157 * pow(p,3) -
  // 0.696844 * pow(p, 4) + 0.791379 * pow(p, 5);
  /// Native, no rescaling
  return 0.2551370334 + 0.1626882918 * input + 0.01417664232 * pow(input, 2) -
         0.003069479016 * pow(input, 3) - 0.0001701280363 * pow(input, 4) +
         0.00002415095529 * pow(input, 5);
}

double wide_polyn(double input) {
  // double p = input / 16;
  // return 0.2658986066 + 1.197210746 * p + 1.035631265 * pow(p, 2)
  // - 1.317052242 * pow(p, 3) - 1.003057355 * pow(p, 4) + 0.8504716875 * pow(p,
  // 5);

  // double p = input / 8;
  // return 0.2658986066 + 0.5986053729 * p + 0.2589078163 * pow(p, 2) -
  // 0.1646315302 * pow(p, 3) - 0.06269108469 * pow(p, 4) + 0.02657724024 *
  // pow(p, 5);
  /// Native, no rescaling
  return 0.2658986066 + 0.07482567161 * input + 0.004045434629 * pow(input, 2) -
         0.0003215459574 * pow(input, 3) - 0.00001530544060 * pow(input, 4) +
         0.0000008110730052 * pow(input, 5);
}

void softmax(const vector<double>& input, vector<double>& output) {
  output.clear();
  output.resize(input.size(), 0.0);

  double denom = 0;
  double new_term = 0;
  for (size_t i = 0; i < input.size(); i++) {
    /// SOFTMAX
    new_term = exp(input[i]);
    output[i] = new_term;
    denom += new_term;

    /// PRIVFT APPROXIMATION
    // output[i] = 1 / 8.0 * pow(input[i],2) + 1 / 2.0 * input[i] + 1/4.0;

    if (input[i] > 10 || input[i] < -8) {
      // cout << input[i] << " " << output[i] << endl;
    }
  }

  // for (int i = 0; i < output.size(); i++) {
  //     output[i] /= denom;
  // }
}

void sigmoid(const vector<double>& input, vector<double>& output) {
  output.clear();
  output.resize(input.size(), 0.0);

  for (size_t i = 0; i < input.size(); i++) {
    /// SIGMOID
    output[i] = 1 / (1 + exp(-input[i] + 1.39));
  }
}

void wide_sigmoid_approx(const vector<double>& input, vector<double>& output) {
  output.clear();
  output.resize(input.size(), 0.0);

  for (size_t i = 0; i < input.size(); i++) {
    /// WIDE APPROXIMATION
    output[i] = wide_polyn(input[i]);
  }
}

void tight_sigmoid_approx(const vector<double>& input, vector<double>& output) {
  output.clear();
  output.resize(input.size(), 0.0);

  for (size_t i = 0; i < input.size(); i++) {
    /// TIGHT APPROXIMATION
    output[i] = tight_polyn(input[i]);
  }
}

void softmax_backward_single(const vector<double>& cache,
                             const vector<double>& dA,
                             vector<double>& prev_gradient) {
  vector<double> gradient_row(cache.size());

  prev_gradient.clear();
  prev_gradient.resize(cache.size(), 0.0);

  //// This is the code for if dA is the output labels.
  for (size_t i = 0; i < cache.size(); i++) {
    for (size_t j = 0; j < cache.size(); j++) {
      if (i == j) {
        // gradient_row[j] =  cache[i] - pow(cache[i], 2);
        gradient_row[j] = -1 + cache[i];
      } else {
        // gradient_row[j] = - cache[i] * cache[j] ;
        gradient_row[j] = cache[i];
      }
    }
    prev_gradient[i] = vector_innerprod(dA, gradient_row);
  }
  //// This is the code for if dA is the output layer.
  // for (int i = 0; i < softmax.size(); i++) {
  //     prev_gradient[i] = softmax[i] - dA[i];
  // }
}

void negate(vector<double>& activation, vector<double>& quotient) {
  quotient.clear();
  quotient.resize(activation.size());

  for (size_t i = 0; i < activation.size(); i++) {
    quotient[i] = -activation[i];
  }
}

void compute_gradients(vector<double>& dZ, vector<double>& activations,
                       vector<vector<double>>& gradients) {
  int num_cols, num_rows;
  num_rows = dZ.size();
  num_cols = activations.size();

  gradients.clear();
  gradients.reserve(num_rows);
  vector<double> grad_row(num_cols);

  for (int i = 0; i < num_rows; i++) {
    for (int j = 0; j < num_cols; j++) {
      grad_row[j] = dZ[i] * activations[j];
    }
    gradients.push_back(grad_row);
    grad_row.clear();
    grad_row.resize(num_cols);
  }
}

void aggregate_gradients(vector<vector<vector<double>>>& gradients,
                         vector<vector<double>>& aggregate) {
  double num_gradients = gradients.size();
  size_t num_cols = gradients[0][0].size();
  size_t num_rows = gradients[0].size();

  double new_val = 0;

  vector<double> agg_row(num_cols);
  aggregate.clear();
  aggregate.reserve(num_rows);

  for (size_t i = 0; i < num_rows; i++) {
    for (size_t j = 0; j < num_cols; j++) {
      for (auto& grad : gradients) {
        new_val += grad[i][j];
      }
      new_val /= num_gradients;
      agg_row[j] = new_val;
      new_val = 0.0;
    }
    aggregate.push_back(agg_row);
    agg_row.resize(num_cols, 0.0);
  }
}


void update_gradients(vector<vector<double>>& weights,
                      vector<vector<double>>& gradients, double learning_rate,
                      double regularization,
                      vector<vector<double>>& new_weights) {
  int num_cols, num_rows;
  num_rows = weights.size();
  num_cols = weights[0].size();

  new_weights.clear();
  new_weights.reserve(num_rows);
  vector<double> new_row(num_cols);

  for (int i = 0; i < num_rows; i++) {
    for (int j = 0; j < num_cols; j++) {
      new_row[j] = (1 - regularization) * weights[i][j] -
                   learning_rate * gradients[i][j];
    }
    new_weights.push_back(new_row);
    new_row.clear();
    new_row.resize(num_cols);
  }
}

void initialize_momentum(vector<vector<double>>& momentum, size_t num_rows,
                         size_t num_cols) {
  momentum.clear();
  momentum.reserve(num_rows);

  for (size_t i = 0; i < num_rows; i++) {
    vector<double> row;
    for (size_t j = 0; j < num_cols; j++) {
      row.push_back(0.0);
    }
    momentum.push_back(row);
  }
}
