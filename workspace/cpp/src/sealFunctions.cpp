#include "sealFunctions.h"

#include <vector>

#include "utility.h"
//#include "csvFunctions.h"

using namespace std;
using namespace seal;

void headOfVector(const vector<double>& vec, const int n) {
  if (vec.empty() == true) {
    cout << "[  ]" << endl;
    return;
  }
  cout << "[ ";
  int i = 0;
  for (vector<double>::const_iterator it = vec.begin();
       it != vec.end() && i < n; ++it) {
    // justify right, use a field of 6 spaces wide, fixed point notation
    // and precision of 2 decimals
    cout << right << setw(6) << fixed << setprecision(2) << *it << " ";
    i++;
  }
  cout << "... ";
  cout << "]" << endl;
}

void peekAtChunks(const Ciphertext& ct, CKKSEncoder& encoder,
                  Decryptor& decryptor, size_t num_chunks, size_t n,
                  size_t full_size) {
  vector<double> vect;
  decrypt(ct, encoder, decryptor, vect);
  print_chunks(vect, num_chunks, n, true, encoder.slot_count() / num_chunks,
               full_size);
}

void ciphertextReport(const Ciphertext& ct) {
  cout << "CMS: " << ct.coeff_modulus_size() << " Scale: 2^" << log2(ct.scale())
       << " Size: " << ct.size() << endl
       << "Parms id: " << ct.parms_id() << endl;
}

void plaintextReport(const Plaintext& pt) {
  cout << "Scale: 2^" << log2(pt.scale()) << endl
       << "Parms_id: " << pt.parms_id() << endl;
}

ostream& operator<<(ostream& os, const parms_id_type& pid) {
  for (auto& val : pid) {
    os << val << " ";
  }
  return os;
}

void encode_zero_one(const parms_id_type& parms_id, size_t scale,
                     CKKSEncoder& encoder, size_t chunk_size,
                     Plaintext& zero_one) {
  vector<double> zerosOnes;
  for (size_t i = 0; i < encoder.slot_count(); i++) {
    if (0 == i % chunk_size)
      zerosOnes.push_back(1);
    else
      zerosOnes.push_back(0);
  }
  encoder.encode(zerosOnes, parms_id, scale, zero_one);
}

void cyclic_add_inplace(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                        Evaluator& evaluator, int N) {
  if (N == -1) N = (int)log2(toAdd.poly_modulus_degree()) - 1;
  Ciphertext temp;

  for (int i = 0; i < N; i++) {
    evaluator.rotate_vector(toAdd, (int)pow(2, i), galois_keys, temp);
    evaluator.add_inplace(toAdd, temp);
  }
}

void cyclic_add(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                Evaluator& evaluator, Ciphertext& result, int N) {
  result = toAdd;
  cyclic_add_inplace(result, galois_keys, evaluator, N);
}

void cyclic_add_inplace_right(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                              Evaluator& evaluator, int N) {
  if (N == -1) N = (int)log2(toAdd.poly_modulus_degree()) - 1;
  Ciphertext temp;

  for (int i = 0; i < N; i++) {
    evaluator.rotate_vector(toAdd, -(int)pow(2, i), galois_keys, temp);
    evaluator.add_inplace(toAdd, temp);
  }
}

void cyclic_add_right(Ciphertext& toAdd, const GaloisKeys& galois_keys,
                      Evaluator& evaluator, Ciphertext& result, int N) {
  result = toAdd;
  cyclic_add_inplace_right(result, galois_keys, evaluator, N);
}

void peekAtCiphertext(const Ciphertext& ct, CKKSEncoder& encoder,
                      Decryptor& decryptor, int n) {
  vector<double> output;
  decrypt(ct, encoder, decryptor, output);
  headOfVector(output, n);
}

void encode(const vector<double>& vec, const double scale, CKKSEncoder& encoder,
            Plaintext& destination) {
  encoder.encode(vec, scale, destination);
}

void decode(const Plaintext& plaintext, CKKSEncoder& encoder,
            vector<double>& destination, double error) {
  encoder.decode(plaintext, destination);
  while (destination.size() > 0 && abs(destination.back()) < error)
    destination.pop_back();
}

void encrypt(const Plaintext& plaintext, const Encryptor& encryptor,
             Ciphertext& ciphertext) {
  encryptor.encrypt(plaintext, ciphertext);
}

void encrypt(const vector<double>& vec, const double scale,
             CKKSEncoder& encoder, const Encryptor& encryptor,
             Ciphertext& ciphertext) {
  Plaintext plaintext;
  encode(vec, scale, encoder, plaintext);
  encrypt(plaintext, encryptor, ciphertext);
}

void decrypt(const Ciphertext& ciphertext, Decryptor& decryptor,
             Plaintext& plaintext) {
  decryptor.decrypt(ciphertext, plaintext);
}

void decrypt(const Ciphertext& ciphertext, CKKSEncoder& encoder,
             Decryptor& decryptor, vector<double>& vec, double error) {
  Plaintext plaintext;
  decrypt(ciphertext, decryptor, plaintext);
  decode(plaintext, encoder, vec, error);
}

void saveParams(EncryptionParameters& parms, const string filename) {
  ofstream parms_enc_stream(filename, ios::binary);
#ifdef SEAL36
  parms.save(parms_enc_stream, compr_mode_type::zstd);
#else
  parms.save(parms_enc_stream);
#endif
  parms_enc_stream.close();
}

void loadParams(EncryptionParameters& parms, const string filename) {
  ifstream parms_enc_stream(filename, ios::binary);
  parms.load(parms_enc_stream);
  parms_enc_stream.close();
}
