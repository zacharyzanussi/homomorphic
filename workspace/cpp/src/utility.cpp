#include "utility.h"

#include <ctime>
#include <filesystem>
#include <functional>
#include <iostream>
#include <random>
#include <set>
#include <sstream>

using namespace std;


void print_matrix(vector<vector<double>>& matrix, size_t num) {
  size_t count = 1;
  size_t stop = num;
  for (const auto& row : matrix) {
    for (const auto& val : row) {
      cout << val << " ";
      if (count == stop && num != 0) {
        cout << "...";
        break;
      }
      count++;
    }
    cout << endl;
    stop--;
    if (stop == 0 && num != 0) {
      cout << "..." << endl;
      break;
    }
    count = 1;
  }
}

void random_weight_matrix(size_t rows, size_t cols,
                          vector<vector<double>>& matrix, string random,
                          bool random_seed) {
  function<float()> random_number;
  if (random == "normal") {
    random_number = [cols, random_seed]() {
      return get_normal_random(0, 1 / sqrt(cols), random_seed);
    };
  } else if (random == "uniform") {
    double bound = 1 / (double)sqrt((double)cols);
    cout << bound << endl;
    random_number = [bound, random_seed]() {
      return get_uniform_random(-1 * bound, bound, random_seed);
    };
  } else {
    throw invalid_argument("Invalid random number generator provided to random_matrix");
  }
  matrix.clear();

  vector<double> matrix_row(cols, 0.0);

  for (size_t i = 0; i < rows; i++) {
    for (size_t j = 1; j < cols; j++) {
      matrix_row[j] = random_number();
      // matrix_row[j] = j;
    }
    matrix.push_back(matrix_row);
    matrix_row.resize(cols, 0.0);
  }
}

void random_packed_weight_matrix(size_t rows, size_t cols, size_t num_packed,
                                 vector<vector<double>>& matrix, string random,
                                 bool random_seed) {
  function<float()> random_number;
  if (random == "normal") {
    random_number = [cols, random_seed]() {
      return get_normal_random(0, 1 / sqrt(cols), random_seed);
    };
  } else if (random == "uniform") {
    double bound = 1 / (double)sqrt((double)cols);
    cout << bound << endl;
    random_number = [bound, random_seed]() {
      return get_uniform_random(-1 * bound, bound, random_seed);
    };
  } else {
    cout << "Invalid random number generator provided to random_matrix" << endl;
    return;
  }
  matrix.clear();

  for (size_t i = 0; i < rows; i++) {
    vector<double> matrix_row;
    matrix_row.reserve(cols * num_packed);
    for (size_t j = 0; j < cols * num_packed; j++) {
      matrix_row.push_back(random_number());
      // matrix_row.push_back(j);
    }
    // Initialize the first element in every chunk to zero,
    // as this corresponds to the bias term
    for (size_t k = 0; k < cols * num_packed; k += cols) {
      matrix_row[k] = 0.0;
    }
    matrix.push_back(matrix_row);
  }
  // cout << "Just created a matrix with " << matrix.size() << " rows and " <<
  // matrix.at(0).size() << " columns " << endl;
}

float get_normal_random(float mean, float stddev, bool rand) {
  auto t = time(0);
  if (rand == false) {
    t = 3;
  }
  static default_random_engine e(t);
  // normal_distribution takes mean and standard deviation
  static normal_distribution<float> dis(mean, stddev);
  return dis(e);
}

float get_uniform_random(float min, float max, bool rand) {
  // static default_random_engine e;
  // if (rand == true) {
  //   e = default_random_engine(time(0));
  // } else {
  //   e = default_random_engine(3);
  // }
  auto t = time(0);
  if (rand == false) {
    t = 3;
  }
  static default_random_engine e(t);
  static std::uniform_real_distribution<> dis(min, max);
  return dis(e);
}

void print_chunks(const vector<double>& vect, size_t num_chunks, size_t num,
                  bool oneline, size_t chunk_size, size_t full_size) {
  if (vect.size() == 0) {
    cout << "Vector is empty" << endl;
    return;
  }
  if (chunk_size == 0) {
    chunk_size = ceil(vect.size() / (double)num_chunks);
  }

  size_t entry_field_size = 6;

  for (size_t i = 0; i < num_chunks; ++i) {
    size_t count = 1;
    cout << "[ ";
    for (vector<double>::const_iterator it = vect.begin() + i * chunk_size;
         it != vect.end(); ++it) {
      cout << right << setw(entry_field_size) << fixed << setprecision(2) << *it
           << " ";
      if (count == num) {
        cout << "... ";
        break;
      }
      ++count;
    }

    if (oneline) {
      cout << "]   ";
      if (full_size != 0) {
        cout << string((entry_field_size + 1) * (full_size - num), ' ');
      }
    } else {
      cout << "]" << endl;
    }
  }
  if (oneline) cout << endl;
}

void print_chunks(const vector<vector<double>>& mat, size_t num_chunks,
                  size_t num) {
  size_t stop = num;
  for (auto& row : mat) {
    print_chunks(row, num_chunks, stop, true);
    stop--;
    if (stop == 0 && num != 0) {
      cout << "..." << endl;
      break;
    }
  }
}

double vector_innerprod(const vector<double>& vec1,
                        const vector<double>& vec2) {
  if (vec1.size() != vec2.size()) {
    throw invalid_argument(
        "Vectors are different sizes: " + to_string(vec1.size()) + " and " +
        to_string(vec2.size()) + " in vector_innerprod");
  }

  double res = 0;
  for (size_t i = 0; i < vec1.size(); i++) {
    res += vec1[i] * vec2[i];
  }

  return res;
}

void matrix_vector_product(const vector<vector<double>>& matrix,
                           const vector<double>& vec, vector<double>& result) {
  size_t num_rows, num_cols;
  num_rows = matrix.size();
  num_cols = matrix[0].size();
  for (const auto& row : matrix) {
    if (row.size() != num_cols) {
      throw invalid_argument("Not all rows are the same size");
    }
  }
  if (num_cols != vec.size()) {
    print_vector(vec);
    throw invalid_argument("Matrix column number " + to_string(num_cols) +
                           " does not match vector size " +
                           to_string(vec.size()));
  }

  result.clear();
  result.reserve(num_rows);

  for (const auto& row : matrix) {
    result.push_back(vector_innerprod(row, vec));
  }
}

void load_data(string& line, vector<double>& vect, size_t number_ngrams,
               size_t hashor, bool normalize) {
  size_t initial_length = vect.size();
  // resize according to hash size
  if (hashor == 0) {
    vect.resize(initial_length + number_ngrams + 1, 0.0);
  } else {
    vect.resize(initial_length + hashor, 0.0);
  }

  // containers for line and columns
  stringstream s(line);
  string col;
  vector<string> cols;
  // Put separate columns into a vector 'cols'
  while (getline(s, col, ',')) {
    if (!col.empty() && col != "\r" && col != "\n") cols.push_back(col);
  }

  // Put all the ngrams into a vector 'ngrams'
  string ngram;
  vector<int> ngrams;
  for (size_t i = 2; i < cols.size(); i++) {
    stringstream t(cols[i]);
    while (getline(t, ngram, ' ')) {  // && !ngram.compare(" "
      try {
        ngrams.push_back(stoi(ngram));
      } catch (exception& e) {
        // cout << "sand" << ngram << "wich" << endl;
        cout << "What?:" << e.what() << endl;
        for (auto val : cols) {
          cout << val << endl;
        }
        cout << "bad ngram; " << ngram << endl;
        cout << "cols is size " << cols.size() << endl;
      }
    }
    // clear and deallocate the sstream
    stringstream().swap(t);
  }
  double total_ngrams = ngrams.size() + 1;

  function<int(int)> indexor;
  hash<string> shash;
  if (hashor == 0) {
    indexor = [](int val) { return val + 1; };
  } else {
    indexor = [shash, hashor](int val) {
      return 1 + (shash(to_string(val)) % (hashor - 1));
    };
  }

  double update_val = 1;
  if (normalize) {
    update_val = 1 / total_ngrams;
  }

  vect[initial_length] = update_val;
  for (const auto& val : ngrams) {
    // cout << val << endl;
    vect[indexor(val) + initial_length] =
        vect[indexor(val) + initial_length] + update_val;  // Adding all ngrams
    // vec[indexor(val) + initial_length] = update_val;            // Oring all
    // ngrams
  }

  // double sum = 0;
  // for (int i = initial_length; i < initial_length + hashor; ++i) {
  //   sum += vect[i];
  // }
  // cout << "sum = " << sum << endl;
}

void load_data_line(string& line, vector<double>& vect, size_t number_ngrams,
                    size_t hashor, bool normalize) {
  // destroy all elements
  vect.clear();
  //
  load_data(line, vect, number_ngrams, hashor, normalize);
}

void load_data_packed(vector<string>& lines, vector<double>& vect,
                      size_t number_ngrams, size_t hashor, bool normalize) {
  vect.clear();

  size_t num_packed = lines.size();

  if (hashor == 0) {
    vect.reserve(number_ngrams * num_packed);
  } else {
    vect.reserve(hashor * num_packed);
  }

  for (auto& line : lines) {
    load_data(line, vect, number_ngrams, hashor, normalize);
  }
}


void load_label_line(string& line, vector<double>& vect, size_t number_label) {
  vect.clear();
  vect.resize(number_label, 0.0);

  string label;
  stringstream s(line);
  getline(s, label, ',');
  try {
    vect[stoi(label)] = 1;
  } catch (exception& e) {
    cout << e.what() << endl;
    cout << "The offending argument: " << line << endl;
    throw e;
  }
}

void load_description(string line, string& desc) {
  stringstream s(line);
  string d;
  getline(s, d, ',');
  getline(s, desc, ',');
}

/*int argmax(vector<double> vect) {
    int argmax = -1;
    double max = -100;
    for (int i = 0; i < vect.size(); i++) {
        if (vect[i] > max) {
            argmax = i;
            max = vect[i];
        }
    }

    return argmax;
}*/

void statsOnVector(vector<int> vec) {
  set<int> setter(vec.begin(), vec.end());

  int number;
  for (const auto& elt : setter) {
    number = 0;
    for (const auto& val : vec) {
      if (val == elt) number++;
    }
    cout << "  " << elt << ": " << number << endl;
  }
}

int tally_activations(vector<vector<double>>& activations,
                      vector<vector<double>>& labels) {
  double total = activations.size();
  int correct = 0;
  int curr = -1;

  for (int i = 0; i < total; i++) {
    curr = argmax(activations[i]);
    if (almost_equal(labels[i][curr], 1.0, 2)) {
      correct++;
    }
  }
  return correct;
}

void check_for_file(string file_name) {
  if (!filesystem::exists(file_name)) {
    throw invalid_argument("file with name " + file_name + " doesn't exist!");
  }
}
