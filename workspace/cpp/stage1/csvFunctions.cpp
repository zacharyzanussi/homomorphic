#include "csvFunctions.h"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <set>

using namespace std;



void loadCSV(fstream* fin, RecordsMap& database)
{
	string str, word;
	vector<string> vec;

	while (getline(*fin, str))
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		database[recordkey(vec[0], vec[1], vec[2])].push_back(stof(vec[3]));
	}
}

void loadNCSVLines(fstream* fin, RecordsMap& database, int N)
{
	string str, word;
	vector<string> vec;

	int i = 0;
	while (getline(*fin, str) && i < N)
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		database[recordkey(vec[0], vec[1], vec[2])].push_back(stof(vec[3]));
		i++;
	}
}

void headOfVector(vector<double>& vec, int n)
{
	if (vec.empty() == true) {
		cout << "[  ]" << endl;
		return;
	}
	cout << "[ ";
	int i = 0;
	for (vector<double>::iterator it = vec.begin(); it != vec.end() && i < n; ++it) {
		cout << *it << " ";
		i++;
	}
	cout << "... ";
	vector<double> temp(vec.end() - n / 2, vec.end());
	i = 0;
	for (vector<double>::iterator it = temp.begin(); it != temp.end() && i < n / 2; ++it) {
		cout << *it << " ";
		i++;
	}
	cout << "]" << endl;
}

bool equalWithinEpsilon(double val1, double val2, double epsilon)
{
	if (val1 > val2)
		return val1 - val2 <= epsilon;
	else
		return val2 - val1 <= epsilon;
}

bool listWithinEpsilon(vector<double>& vec1, vector<double>& vec2, double epsilon)
{
	size_t len1 = vec1.size();
	size_t len2 = vec2.size();
	if (len1 != len2) {
		cout << "Vectors are different lengths, listWithinEpsilon returning False" << endl;
		return false;
	}

	vector<size_t> notEqual;
	for (size_t i = 0; i < len1; i++) {
		if (!equalWithinEpsilon(vec1.at(i), vec2.at(i), epsilon)) {
			notEqual.push_back(i);
		}
	}

	if (!notEqual.empty()) {
		cout << "Elements at position ";
		for (vector<size_t>::iterator it = notEqual.begin(); it != notEqual.end(); ++it) {
			cout << *it << " ";
		}
		cout << "are not within an epsilon of " << epsilon << endl;
		cout << "listWithinEpsilon returning False." << endl;
		return false;
	}

	return true;
}

double percentageError(double val1, double val2)
{
	if (val1 == 0 && val2 == 0)
		return 0;
	

	double ab = abs(val1 - val2);
	double mean = (val1 + val2) / 2.0;

	return ab / mean;
	}

bool withinPercentageError(double val1, double val2, double error)
{
	if (percentageError(val1, val2) <= error)
		return true;
	else
		return false;
}

bool listWithinPercentageError(vector<double>& vec1, vector<double>& vec2, double error)
{
	size_t len1 = vec1.size();
	size_t len2 = vec2.size();
	if (len1 != len2) {
		cout << "Vectors are different lengths, listWithinPercentageError returning False" << endl;
		return false;
	}

	vector<size_t> notEqual;
	for (size_t i = 0; i < len1; i++) {
		if (!withinPercentageError(vec1.at(i), vec2.at(i), error)) {
			notEqual.push_back(i);
		}
	}

	if (!notEqual.empty()) {
		cout << "Elements at position ";
		for (vector<size_t>::iterator it = notEqual.begin(); it != notEqual.end(); ++it) {
			cout << *it << " ";
		}
		cout << "are not within percentage error of " << error << endl;
		cout << "listWithinPercentageError returning False." << endl;
		return false;
	}

	return true;
}

double total(vector<double>& vec)
{
	if (vec.empty())
		return 0;

	double sum = 0;
	for (vector<double>::iterator it = vec.begin(); it != vec.end(); ++it) {
		sum += *it;
	}

	return sum;
}

double mean(vector<double>& vec)
{
	return total(vec) / vec.size();
}

double variance(vector<double>& vec)
{
	double meanVal = mean(vec);

	double var = 0;
	for (vector<double>::iterator it = vec.begin(); it != vec.end(); ++it) {
		var += pow(*it - meanVal, 2);
	}
	return var / vec.size();
}

int equalToDecimalPlace(double val1, double val2)
{
	double diff;
	diff = abs(val1 - val2);
	if (diff == 0)
		return 0;
	ostringstream stream;
	stream << setprecision(17) << fixed;

	stream << diff;
	string sdiff = stream.str();
	int firstNonzero = sdiff.find_first_of("123456789");
	int locationOfDecimal = sdiff.find('.');
	string digitsFiveOrLarger = "56789";

	if (digitsFiveOrLarger.find(sdiff.at(firstNonzero)) != -1)
		firstNonzero--;

	return firstNonzero - locationOfDecimal - 1;
}

int equalToTensPlace(double val1, double val2)
{
	double rd1, rd2;

	int i = ceil(log10(max(val1, val2)));

	bool done = false;
	while (done == false) {
		rd1 = round(val1 * pow(10, -i)) / pow(10, -i);
		rd2 = round(val2 * pow(10, -i)) / pow(10, -i);
		if (rd1 != rd2)
			done = true;
		else
			i--;
	}
	return i;
}

int digitsOfPrecision(double val1, double val2)
{
	double rd1, rd2;

	int i = ceil(log10(max(val1, val2)));
	int digits = -1;
	bool done = false;
	while (done == false) {
		rd1 = round(val1 * pow(10, -i)) / pow(10, -i);
		rd2 = round(val2 * pow(10, -i)) / pow(10, -i);
		if (rd1 != rd2)
			done = true;
		else {
			i--;
			digits++;
		}
	}
	return digits;
}

bool listOfPrecision(vector<double> vec1, vector<double> vec2, int minDigits)
{
	size_t len1 = vec1.size();
	size_t len2 = vec2.size();
	if (len1 != len2) {
		cout << "Vectors are different lengths, listOfPrecision returning False" << endl;
		return false;
	}

	vector<size_t> notEqual;
	for (size_t i = 0; i < len1; i++) {
		if (digitsOfPrecision(vec1.at(i), vec2.at(i)) < minDigits) {
			notEqual.push_back(i);
		}
	}

	if (!notEqual.empty()) {
		cout << "Elements at position ";
		for (vector<size_t>::iterator it = notEqual.begin(); it != notEqual.end(); ++it) {
			cout << *it << " ";
		}
		cout << "are not accurate to " << minDigits << " digits" << endl;
		cout << "listOfPrecision returning False." << endl;
		return false;
	}

	return true;
}

void statsOnVector(vector<int> vec)
{
	set<int> setter (vec.begin(), vec.end());
	
	int number;
	for (const auto& elt : setter) {
		number = 0;
		for (const auto& val : vec) {
			if (val == elt)
				number++;
		}
		cout << "  " << elt << ": " << number << endl;
	}
}

void loadNValues(fstream* fin, vector<double>& vec, int num)
{
	string str, word;
	vector<string> temp;

	vec.clear();
	vec.reserve(num);
	int i = 0;
	while (getline(*fin, str) && i < num) {
		stringstream s(str);

		temp.clear();
		while (getline(s, word, ',')) {
			temp.push_back(word);
		}

		vec.push_back(stof(temp[3]));
		i++;
	}

}
