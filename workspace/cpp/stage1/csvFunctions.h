#ifndef CSVFUNCTIONS_H
#define CSVFUNCTIONS_H

#include <fstream>
#include <vector>
#include "recordkey.h"


using namespace std;

void loadCSV(fstream* fin, RecordsMap& database);

void loadNCSVLines(fstream* fin, RecordsMap& database, int N);

void headOfVector(vector<double>& vec, int n = 4);

bool equalWithinEpsilon(double val1, double val2, double epsilon = 1e-5);

bool listWithinEpsilon(vector<double>& vec1, vector<double>& vec2, double epsilon = 1e-5);

double percentageError(double val1, double val2);

bool withinPercentageError(double val1, double val2, double error);

bool listWithinPercentageError(vector<double>& vec1, vector<double>& vec2, double error);

double total(vector<double>& vec);

double mean(vector<double>& vec);

// Calculates the population variance of the values in the vector vec.
double variance(vector<double>& vec);

// Gives the lowest decimal place that the two values are equal to
int equalToDecimalPlace(double val1, double val2);

// This function is bad; do not use it.
int equalToTensPlace(double val1, double val2);

// Gives the number of digits that the two values are equal to; that is, how many decimal places can you 
// round to and still get the same number? For example, should return 2 for 768.4 and 768.5, because you can 
// round to the hundreth, or the tenth, but not the oneth.
int digitsOfPrecision(double val1, double val2);

bool listOfPrecision(vector<double> vec1, vector<double> vec2, int minDigits = 4);

// Gives a 'histogram' of the elements in vec, printing a list of the distinct elements and the number of each.
void statsOnVector(vector<int> vec);

void loadNValues(fstream* fin, vector<double>& vec, int num);

#endif // !CSVFUNCTIONS_H