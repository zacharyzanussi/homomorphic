// finalProducts.cpp - This is where we will store the "final drafts" of code that we have created.

#include "recordkey.h"
#include "sealFunctions.h"
#include "csvFunctions.h"
//#include <iostream>
//#include <string>
//#include <vector>
#include <chrono>
#include <iomanip>
#include <direct.h>
#include "json.hpp"

using namespace std;
using namespace seal;

void entire_dataset_stats();

void save_experimentation();

void load_experimentation();

void generate_configfile(string str);

void generate_configfile_consumer(string str);

void generate_configfile_client(string str);

void generate_configfile_cloud(string str);

void save_several(string configIn, string configOut);

void load_several(string config);

void consumer_keygen(string config, string clientConfig, string cloudConfig);

void client_encrypt(string config, string consumerConfig, string cloudConfig);

void cloud_compute(string config, string consumerConfig, string clientConfig, string toConsumerConfig);

void consumer_decrypt(string config, string cloudConfig);

int main(int argc, char** argv)
{
	//entire_dataset_stats();
	
	//generate_configfile("configFileSave.json");

	//save_several(argv[1], argv[2]);

	//load_several(argv[2]);

	//generate_configfile_consumer(argv[1]);

	//generate_configfile_client(argv[2]);

	//generate_configfile_cloud(argv[3]);
	

	nlohmann::json globalConfigList = {
		{"consumerConfigLocation", argv[1]},
		{"clientConfigLocation", argv[2]},
		{"cloudConfigLocation", argv[3]},
		{"ConsumerToClientConfigLocation", "toClient/configFromConsumer.json"},
		{"ConsumerToCloudConfigLocation", "toCloud/configFromConsumer.json"},
		{"ClientToCloudConfigLocation", "toCloud/configFromClient.json"},
		{"CloudToConsumerConfigLocation", "toConsumer/configFromCloud.json"} 
	};

	chrono::high_resolution_clock::time_point time_start, time_end;
	chrono::microseconds time_diff;

	time_start = chrono::high_resolution_clock::now();
	

	cout << endl << "Beginning a full round of Homomorphic Encryption on scanner data" << endl;

	cout << "Using config files " << argv[1] << ", " << argv[2] << ", " << argv[3] << endl << endl;
	cout << endl << "============= CONSUMER SIDE (eg Statcan) =================" << endl;
	consumer_keygen(globalConfigList["consumerConfigLocation"], globalConfigList["ConsumerToClientConfigLocation"], globalConfigList["ConsumerToCloudConfigLocation"]);

	cout << endl << "============= CLIENT SIDE (eg any major retailer) ========" << endl;
	client_encrypt(globalConfigList["clientConfigLocation"], globalConfigList["ConsumerToClientConfigLocation"], globalConfigList["ClientToCloudConfigLocation"]); 

	cout << endl << "============= CLOUD SIDE (eg Azure) ======================" << endl;
	cloud_compute(globalConfigList["cloudConfigLocation"], globalConfigList["ConsumerToCloudConfigLocation"], globalConfigList["ClientToCloudConfigLocation"], globalConfigList["CloudToConsumerConfigLocation"]);

	cout << endl << "============= Back to CONSUMER ===========================" << endl;
	consumer_decrypt(globalConfigList["consumerConfigLocation"], globalConfigList["CloudToConsumerConfigLocation"]);


	time_end = chrono::high_resolution_clock::now();
	time_diff = chrono::duration_cast<chrono::microseconds>(time_end - time_start);
	cout << "This entire process took " << time_diff.count() / 1000 << " milliseconds" << endl;
	/*size_t n = 14;
	
	for (int i = 0; i < 20; i++) {
		cout << (size_t) max(pow(2, n - (double)i - 2), 1.0) << endl;
	}
	cout << endl << n - 14 << endl;
	cout << n - 15.0 << endl;*/

	return 0;
}

// In this file, we set up and save the context, generate and save the keys, and create config files for client and cloud. 
void consumer_keygen(string config, string clientConfig, string cloudConfig)
{
	//Load the configIn file
	fstream fconfigIn(config, ios::in);
	nlohmann::json configInfo;
	fconfigIn >> configInfo;
	//

	EncryptionParameters parms(scheme_type::CKKS);

	chrono::high_resolution_clock::time_point time_start_total, time_end_total;
	chrono::microseconds time_diff_total;

	time_start_total = chrono::high_resolution_clock::now();


	size_t poly_modulus_degree = configInfo["parms"]["poly_modulus_degree"];
	vector<int> chain = configInfo["parms"]["chain"];
	double scale = configInfo["parms"]["scale"];
	sec_level_type secLevel;
	switch (configInfo["parms"]["security"].get<int>()) {
	case 128:
		secLevel = sec_level_type::tc128;
		break;
	case 192:
		secLevel = sec_level_type::tc192;
		break;
	case 256:
		secLevel = sec_level_type::tc256;
	default:
		cout << "Error: bad security level" << endl;
		return;
	}

	cout << "Initializing SEAL Parameters... ";
	parms.set_poly_modulus_degree(poly_modulus_degree);
	parms.set_coeff_modulus(CoeffModulus::Create(poly_modulus_degree, chain));

	auto context = SEALContext::Create(parms, true, secLevel);

	KeyGenerator keygen(context);
	auto public_key = keygen.public_key();
	auto secret_key = keygen.secret_key();
	auto relin_keys = keygen.relin_keys();
	auto gal_keys = keygen.galois_keys();
	//Encryptor encryptor(context, public_key);
	//Evaluator evaluator(context);
	//Decryptor decryptor(context, secret_key);
	cout << "Done!" << endl;

	cout << " --- Parameters; " << endl;
	cout << "   - poly mod : " << poly_modulus_degree << " which is 2^" << configInfo["parms"]["poly_modulus_exponent"].get<int>() << endl;
	cout << "   - chain    : ";
	for (const auto& val : chain)
		cout << val << ", ";
	cout << endl;
	cout << "   - security level: " << configInfo["parms"]["security"].get<int>() << endl;
	cout << endl;

	//CKKSEncoder encoder(context);

	// Make the necessary directories
	cout << "Setting up the filesystem... ";
	if (!_mkdir(configInfo["storageLocations"].at(0).get<string>().c_str()))
		cout << "Directory Created!" << endl;
	if (!_mkdir(configInfo["storageLocations"].at(1).get<string>().c_str()))
		cout << "Directory Created!" << endl;
	if (!_mkdir(configInfo["storageLocations"].at(2).get<string>().c_str()))
		cout << "Directory Created!" << endl;
	cout << "Done!" << endl;
	//

	// Save the keys in the right spots
	cout << "Saving the keys... ";
	fstream fparms, pubKey, secKey, relinKey, galKey;

	fparms.open(configInfo["ParmsLocation"].get<string>(), ios::binary | ios::out);
	parms.save(fparms);
	fparms.close();

	fparms.open(configInfo["CloudParmsLocation"].get<string>(), ios::binary | ios::out);
	parms.save(fparms);
	fparms.close();

	pubKey.open(configInfo["PublicKeyLocation"].get<string>(), ios::binary | ios::out);
	public_key.save(pubKey);
	pubKey.close();

	secKey.open(configInfo["SecretKeyLocation"].get<string>(), ios::binary | ios::out);
	secret_key.save(secKey);
	pubKey.close();

	relinKey.open(configInfo["RelinKeyLocation"].get<string>(), ios::binary | ios::out);
	relin_keys.save(relinKey);
	relinKey.close();

	galKey.open(configInfo["GaloisKeyLocation"].get<string>(), ios::binary | ios::out);
	gal_keys.save(galKey);
	galKey.close();
	cout << "Done!" << endl;
	//

	// Make the config files
	cout << "Generating config files... ";
	nlohmann::json clientJson, cloudJson;

	clientJson["ParmsLocation"] = configInfo["ParmsLocation"];
	clientJson["PublicKeyLocation"] = configInfo["PublicKeyLocation"];
	clientJson["security"] = configInfo["parms"]["security"];
	clientJson["scale"] = configInfo["parms"]["scale"];
	clientJson["RecordKeyLocation"] = configInfo["RecordKeyLocation"];
	
	cloudJson["ParmsLocation"] = configInfo["CloudParmsLocation"];
	cloudJson["RelinKeyLocation"] = configInfo["RelinKeyLocation"];
	cloudJson["GaloisKeyLocation"] = configInfo["GaloisKeyLocation"];
	cloudJson["security"] = configInfo["parms"]["security"];
	cloudJson["OutputCtLocation"] = configInfo["OutputCtLocation"];

	fstream fclient, fcloud;
	
	fclient.open(clientConfig, ios::out);
	fclient << clientJson.dump(4) << endl;

	fcloud.open(cloudConfig, ios::out);
	fcloud << cloudJson.dump(4) << endl;
	cout << "Done!" << endl;
	//

	time_end_total = chrono::high_resolution_clock::now();
	time_diff_total = chrono::duration_cast<chrono::microseconds>(time_end_total - time_start_total);

	cout << "Keygen took " << time_diff_total.count() / 1000 << " milliseconds" << endl;
}

void client_encrypt(string config, string consumerConfig, string cloudConfig)
{
	//Load the configIn file
	fstream fconfigIn(config, ios::in);
	nlohmann::json configInfo;
	fconfigIn >> configInfo;
	fconfigIn.close();

	fconfigIn.open(consumerConfig, ios::in);
	nlohmann::json consConfig;
	fconfigIn >> consConfig;
	fconfigIn.close();
	
	//

	chrono::high_resolution_clock::time_point time_start_total, time_end_total;
	chrono::microseconds time_diff_total;

	time_start_total = chrono::high_resolution_clock::now();


	cout << "Loading the data... ";
	fstream data(configInfo["Data"].get<string>(), ios::in);
	RecordsMap databaseLarge;
	vector<RecordsMap> databases;

	string columnVals;
	getline(data, columnVals);
	int numberOfLines = configInfo["numberOfLines"];
	loadNCSVLines(&data, databaseLarge, numberOfLines);
	cout << "Done! Loaded " << numberOfLines << " lines, with " << databaseLarge.size() << " groups" << endl;

	cout << "Separating data... ";
	RecordsMap loading;
	RecordsMap singletons;
	getListsOfSize(databaseLarge, singletons, 0, 2);
	int listsGathered = singletons.size();
	int listSize = 1;
	int maximumListSize = 20; //This is here to prevent an infinite loop
	while (listsGathered < databaseLarge.size() && listSize < maximumListSize) {
		getListsOfSize(databaseLarge, loading, pow(2, listSize - 1) + 1, pow(2, listSize) + 1);
		listsGathered += loading.size();
		databases.push_back(loading);
		listSize++;
	}
	cout << "Done! Here is a sample of the database:" << endl;
	printRecordsMap(databases.front(), 1);

	cout << endl << "Size of sublists of databases: ";
	for (const auto& list : databases) {
		cout << list.size() << ", ";
	}
	cout << endl;

	// While we are here, let's compute and save the true results;
	ResultsMap trueRes;
	computeResultsMap(databaseLarge, trueRes);
	ofstream ftrue("trueResults.csv", ios::out);
	saveResultsMap(trueRes, ftrue);
	ftrue.close();
	//
	/*ftrue.open("singletonsResult.csv", ios::out);
	ResultsMap singles;
	computeResultsMap(singletons, singles);
	saveResultsMap(sing)*/

	cout << "Grabbing recordkeys... ";
	vector<vector<recordkey>> allRecordkeys;
	vector<recordkey> recordkeyLoader;

	for (auto& database : databases) {
		getRecordKeys(database, recordkeyLoader);
		allRecordkeys.push_back(recordkeyLoader);
	}
	cout << "Done." << endl;

	

	fstream fRkeys;
	string rkPrefix = consConfig["RecordKeyLocation"];
	if (!_mkdir(rkPrefix.c_str()))
		cout << "Directory Created!" << endl;
	stringstream rkey;

	fRkeys.open(rkPrefix + "singletons", ios::out);
	saveSingletons(singletons, fRkeys);
	fRkeys.close();

	for (int i = 0; i < allRecordkeys.size(); i++) {
		rkey << setw(2) << setfill('0') << i;
		fRkeys.open(rkPrefix + "rk" + rkey.str(), ios::out);
		saveRecordKeys(allRecordkeys.at(i), fRkeys);
		rkey.str("");
		fRkeys.close();
	}

	// Time to reinitiate the SEAL context
	cout << endl << "Initializing SEAL context... ";
	sec_level_type secLevel;
	switch (consConfig["security"].get<int>()) {
	case 128:
		secLevel = sec_level_type::tc128;
		break;
	case 192:
		secLevel = sec_level_type::tc192;
		break;
	case 256:
		secLevel = sec_level_type::tc256;
	default:
		cout << "Error: bad security level" << endl;
		return;
	}
	
	EncryptionParameters parms;
	
	fstream fparm(consConfig["ParmsLocation"].get<string>(), ios::binary | ios::in);
	parms.load(fparm);
	size_t n = log2(parms.poly_modulus_degree());
	size_t scale = consConfig["scale"];

	auto context = SEALContext::Create(parms, true, secLevel);
	fstream fpub(consConfig["PublicKeyLocation"].get<string>(), ios::binary | ios::in);
	PublicKey public_key;
	public_key.load(context, fpub);

	CKKSEncoder encoder(context);
	Encryptor encryptor(context, public_key);
	cout << "Done!" << endl;

	vector<vector<double>> scanners;
	vector<double> encryptLoader;
	vector<size_t> listLengths;

	vector<Ciphertext> smallCtsLoader;
	vector<Plaintext> specialPtsLoader;
	vector<vector<Plaintext>> smallPtsListsLoader;

	fstream textsOut;

	string ctPrefix;
	ctPrefix = configInfo["ctLocation"];
	string ptPrefix;
	ptPrefix = configInfo["ptLocation"];
	int numberOfCts = 0;
	int numberOfCtsThisRound = 0;
	int numberOfPtSets = 0;
	stringstream number;

	vector<string> Ctfilenames;
	vector<string> Ptfilenames;
	vector<string> ptPrefixes = { "sqN", "sq3N", "fixN", "N" };
	vector<string> numbers;
	string filename;
	if (!_mkdir(ctPrefix.c_str()))
		cout << "Directory Created!" << endl;

	
	if (!_mkdir(ptPrefix.c_str()))
		cout << "Directory Created!" << endl;

	nlohmann::json ctArray;

	for (int i = 0; i < min(n - 2, databases.size()); i++) {
		cout << " -- For size 2^" << i + 1 << ", ";
		getVectors(databases.at(i), scanners);
		numberOfCtsThisRound = 0;
		for (const auto& list : scanners) {
			for (const auto& val : list)
				encryptLoader.push_back(val);
			listLengths.push_back(list.size());
			for (int j = list.size(); j < pow(2, i + 1); j++) {
				encryptLoader.push_back(0);
			}
		}
		cout << "collected " << scanners.size() << " groups, (";
		if (scanners.size() != 0) {
			encryptVector(encryptLoader, scale, encoder, encryptor, smallCtsLoader);
			cout << "Enc, ";
			preparePlaintexts(listLengths, scale, encoder, smallPtsListsLoader);
			cout << "Pt, ";
			

			for (int j = 0; j < smallCtsLoader.size(); j++) {
				number << setw(4) << setfill('0') << numberOfCts;
				textsOut.open(filename = (ctPrefix + "ct" + number.str()), ios::binary | ios::out);
				smallCtsLoader.at(j).save(textsOut);
				textsOut.close();
				Ctfilenames.push_back(filename);
				numbers.push_back(number.str());

				for (int k = 0; k < 4; k++) {
					textsOut.open(filename = (ptPrefix + ptPrefixes.at(k) + number.str()), ios::binary | ios::out);
					smallPtsListsLoader.at(j).at(k).save(textsOut);
					textsOut.close();
					Ptfilenames.push_back(filename);
				}

				number.str("");
				numberOfCts++;
				numberOfCtsThisRound++;
			}
		}
		ctArray[i] = numbers;
		numbers.clear();
		number << setw(2) << setfill('0') << i;
		specialPlaintexts(i + 1, scale, encoder, specialPtsLoader);
		cout << "sPt) ";
		textsOut.open(ptPrefix + "zeroOne" + number.str(), ios::binary | ios::out);
		specialPtsLoader.at(0).save(textsOut);
		textsOut.close();
		textsOut.open(ptPrefix + "ones" + number.str(), ios::binary | ios::out);
		specialPtsLoader.at(1).save(textsOut);
		textsOut.close();
		number.str("");

		cout << "and saved " << numberOfCtsThisRound << " cts" << endl;

		specialPtsLoader.clear();
		smallPtsListsLoader.clear();
		smallCtsLoader.clear();
		encryptLoader.clear();
		listLengths.clear();
	}

	//Now we do large Cts
	nlohmann::json largeCtArray;
	vector<Ciphertext> largeCtsLoader;
	vector<Plaintext> largePtsListsLoader;
	fstream fCt;
	vector<vector<string>> largeNumbers;
	vector<string> loader;
	size_t numberOfLargeCtsThisRound = 0;

	for (int i = n - 2; i < databases.size(); i++) {
		cout << " -- For size 2^" << i + 1 << ", ";
		getVectors(databases.at(i), scanners);
		numberOfCtsThisRound = 0;
		cout << "collected " << scanners.size() << " groups, ";
		for (int k = 0; k < pow(2, i - n + 2); k++)
			largeNumbers.push_back(loader);
		for (auto& list : scanners) {
			number << setw(4) << setfill('0') << numberOfCts;
			encryptVector(list, scale, encoder, encryptor, largeCtsLoader);
			for (int k = 0; k < largeCtsLoader.size(); k++) {
				fCt.open(ctPrefix + "ct" + number.str() + "-" + to_string(k), ios::binary | ios::out);
				largeCtsLoader.at(k).save(fCt);
				fCt.close();
			}
			largeNumbers.at(largeCtsLoader.size() - 1).push_back(number.str());

			numberOfLargeCtsThisRound += largeCtsLoader.size();
			preparePlaintexts(list.size(), scale, encoder, largePtsListsLoader);

			for (int k = 0; k < 4; k++) {
				textsOut.open(ptPrefix + ptPrefixes.at(k) + number.str(), ios::binary | ios::out);
				largePtsListsLoader.at(k).save(textsOut);
				textsOut.close();
			}
			numberOfCts++;
			number.str("");
			numbers.clear();
			
		}
		cout << "and saved " << numberOfLargeCtsThisRound << " cts" << endl;
		numberOfLargeCtsThisRound = 0;
	}
	largeCtArray = largeNumbers;

	// Create the cloud config file
	nlohmann::json cloudConfigJson;
	cloudConfigJson["ctLocation"] = configInfo["ctLocation"];
	cloudConfigJson["ptLocation"] = configInfo["ptLocation"];
	cloudConfigJson["cts"] = Ctfilenames;
	cloudConfigJson["pts"] = Ptfilenames;
	cloudConfigJson["numberOfCts"] = numberOfCts;
	cloudConfigJson["ctNumbers"] = ctArray;
	cloudConfigJson["largeCtNumbers"] = largeCtArray;

	//TO DO Large lists

	fstream fcloud(cloudConfig, ios::out);
	fcloud << cloudConfigJson.dump(4);


	time_end_total = chrono::high_resolution_clock::now();
	time_diff_total = chrono::duration_cast<chrono::microseconds>(time_end_total - time_start_total);
	cout << "Encryption took " << time_diff_total.count() / 1000 << " milliseconds" << endl;
}

void cloud_compute(string config, string consumerConfig, string clientConfig, string toConsumerConfig)
{
	//Load the configIn file
	fstream fconfigIn(config, ios::in);
	nlohmann::json configInfo;
	fconfigIn >> configInfo;
	fconfigIn.close();

	fconfigIn.open(consumerConfig, ios::in);
	nlohmann::json consConfig;
	fconfigIn >> consConfig;
	fconfigIn.close();

	fconfigIn.open(clientConfig, ios::in);
	nlohmann::json cliConfig;
	fconfigIn >> cliConfig;
	fconfigIn.close();
	//

	chrono::high_resolution_clock::time_point time_start_total, time_end_total;
	chrono::microseconds time_diff_total;

	time_start_total = chrono::high_resolution_clock::now();

	// Load SEAL context
	cout << endl << "Loading the previous SEAL context... ";
	sec_level_type secLevel;
	switch (consConfig["security"].get<int>()) {
	case 128:
		secLevel = sec_level_type::tc128;
		break;
	case 192:
		secLevel = sec_level_type::tc192;
		break;
	case 256:
		secLevel = sec_level_type::tc256;
	default:
		cout << "Error: bad security level" << endl;
		return;
	}

	EncryptionParameters parms;

	fstream fparm(consConfig["ParmsLocation"].get<string>(), ios::binary | ios::in);
	parms.load(fparm);
	size_t n = log2(parms.poly_modulus_degree());

	auto context = SEALContext::Create(parms, true, secLevel);

	GaloisKeys gal_keys;
	RelinKeys relin_keys;

	fstream fGal(consConfig["GaloisKeyLocation"].get<string>(), ios::binary | ios::in);
	gal_keys.load(context, fGal);
	fstream fRelin(consConfig["RelinKeyLocation"].get<string>(), ios::binary | ios::in);
	relin_keys.load(context, fRelin);

	Evaluator evaluator(context);
	cout << "Done!" << endl;
	//

	cout << "Beginning to compute statistics:" << endl;
	vector<vector<string>> ctArray;
	ctArray = cliConfig["ctNumbers"].get<vector<vector<string>>>();

	vector<string> numberList;
	vector<string> ptPrefixes = { "sqN", "sq3N", "fixN", "N" };
	vector<string> ctPrefixes = { "tot", "mean", "var" };
	//cout << ctArray.size() << endl;

	stringstream number;

	vector<Plaintext> specialPts;
	Plaintext zeroOne, ones, ptLoad;
	vector<Plaintext> pts;
	Ciphertext ct;
	vector<Ciphertext> outputCt;
	fstream textIn, resOut;

	string ptPrefix(cliConfig["ptLocation"].get<string>());
	string ctPrefix(cliConfig["ctLocation"].get<string>());
	string savePrefix(consConfig["OutputCtLocation"].get<string>()); 


	if (!_mkdir(savePrefix.c_str()))
		cout << "Directory Created!" << endl;

	for (int i = 0; i < ctArray.size(); i++) {
		numberList = ctArray.at(i);
		number << setw(2) << setfill('0') << i;
		cout << "Beginning lists size at most 2^" << i + 1 << "... ";
		textIn.open(ptPrefix + "zeroOne" + number.str(), ios::binary | ios::in);
		zeroOne.load(context, textIn);
		textIn.close();
		textIn.open(ptPrefix + "ones" + number.str(), ios::binary | ios::in);
		ones.load(context, textIn);
		textIn.close();

		specialPts.push_back(zeroOne);
		specialPts.push_back(ones);

		number.str("");

		for (auto& num : numberList) {

			for (int k = 0; k < 4; k++) {
				textIn.open(ptPrefix + ptPrefixes.at(k) + num, ios::binary | ios::in);
				ptLoad.load(context, textIn);
				textIn.close();
				pts.push_back(ptLoad);
			}
			textIn.open(ctPrefix + "ct" + num, ios::binary | ios::in);
			ct.load(context, textIn);
			textIn.close();

			homomorphicStats(ct, pts, specialPts, gal_keys, evaluator, relin_keys, outputCt, i + 1);

			for (int k = 0; k < 3; k++) {
				resOut.open(savePrefix + ctPrefixes.at(k) + num, ios::binary | ios::out);
				outputCt.at(k).save(resOut);
				resOut.close();
			}

			pts.clear();
		}
		cout << "Done!" << endl;
		specialPts.clear();
	}

	vector<vector<string>> largeCtArray;
	largeCtArray = cliConfig["largeCtNumbers"].get<vector<vector<string>>>();
	vector<Ciphertext> cts;

	for (int i = 0; i < largeCtArray.size(); i++) {
		numberList = largeCtArray.at(i);
		number << setw(2) << setfill('0') << i;
		cout << "Beginning lists size at most 2^" << n - 2 + i + 1 << "... ";

		number.str("");

		for (auto& num : numberList) {

			for (int k = 0; k < 4; k++) {
				textIn.open(ptPrefix + ptPrefixes.at(k) + num, ios::binary | ios::in);
				ptLoad.load(context, textIn);
				textIn.close();
				pts.push_back(ptLoad);
			}
			for (int k = 0; k < i + 1; k++) {
				textIn.open(ctPrefix + "ct" + num + "-" + to_string(k), ios::binary | ios::in);
				ct.load(context, textIn);
				textIn.close();
				cts.push_back(ct);
			}

			homomorphicStats(cts, pts, gal_keys, evaluator, relin_keys, outputCt);

			for (int k = 0; k < 3; k++) {
				resOut.open(savePrefix + ctPrefixes.at(k) + num, ios::binary | ios::out);
				outputCt.at(k).save(resOut);
				resOut.close();
			}

			cts.clear();
			pts.clear();
		}
		cout << "Done!" << endl;
	}

	nlohmann::json toConsumerJson;

	toConsumerJson["ctNumbers"] = cliConfig["ctNumbers"]; //Maybe client could send this to consumer and we wouldn't need this config file?
	toConsumerJson["largeCtNumbers"] = cliConfig["largeCtNumbers"];
	fstream toConOut(toConsumerConfig, ios::out);
	toConOut << toConsumerJson.dump(4);

	time_end_total = chrono::high_resolution_clock::now();
	time_diff_total = chrono::duration_cast<chrono::microseconds>(time_end_total - time_start_total);
	cout << "Computation took " << time_diff_total.count() / 1000 << " milliseconds" << endl;
}

void consumer_decrypt(string config, string cloudConfig)
{
	//Load the configIn file
	fstream fconfigIn(config, ios::in);
	nlohmann::json configInfo;
	fconfigIn >> configInfo;
	fconfigIn.close();

	fconfigIn.open(cloudConfig, ios::in);
	nlohmann::json cloConfig;
	fconfigIn >> cloConfig;
	fconfigIn.close();
	//

	chrono::high_resolution_clock::time_point time_start_total, time_end_total;
	chrono::microseconds time_diff_total;

	time_start_total = chrono::high_resolution_clock::now();


	// Load SEAL context
	cout << endl << "Loading the previous SEAL context... ";
	sec_level_type secLevel;
	switch (configInfo["parms"]["security"].get<int>()) {
	case 128:
		secLevel = sec_level_type::tc128;
		break;
	case 192:
		secLevel = sec_level_type::tc192;
		break;
	case 256:
		secLevel = sec_level_type::tc256;
	default:
		cout << "Error: bad security level" << endl;
		return;
	}

	EncryptionParameters parms;
	fstream fparm(configInfo["ParmsLocation"].get<string>(), ios::binary | ios::in); //Technically, we've just recovered parms from the client, but there is no reason we can't store parms locally as well.
	parms.load(fparm);
	size_t n = log2(parms.poly_modulus_degree());

	auto context = SEALContext::Create(parms, true, secLevel);
	SecretKey secret_key;
	fstream fSKey(configInfo["SecretKeyLocation"].get<string>(), ios::binary | ios::in);
	secret_key.load(context, fSKey);

	CKKSEncoder encoder(context);
	Decryptor decryptor(context, secret_key);
	cout << "Done!" << endl;
	//

	// Now we begin decryption; first for small ct's
	cout << "Beginning decryption: " << endl;
	vector<vector<string>> ctArray;
	ctArray = cloConfig["ctNumbers"].get<vector<vector<string>>>();
	Ciphertext ct;
	vector<Ciphertext> outputCts;
	vector<recordkey> recordKeys;
	vector<vector<double>> results;
	ResultsMap outputMap;

	fstream fkey, fct;

	string keyPrefix(configInfo["RecordKeyLocation"]);
	string ctPrefix(configInfo["OutputCtLocation"]);
	vector<string> ctPrefixes = { "tot", "mean", "var" };
	stringstream number;
	size_t chunksPerCiphertext;
	size_t iter;
	int lastNum = 0;

	vector<recordkey>::iterator rkIt;
	for (int i = 0; i < ctArray.size(); i++) {
		number << setw(2) << setfill('0') << i;
		fkey.open(keyPrefix + "rk" + number.str(), ios::in);
		loadRecordKeys(recordKeys, fkey);
		fkey.close();
		rkIt = recordKeys.begin();
		cout << " - List size 2^" << i + 1 << "... ";

		chunksPerCiphertext = (size_t)max(pow(2, n - (double)i - 2), 1.0);

		for (auto& num : ctArray.at(i)) {
			for (int k = 0; k < 3; k++) {
				fct.open(ctPrefix + ctPrefixes.at(k) + num, ios::binary | ios::in);
				ct.load(context, fct);
				fct.close();
				outputCts.push_back(ct);
			}
			decryptAndExtract(outputCts, encoder, decryptor, results, i + 1);
			iter = 0;
			while (iter < chunksPerCiphertext && rkIt != recordKeys.end()) {
				outputMap.insert({ *rkIt, vector({results.at(0).at(iter), results.at(1).at(iter), results.at(2).at(iter)}) });
				iter++;
				++rkIt;
			}
			lastNum = stoi(num);
			outputCts.clear();
		}

		cout << "completed!" << endl;
		
		number.str("");
	}
	cout << "Small lists completed" << endl;


	
	// Now we begin large lists
	vector<vector<string>> largeCtArray;
	largeCtArray = cloConfig["largeCtNumbers"].get<vector<vector<string>>>();

	int currentRk = n - 2;
	number << setw(2) << setfill('0') << currentRk;
	fkey.open(keyPrefix + "rk" + number.str(), ios::in);
	recordKeys.clear();
	
	while (fkey.good()) {
		loadRecordKeysToBack(recordKeys, fkey);
		fkey.close();
		currentRk++;
		number.str("");
		number << setw(2) << setfill('0') << currentRk;
		fkey.open(keyPrefix + "rk" + number.str(), ios::in);
	}
	fkey.close();
	
	rkIt = recordKeys.begin();
	
	for (int i = 0; i < largeCtArray.size(); i++) {
		//int ctsThisRound = 0;
		
		cout << " - List size 2^" << n - 2 + i + 1 << "... ";


		for (auto& num : largeCtArray.at(i)) {
			for (int k = 0; k < 3; k++) {
				fct.open(ctPrefix + ctPrefixes.at(k) + num, ios::binary | ios::in);
				ct.load(context, fct);
				fct.close();
				outputCts.push_back(ct);
			}
		
			decryptAndExtract(outputCts, encoder, decryptor, results, n - 1);
			outputMap.insert({ *(rkIt + stoi(num) - lastNum - 1), vector({results.at(0).at(0), results.at(1).at(0), results.at(2).at(0)}) });
			
			//ctsThisRound++;
			outputCts.clear();
		}
		cout << "completed" << endl;
		//number.str("");
	}
	cout << "Decryption Complete! " << endl;

	fstream fsing(keyPrefix + "singletons", ios::in);
	RecordsMap singletons;
	loadSingletons(singletons, fsing);

	insertRecordsMap(outputMap, singletons);
	ofstream fout("computedResults1.csv", ios::out);
	saveResultsMap(outputMap, fout);
	// And this is it, outputMap should be the end result!!!!!!!!!!!!

	ResultsMap trueRes;
	ifstream ftrue("trueResults.csv", ios::in);
	loadResultsMap(trueRes, ftrue);
	cout.precision(17);
	cout << "We are all done, let's check for correctness: " << compareResultsMapsPercentage(trueRes, outputMap, 1e-5) << endl;
	//cout << "Fix this by altering the compareResultsMapsDigts function to first check for matching codes, then for epsilon, then digits.";


	time_end_total = chrono::high_resolution_clock::now();
	time_diff_total = chrono::duration_cast<chrono::microseconds>(time_end_total - time_start_total);
	cout << "Decryption took " << time_diff_total.count() / 1000 << " milliseconds" << endl;
}

void save_several(string configIn, string configOut)
{
	//Load the configIn file
	fstream fconfigIn(configIn, ios::in);
	nlohmann::json configInfo;
	fconfigIn >> configInfo;
	//

	EncryptionParameters parms(scheme_type::CKKS);

	size_t poly_modulus_degree = configInfo["parms"]["poly_modulus_degree"];
	vector<int> chain = configInfo["parms"]["chain"];
	double scale = configInfo["parms"]["scale"];
	sec_level_type secLevel;
	switch (configInfo["parms"]["security"].get<int>()) {
	case 128:
		secLevel = sec_level_type::tc128;
		break;
	case 192: 
		secLevel = sec_level_type::tc192;
		break;
	case 256:
		secLevel = sec_level_type::tc256;
	default:
		cout << "Error: bad security level" << endl;
		return;
	}

	cout << "Initializing SEAL Parameters: ";
	parms.set_poly_modulus_degree(poly_modulus_degree);
	parms.set_coeff_modulus(CoeffModulus::Create(poly_modulus_degree, chain));

	auto context = SEALContext::Create(parms, true, secLevel);

	KeyGenerator keygen(context);
	auto public_key = keygen.public_key();
	auto secret_key = keygen.secret_key();
	auto relin_keys = keygen.relin_keys();
	auto gal_keys = keygen.galois_keys();
	Encryptor encryptor(context, public_key);
	Evaluator evaluator(context);
	Decryptor decryptor(context, secret_key);

	CKKSEncoder encoder(context);
	size_t slot_count = encoder.slot_count();
	//
	cout << "     Done" << endl;

	cout << "Saving parameters and keys" ;
	nlohmann::json frame;

	string filename;
	
	if(!_mkdir(configInfo["storageLocations"].at(0).get<string>().c_str()))
		cout << "mkdir failed!" << endl;
	
	fstream foutData(filename = configInfo["SecretKeyLocation"], ios::binary | ios::out);
	secret_key.save(foutData);
	foutData.close();
	frame["secretKeyLocation"] = filename;

	foutData.open(filename = configInfo["ParmsLocation"], ios::binary | ios::out);
	parms.save(foutData);
	foutData.close();
	frame["parmsLocation"] = filename;
	cout << "     Done" << endl;

	size_t megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	cout << "[" << megabytes << " MB] "
		<< "Total allocation from the memory pool" << endl;

	// Set up RecordsMap
	RecordsMap database, databaseLarge, outMap;
	fstream fin;
	fin.open("../HEStatisticsSource/synthetic_output_combined_inputNoZeros.csv", ios::in);

	string columnVals;
	srand((unsigned int)time(NULL));
	int startAt = rand() % 1000;
	for (int i = 0; i < /*startAt +*/ 1; i++)
		getline(fin, columnVals);

	vector<double> scanner;
	Ciphertext ct;
	stringstream number;
	
	vector<string> filenames;

	int numberOfLines = configInfo["numberOfLines"];

	fstream fout;
	if (!_mkdir(configInfo["storageLocations"].at(1).get<string>().c_str()))
		cout << "mkdir failed!" << endl;
	string ctsLocation(configInfo["storageLocations"].at(1).get<string>());
	for (int i = 0; i < 5; i++) {
		loadNValues(&fin, scanner, numberOfLines);
		encrypt(scanner, scale, encoder, encryptor, ct);
		headOfVector(scanner);

		number << setw(4) << setfill('0') << i;
		fout.open(filename = (ctsLocation + "/ct" + number.str()), ios::binary | ios::out);
		number.str("");
		filenames.push_back(filename);

		ct.save(fout);
		fout.close();
	}

	//frame["parms"] = configInfo["parms"];
	frame["ctsLocations"] = filenames;
	frame[".Description"] = "Config file for the load part of the save and load_several experiments in finalProducts.cpp";

	cout << frame.dump(4) << endl;

	fstream foutConfig;
	foutConfig.open(configOut, ios::out);
	foutConfig << frame.dump(4);
	foutConfig.close();
	//fout << "hello!" << endl;
	fout.close();
}

void load_several(string config)
{
	//Load in the config file
	fstream fin(config, ios::in);
	nlohmann::json configFile;
	fin >> configFile;
	fin.close();
	//

	cout << endl << "------We are now in the load function" << endl;

	//cout << configFile["parms"].get<string>() << endl;
	EncryptionParameters parms;
	fstream loader(configFile["parmsLocation"].get<string>(), ios::binary | ios::in);
	try {
		parms.load(loader);
	}
	catch (exception & e) {
		cout << e.what() << endl;
	}
	auto context = SEALContext::Create(parms, true, sec_level_type::tc128);
	cout << "Context loaded" << endl;

	
	CKKSEncoder encoder(context);


	//cout << configFile["secretKeyLocation"].get<string>() << endl;
	fstream secret(configFile["secretKeyLocation"].get<string>(), ios::binary | ios::in);
	SecretKey secret_key;
	try {
		secret_key.load(context, secret);
	}
	catch (exception & e) {
		cout << e.what() << endl;
		return;
	}
	cout << "Secret key loaded" << endl;
	Decryptor decryptor(context, secret_key);

	
	nlohmann::json fileArray;
	fileArray = configFile["ctsLocations"];
	vector<double> scanner;
	Ciphertext ct;
	fstream ctIn;
	cout << "About to enter decrypt loop" << endl;
	for (auto& value : fileArray) {
		ctIn.open(value.get<string>(), ios::binary | ios::in);
		ct.load(context, ctIn);
		ctIn.close();

		decrypt(ct, encoder, decryptor, scanner);
		headOfVector(scanner);
	}
	cout << "Decrypting finished!" << endl;
}

void save_experimentation()
{
	// Set up SEAL Context
	EncryptionParameters parms(scheme_type::CKKS);

	size_t poly_modulus_degree = 8192;
	vector<int> chain = { 60, 40, 40, 60 };
	double scale = pow(2.0, 40);

	parms.set_poly_modulus_degree(poly_modulus_degree);
	parms.set_coeff_modulus(CoeffModulus::Create(poly_modulus_degree, chain));

	auto context = SEALContext::Create(parms, true, sec_level_type::tc128);

	KeyGenerator keygen(context);
	auto public_key = keygen.public_key();
	auto secret_key = keygen.secret_key();
	auto relin_keys = keygen.relin_keys();
	auto gal_keys = keygen.galois_keys();
	Encryptor encryptor(context, public_key);
	Evaluator evaluator(context);
	Decryptor decryptor(context, secret_key);

	CKKSEncoder encoder(context);
	size_t slot_count = encoder.slot_count();
	//

	size_t megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	cout << "[" << megabytes << " MB] "
		<< "Total allocation from the memory pool" << endl;

	// Set up RecordsMap
	RecordsMap database, databaseLarge, outMap;
	fstream fin;
	fin.open("../HEStatisticsSource/synthetic_output_combined_inputNoZeros.csv", ios::in);

	string columnVals;
	srand((unsigned int)time(NULL));
	int startAt = rand() % 1000;
	for (int i = 0; i < /*startAt +*/ 1; i++)
		getline(fin, columnVals);

	vector<double> scanner;


	int numberOfLines = 201;
	loadNValues(&fin, scanner, numberOfLines);
	//

	ofstream fout;
	fout.open("saveACiphertext.txt", ios::binary | ios::out);
	if (!fout)
		cout << "Output file did not open!" << endl;

	ofstream foutParms;
	foutParms.open("saveParameters.txt", ios::binary | ios::out);
	if (!foutParms)
		cout << "Parameters output file did not open!" << endl;

	ofstream foutKey;
	foutKey.open("saveSecretKey.txt", ios::binary | ios::out);
	if (!foutKey)
		cout << "Key output file did not open!" << endl;

	cout << "File opened, now encrypting...";

	try {
		parms.save(foutParms);
	}
	catch (exception & e) {
		cout << e.what() << endl;
		return;
	}
	cout << "Parms save successful!" << endl;
	//foutParms.close();

	Ciphertext ct;
	encrypt(scanner, scale, encoder, encryptor, ct);
	peekAtCiphertext(ct, encoder, decryptor);
	cout << " Done" << endl;

	cout << "Saving... ";
	try {
		ct.save(fout);
		secret_key.save(foutKey);
	}
	catch (exception & e) {
		cout << e.what() << endl;
		return;
	}
	cout << "Done" << endl;

	cout << "Closing... ";
	//fout.flush();
	fout.close();
	cout << "Done" << endl;

	cout << "The size of the saved ciphertext is " << ct.save_size(Serialization::compr_mode_default) << endl;

	//cout << "Opening the saved ciphertext...";
	//ifstream fin1;
	//fin1.open("saveACiphertext.txt", ios::binary | ios::in);
	//if (!fin1)
	//	cout << "Opening the input file failed!" << endl;
	//cout << " Done" << endl;

	//Ciphertext inct;

	////inct.load(context, fin1);
	////peekAtCiphertext(inct, encoder, decryptor);

	//stringstream stream;
	//string str;
	//while (getline(fin1, str, '\n')) {
	//	cout << "The size of string " << str.size() << endl;
	//}
	//megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	//cout << "[" << megabytes << " MB] "
	//	<< "Total allocation from the memory pool" << endl;
}

void load_experimentation()
{
	fstream fin1, finCt, finKey;
	fin1.open("saveParameters.txt", ios::binary | ios::in);
	finKey.open("saveSecretKey.txt", ios::binary | ios::in);
	finCt.open("saveACiphertext.txt", ios::binary | ios::in);

	EncryptionParameters parms;
	parms.load(fin1);

	auto context = SEALContext::Create(parms, false, sec_level_type::tc128);

	//KeyGenerator keygen(context);
	SecretKey secret_key;
	secret_key.load(context, finKey);
	Decryptor decryptor(context, secret_key);
	CKKSEncoder encoder(context);

	Ciphertext ct;
	ct.load(context, finCt);
	peekAtCiphertext(ct, encoder, decryptor);
}

void entire_dataset_stats()
{
	chrono::high_resolution_clock::time_point time_start, time_end;
	chrono::microseconds time_diff;

	time_start = chrono::high_resolution_clock::now();
	time_end = chrono::high_resolution_clock::now();
	time_diff = chrono::duration_cast<chrono::microseconds>(time_end - time_start);
	// Prepare to load the database
	cout << "Loading and sorting the database by NAPCS, province, and store codes" << endl;
	RecordsMap databaseLarge;
	vector<RecordsMap> databases;
	fstream fin;
	//fin.open("../HEStatisticsSource/synthetic_output_combined_inputNoZeros.csv", ios::in);
	//fin.open("largeDatasetTest.csv", ios::in);
	fin.open("synthetic_output_combined_input_13m.csv", ios::in);

	string columnVals;
	getline(fin, columnVals);
	int numberOfLines = 13000000;
	loadNCSVLines(&fin, databaseLarge, numberOfLines);

	// Separate the database for smart-packing
	cout << "We loaded " << numberOfLines << " lines from the database" << endl;
	cout << "The database has " << databaseLarge.size() << " groups in it" << endl << endl;
	RecordsMap loading;
	RecordsMap singletons;
	getListsOfSize(databaseLarge, singletons, 0, 2);
	int listsGathered = singletons.size();
	int listSize = 1;
	int maximumListSize = 20; //This is here to prevent an infinite loop
	while (listsGathered < databaseLarge.size() && listSize < maximumListSize) {
		getListsOfSize(databaseLarge, loading, pow(2, listSize - 1) + 1, pow(2, listSize) + 1);
		listsGathered += loading.size();
		databases.push_back(loading);
		listSize++;
	}
	// Have: the entire database in a vector of recordsmaps called databases, the index i gives the max size of the lists, which is 2^(i + 1)

	// Check the database separating for correctness
	//size_t totalLists = singletons.size();

	//cout << "Singletons has " << totalLists << " entries" << endl;

	//for (int i = 0; i < databases.size(); i++) {
		//totalLists += databases.at(i).size();
		//cout << "Database " << i << " has " << databases.at(i).size() << " groups in it" << endl;
	//}
	//if (totalLists != databaseLarge.size())
		//cout << "===========ERROR: Missing some lists!===============" << endl;
	//cout << "Recovered " << totalLists << " of the original " << databaseLarge.size() << endl;
	printRecordsMap(databases.at(1), 1);
	//

	cout << "Size of sublists of databases ";
	for (const auto& list : databases) {
		cout << list.size() << ", ";
	}
	cout << endl;

	// Set up a SEAL context 
	cout << "Preparing a SEAL context: " ;
	EncryptionParameters parms(scheme_type::CKKS);

	size_t n = 14;
	//size_t n = 13;
	size_t poly_modulus_degree = pow(2, n);
	vector<int> chain = { 60, 40, 40, 40, 60 };
	//vector<int> chain = { 55, 35, 35, 35, 55 };
	double scale = pow(2.0, 40);
	//double scale = pow(2.0, 35);

	parms.set_poly_modulus_degree(poly_modulus_degree);
	parms.set_coeff_modulus(CoeffModulus::Create(poly_modulus_degree, chain));

	//sec_level_type secLevel = sec_level_type::tc192;
	sec_level_type secLevel = sec_level_type::tc128;
	//int secLevelInt = 192; // NEED TO UPDATE THIS MANUALLY
	int secLevelInt = 128;
	auto context = SEALContext::Create(parms, true, secLevel);

	KeyGenerator keygen(context);
	auto public_key = keygen.public_key();
	auto secret_key = keygen.secret_key();
	auto relin_keys = keygen.relin_keys();
	auto gal_keys = keygen.galois_keys();
	Encryptor encryptor(context, public_key);
	Evaluator evaluator(context);
	Decryptor decryptor(context, secret_key);

	CKKSEncoder encoder(context);
	size_t slot_count = encoder.slot_count();
	cout << "Done " << endl;
	// Have: all the objects needed to run SEAL

	// Print out information about what we are about to do
	cout << endl << "------Computing statistics homomorphically on a big dataset------" << endl;

	cout << " --- Parameters; " << endl;
	cout << "   - poly mod : " << poly_modulus_degree << " which is 2^" << n << endl;
	cout << "   - chain    : ";
	for (const auto& val : chain)
		cout << val << ", ";
	cout << endl;
	cout << "   - security level: " << secLevelInt << endl;
	cout << endl;

	size_t megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	cout << "[" << megabytes << " MB] "
		<< "Total allocation from the memory pool" << endl;
	cout << endl;

	// This will store the recordkeys of the entire database.
	// index i will have the recordkeys for the lists with 2^(i + 1)
	cout << "Grabbing recordkeys... ";
	vector<vector<recordkey>> allRecordkeys;
	vector<recordkey> recordkeyLoader;

	for (auto& database : databases) {
		getRecordKeys(database, recordkeyLoader);
		allRecordkeys.push_back(recordkeyLoader);
	}
	cout << " Done." << endl;
	// Have: all recordkey lists in one vector. This will be sent directly to the consumer

	cout << "Size of sublists of allRecordkeys ";
	for (const auto& list : allRecordkeys) {
		cout << list.size() << ", ";
	}
	cout << endl;

	// We now encrypt. We have to do this in two different ways; first, we need to do smart packing on the smaller lists, then we need
	// to do regular encrypting on the bigger lists. I'm not sure what the best way to do this is, but for the smaller ones we use the 
	// smart paradigm and for the big ones we use the regular process we generated a while ago
	cout << "--- Beginning encryption... " << endl;
	vector<vector<double>> scanners, toEncrypt;
	vector<double> encryptLoader;
	vector<size_t> listLengths;

	vector<vector<Ciphertext>> smallCts;
	vector<Ciphertext> smallCtsLoader;
	vector<vector<vector<Plaintext>>> smallPtsLists;
	vector<vector<Plaintext>> smallPtsListsLoader;
	vector<vector<Plaintext>> specialPts;
	vector<Plaintext> specialPtsLoader;

	int numberOfCts = 0;
	int numberOfPts = 0;
	int numberOfPtsThisRound = 0;

	for (int i = 0; i < min(n - 2, databases.size()); i++) {
		cout << "For size 2^" << i + 1 << ", ";
		getVectors(databases.at(i), scanners);
		for (const auto& list : scanners) {
			for (const auto& val : list)
				encryptLoader.push_back(val);
			listLengths.push_back(list.size());
			for (int j = list.size(); j < pow(2, i + 1); j++) {
				encryptLoader.push_back(0);
			}
		}
		cout << "collected " << scanners.size() << " groups, ";
		if (scanners.size() != 0) {
			encryptVector(encryptLoader, scale, encoder, encryptor, smallCtsLoader);
			cout << "(Enc, ";
			preparePlaintexts(listLengths, scale, encoder, smallPtsListsLoader);
			cout << "Pt, ";
			specialPlaintexts(i + 1, scale, encoder, specialPtsLoader);
			cout << "sPt) ";
		}
		smallCts.push_back(smallCtsLoader);
		smallPtsLists.push_back(smallPtsListsLoader);
		specialPts.push_back(specialPtsLoader);
		numberOfPtsThisRound = 0;
		for (auto& list : smallPtsListsLoader)
			numberOfPtsThisRound += list.size();
		cout << "and encrypted them into " << smallCtsLoader.size() << " ciphertexts and " << numberOfPtsThisRound << " plaintexts" << endl;
		numberOfCts += smallCtsLoader.size();
		numberOfPts += numberOfPtsThisRound;

		// These three shouldn't be necessary
		smallCtsLoader.clear();
		smallPtsListsLoader.clear();
		specialPtsLoader.clear();
		//
		encryptLoader.clear();
		listLengths.clear();
	}
	cout << "-- Small lists done... " << endl;
	// Have: all small lists encrypted, and plaintexts prepared. Now we encrypt the large lists
	vector<vector<vector<Ciphertext>>> largeCts;
	vector<vector<Ciphertext>> largeCtsLoader;
	vector<Ciphertext> oneGroupCt;
	vector<vector<vector<Plaintext>>> largePtsLists;
	vector<vector<Plaintext>> largePtsListsLoader;
	vector<Plaintext> oneGroupPt;

	int numberOfLargeCtsThisRound = 0;
	int numberOfLargeCts = 0;
	for (int i = n - 2; i < databases.size(); i++) {
		numberOfLargeCtsThisRound = 0;
		getVectors(databases.at(i), scanners);
		cout << "For size " << i << ", ";
		for (auto& list : scanners) {
			encryptVector(list, scale, encoder, encryptor, oneGroupCt);
			largeCtsLoader.push_back(oneGroupCt);
			numberOfLargeCtsThisRound += oneGroupCt.size();
			preparePlaintexts(list.size(), scale, encoder, oneGroupPt);
			largePtsListsLoader.push_back(oneGroupPt);
		}
		largeCts.push_back(largeCtsLoader);
		largeCtsLoader.clear();
		largePtsLists.push_back(largePtsListsLoader);
		largePtsListsLoader.clear();

		cout << "just encrypted " << scanners.size() << " groups, in total using " << numberOfLargeCtsThisRound << " ciphertexts" << endl;
		numberOfLargeCts += numberOfLargeCtsThisRound;
	}
	cout << "-- Large lists done." << endl;

	cout << "Total cts used: " << numberOfLargeCts + numberOfCts << endl;
	cout << "Pts used: " << numberOfPts << endl;

	cout << "Size of sublists of smallCts ";
	for (const auto& list : smallCts) {
		cout << list.size() << ", ";
	}
	cout << endl;

	cout << "Size of sublists of largeCts ";
	for (const auto& list : largeCts) {
		cout << list.size() << ", ";
	}
	cout << endl;

	megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	cout << "[" << megabytes << " MB] "
		<< "Total allocation from the memory pool" << endl;
	cout << endl;
	// ALL ENCRYPTION FINISHED: Have lists of ciphertexts smallCts and largeCts, as well as plaintexts smallPtsLists, largePtsLists, and specialPts
	// These (and only these) are sent to the cloud.

	//// CLOUD: only has access to smallCts, largeCts, smallPtsLists, largePtsLists, and specialPts
	// Begin homomorphic statistics calculations
	cout << "--- Beginning homomorphic calculations... " << endl;
	vector<vector<vector<Ciphertext>>> outputCts;
	vector<vector<Ciphertext>> eachListSizeLoader;
	vector<Ciphertext> resultsForASingleCiphertext;
	
	int outputCtsUsed = 0;
	cout << "-- Small ciphertexts..." << endl;
	for (size_t i = 0; i < smallCts.size(); i++) {
		for (size_t j = 0; j < smallCts.at(i).size(); j++) {
			homomorphicStats(smallCts.at(i).at(j), smallPtsLists.at(i).at(j), specialPts.at(i), gal_keys, evaluator, relin_keys, resultsForASingleCiphertext, i + 1);
			eachListSizeLoader.push_back(resultsForASingleCiphertext);
			outputCtsUsed += resultsForASingleCiphertext.size();
		}
		outputCts.push_back(eachListSizeLoader);
		eachListSizeLoader.clear();
		cout << "List size " << i << " completed" << endl;
	}
	cout << "Small cts done" << endl;

	cout << "Large cts..." << endl;
	for (size_t i = 0; i < largeCts.size(); i++) {
		for (size_t j = 0; j < largeCts.at(i).size(); j++) {
			homomorphicStats(largeCts.at(i).at(j), largePtsLists.at(i).at(j), gal_keys, evaluator, relin_keys, resultsForASingleCiphertext);
			eachListSizeLoader.push_back(resultsForASingleCiphertext);
			outputCtsUsed += resultsForASingleCiphertext.size();
		}
		outputCts.push_back(eachListSizeLoader);
		cout << "List size " << i << " completed" << endl;
		eachListSizeLoader.clear();
	}

	cout << "Size of sublists of outputCts ";
	for (const auto& list : outputCts) {
		cout << list.size() << ", ";
	}
	cout << endl;
	
	cout << "Done. Used " << outputCtsUsed << " output ciphertexts" << endl << endl;
	// ALL CALCULATION FINISHED: Have all output ciphertexts stored in outputCts, where each list has a size, then its 3 cts for each group.


	//// CONSUMER: has access to outputCts, allRecordKeys, and decryptor
	cout << "--- Begin decrypting..." << endl;
	ResultsMap outputMap; 

	vector<vector<double>> results;
	size_t chunksPerCiphertext;
	size_t iter;
	
	vector<recordkey>::iterator rkIt;
	for (int i = 0; i < outputCts.size(); i++) {
		rkIt = allRecordkeys.at(i).begin();
		chunksPerCiphertext = (size_t)max(pow(2, n - (double)i - 2), 1.0);
		for (int j = 0; j < outputCts.at(i).size(); j++) {
			decryptAndExtract(outputCts.at(i).at(j), encoder, decryptor, results, i + 1);
			iter = 0;
			while (iter < chunksPerCiphertext && rkIt != allRecordkeys.at(i).end()) {
				outputMap.insert({ *rkIt, vector({results.at(0).at(iter), results.at(1).at(iter), results.at(2).at(iter)}) });
				iter++;
				++rkIt;
			}
		}
		cout << "List size " << i << " completed" << endl;
	}

	cout << " Done. " << endl;
	//TODO: the stuff for the large lists. Or maybe not?

	insertRecordsMap(outputMap, singletons);
	ResultsMap trueResMap;
	computeResultsMap(databaseLarge, trueResMap);

	cout << "Now compare the two: ";
	cout << compareResultsMaps(outputMap, trueResMap, 0.1) << endl;

	time_end = chrono::high_resolution_clock::now();
	time_diff = chrono::duration_cast<chrono::microseconds>(time_end - time_start);
	cout << "This WHOLE thing, took " << time_diff.count() << " microseconds" << endl;

	megabytes = MemoryManager::GetPool().alloc_byte_count() >> 20;
	cout << "[" << megabytes << " MB] "
		<< "Total allocation from the memory pool" << endl;
	cout << endl;

	ofstream fout1, fout2;
	fout1.open("trueResults.csv", ios::out);
	fout2.open("computedResults.csv", ios::out);

	saveResultsMap(trueResMap, fout1);
	saveResultsMap(outputMap, fout2);
}

void generate_configfile(string str)
{
	ofstream fout;
	fout.open(str, ios::out);
	//fout << "Hi there!" << endl;

	nlohmann::json j2 = {
		{".Description", "This is a config file used in the save_several function in finalProducts.cpp."},
		{"Created", "25/02/20"},
		{"Last Modified", "25/02/20"},
		{"CreatedBy", "Zachary Zanussi"},
		{"Data", "../HEStatisticsSource/synthetic_output_combined_input.csv"},
		{"numberOfLines", 201},
		{"parms", {
			{"poly_modulus_degree", pow(2,14)},
			{"poly_modulus_exponent", 14},
			{"chain", {60,40,40,40,60}},
			{"scale", pow(2,40)},
			{"security", 192}
			}},
		{"storageLocations", {"data", "cts"}},
		{"SecretKeyLocation", "data/secretKey"},
		{"ParmsLocation", "data/parms"}
	};

	fout << std::setw(4) << j2 << endl;
}

void generate_configfile_consumer(string str)
{
	ofstream fout;
	fout.open(str, ios::out);
	//fout << "Hi there!" << endl;

	nlohmann::json j2 = {
		{".Description", "This is the consumers config file in the full program of HE. This is generated early, and the others are generated as we go."},
		{"Created", "25/02/20"},
		{"Last Modified", "25/02/20"},
		{"CreatedBy", "Zachary Zanussi"},
		//{"Data", "../HEStatisticsSource/synthetic_output_combined_input.csv"},
		//{"numberOfLines", 201},
		{"parms", {
			{"poly_modulus_degree", pow(2,14)},
			{"poly_modulus_exponent", 14},
			{"chain", {60,40,40,40,60}},
			{"scale", pow(2,40)},
			{"security", 192}
			}},
		{"storageLocations", {"toClient", "toCloud", "toConsumer"}},
		{"SecretKeyLocation", "toConsumer/secretKey"},
		{"PublicKeyLocation", "toClient/publicKey"},
		{"RelinKeyLocation", "toCloud/relinKey"},
		{"GaloisKeyLocation", "toCloud/galoisKey"},
		{"RecordKeyLocation", "toConsumer/recordKeys/"},
		{"ParmsLocation", "toClient/parms"},
		{"CloudParmsLocation", "toCloud/parms"},
		{"OutputCtLocation", "toConsumer/cts/"}
	};

	fout << std::setw(4) << j2 << endl;
}

void generate_configfile_client(string str)
{
	ofstream fout;
	fout.open(str, ios::out);
	//fout << "Hi there!" << endl;

	nlohmann::json j2 = {
		{".Description", "This is the clients config file in the full program of HE. This is information only available and applicable to the client."},
		{"Created", "25/02/20"},
		{"CreatedBy", "Zachary Zanussi"},
		{"Data", "../HEStatisticsSource/synthetic_output_combined_input.csv"},
		{"numberOfLines", 201}, 
		{"ctLocation", "toCloud/cts/"},
		{"ptLocation", "toCloud/pts/"}
		/*{"parms", {
			{"poly_modulus_degree", pow(2,14)},
			{"poly_modulus_exponent", 14},
			{"chain", {60,40,40,40,60}},
			{"scale", pow(2,40)},
			{"security", 192}
			}},*/
	};

	fout << std::setw(4) << j2 << endl;
}

void generate_configfile_cloud(string str)
{
	ofstream fout;
	fout.open(str, ios::out);
	//fout << "Hi there!" << endl;

	nlohmann::json j2 = {
		{".Description", "This is the clouds config file in the full program of HE. This is information only available and applicable to the cloud."},
		{"Created", "25/02/20"},
		{"Last Modified", "25/02/20"},
		{"CreatedBy", "Zachary Zanussi"}
	};

	fout << std::setw(4) << j2 << endl;
}