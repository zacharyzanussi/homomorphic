#include "recordkey.h"

double total(vector<double>&);
double mean(vector<double>&);
double variance(vector<double>&);

using namespace std;

bool listWithinEpsilon(vector<double>& vec1, vector<double>& vec2, double epsilon);

bool listOfPrecision(vector<double>, vector<double>, int);

bool listWithinPercentageError(vector<double>&, vector<double>&, double);

bool operator<(recordkey const& lhs, recordkey const& rhs)
{
	return ((lhs.napcs < rhs.napcs) || 
		(lhs.napcs == rhs.napcs && lhs.prov < rhs.prov) ||
		(lhs.napcs == rhs.napcs && lhs.store < rhs.store && lhs.prov == rhs.prov));
}

ostream& operator<<(ostream& os, recordkey const& rk)
{
	os << "[NAPCS: " << rk.napcs << " & PROV: " << rk.prov << " & STORE: " << rk.store << "]";
	return os;
}

bool operator==(recordkey const& lhs, recordkey const& rhs)
{
	return (lhs.napcs == rhs.napcs && lhs.prov == rhs.prov && lhs.store == rhs.store);
}


void printRecordsMap(RecordsMap const& rm, int const maxCount)
{
	bool end_early = maxCount > -1;
	int i = 0;
	for (const auto& sm_pair : rm)
	{
		//Only print as many was requested
		if (end_early && i>maxCount)
		{
			return ;
		}

		//Print the dollar values
		cout << sm_pair.first << " = [";
		for (const auto& value : sm_pair.second) 
		{
			cout << value << ", ";
		}
		cout << "]" << endl;

		i++;
	}

	return ;
}

void printResultsMap(ResultsMap const& rm, int const maxCount)
{
	bool end_early = maxCount > -1;
	int i = 0;
	for (const auto& sm_pair : rm)
	{
		//Only print as many was requested
		if (end_early && i > maxCount)
		{
			return;
		}

		//Print the dollar values
		cout << sm_pair.first << " = " ;
		
		cout << "Total = " << sm_pair.second.at(0) << ", Mean = " << sm_pair.second.at(1) << ", Variance = " << sm_pair.second.at(2) << endl;
		


		i++;
	}

	return;
}

//void computeAverageMap(RecordsMap& in, ResultsMap& out)
//{
//	out.clear();
//	
//	for (RecordsMap::iterator it = in.begin(); it != in.end(); it++) {
//		out.insert({ it->first, mean(it->second) });
//	}
//}

void computeResultsMap(RecordsMap& in, ResultsMap& out)
{
	out.clear();
	vector<double> triple;
	for (RecordsMap::iterator it = in.begin(); it != in.end(); it++) {
		triple.push_back(total(it->second));
		triple.push_back(mean(it->second));
		triple.push_back(variance(it->second));

		out.insert(pair<recordkey, vector<double>>(it->first, triple));
		triple.clear();
	}
}

bool compareResultsMaps(ResultsMap& lhs, ResultsMap& rhs, double error)
{
	if (lhs.size() != rhs.size()) {
		cout << "Sizes aren't the same : lhs is " << lhs.size() << " rhs is " << rhs.size() << endl;
		return false;
	}

	bool same = true;
	ResultsMap::iterator lit, rit;
	for (lit = lhs.begin(), rit = rhs.begin(); lit != lhs.end(); ++lit, ++rit) {
		if (!(lit->first == rit->first && listWithinEpsilon(lit->second, rit->second, error))) {
			cout << "Failure at element " << lit->first << endl;
			cout << "Sizes are " << lit->second.size() << "  and " << rit->second.size() << endl;
			for (int i = 0; i < max(lit->second.size(), rit->second.size()); i++)
				cout << lit->second[i] << " " << rit->second[i] << endl;
			same = false;
		}
	}
	return same;
}

bool compareResultsMapsDigits(ResultsMap& lhs, ResultsMap& rhs, int minDigits)
{
	if (lhs.size() != rhs.size()) {
		cout << "Sizes aren't the same : lhs is " << lhs.size() << " rhs is " << rhs.size() << endl;
		return false;
	}

	bool same = true;
	ResultsMap::iterator lit, rit;
	for (lit = lhs.begin(), rit = rhs.begin(); lit != lhs.end(); ++lit, ++rit) {
		if (!(lit->first == rit->first && listOfPrecision(lit->second, rit->second, minDigits))) {
			cout << "Failure at element " << lit->first << endl;
			cout << "Sizes are " << lit->second.size() << "  and " << rit->second.size() << endl;
			for (int i = 0; i < max(lit->second.size(), rit->second.size()); i++)
				cout << lit->second[i] << " " << rit->second[i] << endl;
			same = false;
		}
	}
	return same;
}

bool compareResultsMapsPercentage(ResultsMap& lhs, ResultsMap& rhs, double error)
{
	if (lhs.size() != rhs.size()) {
		cout << "Sizes aren't the same : lhs is " << lhs.size() << " rhs is " << rhs.size() << endl;
		return false;
	}

	bool same = true;
	ResultsMap::iterator lit, rit;
	for (lit = lhs.begin(), rit = rhs.begin(); lit != lhs.end(); ++lit, ++rit) {
		if (!(lit->first == rit->first && listWithinPercentageError(lit->second, rit->second, error))) {
			cout << "Failure at element " << lit->first << endl;
			cout << "Sizes are " << lit->second.size() << "  and " << rit->second.size() << endl;
			for (int i = 0; i < max(lit->second.size(), rit->second.size()); i++)
				cout << lit->second[i] << " " << rit->second[i] << endl;
			same = false;
			cout << endl;
		}
	}
	return same;
}

void insertRecordsMap(ResultsMap& resMap, RecordsMap& recMap)
{
	for (const auto& pair : recMap) {
		resMap.insert({ pair.first, vector({pair.second.front(), pair.second.front(), 0.0}) });
	}
}

void getListsOfSize(RecordsMap& in, RecordsMap& out, int greater, int less)
{
	out.clear();
	for (RecordsMap::iterator it = in.begin(); it != in.end(); ++it) {
		if (it->second.size() >= greater && it->second.size() < less)
			out.insert(*it);
	}
}

bool compareRecordsMaps(RecordsMap& lhs, RecordsMap& rhs, double error)
{
	if (lhs.size() != rhs.size())
		return false;

	RecordsMap::iterator lit, rit;
	for (lit = lhs.begin(), rit = rhs.begin(); lit != lhs.end(); ++lit, ++rit) {
		if (!(lit->first == rit->first && listWithinEpsilon(lit->second, rit->second, error))) {
			cout << "Failure at element " << lit->first << endl;
			cout << "Sizes are " << lit->second.size() << "  and " << rit->second.size() << endl;
			for (int i = 0; i < max(lit->second.size(), rit->second.size()); i++)
				cout << lit->second[i] << " " << rit->second[i] << endl;
			return false;
		}
	}
	return true;
}

void saveResultsMap(ResultsMap& inmap, ofstream& fout) 
{
	for (auto& pair : inmap) {
		//fout << pair.first.returnCommaDelimited() << "," << to_string(pair.second[0]) << "," << to_string(pair.second[1]) << "," << to_string(pair.second[2]) << endl;
		fout << pair.first.returnCommaDelimited() << "," << pair.second[0] << "," << pair.second[1] << "," << pair.second[2] << endl;
	}
}

void loadResultsMap(ResultsMap& map, ifstream& fin)
{
	map.clear();
	string str, word;
	vector<string> vec;

	while (getline(fin, str))
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		for (int i = 0; i < 3; i++) {
			map[recordkey(vec[0], vec[1], vec[2])].push_back(stof(vec[3 + i]));
		}
	}
}

void saveRecordKeys(vector<recordkey>& recordKeys, fstream& fout)
{
	for (auto& key : recordKeys) {
		fout << key.returnCommaDelimited() << endl;
	}
}

void loadRecordKeys(vector<recordkey>& recordKeys, fstream& fin)
{
	recordKeys.clear();
	string str, word;
	vector<string> vec;

	while (getline(fin, str))
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		recordKeys.push_back(recordkey(vec[0], vec[1], vec[2]));
	}
}

void loadRecordKeysToBack(vector<recordkey>& recordKeys, fstream& fin)
{
	string str, word;
	vector<string> vec;

	while (getline(fin, str))
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		recordKeys.push_back(recordkey(vec[0], vec[1], vec[2]));
	}
}

void saveSingletons(RecordsMap& singletons, fstream& fout)
{
	for (auto& pair : singletons) {
		fout << pair.first.returnCommaDelimited() << "," << pair.second.front() << endl;
	}
}

void loadSingletons(RecordsMap& singletons, fstream& fin)
{
	singletons.clear();
	string str, word;
	vector<string> vec;

	while (getline(fin, str))
	{
		stringstream s(str);

		vec.clear();
		while (getline(s, word, ',')) {
			vec.push_back(word);
		}

		singletons[recordkey(vec[0], vec[1], vec[2])].push_back(stof(vec[3]));
	}
}

void getRecordKeys(RecordsMap& input, vector<recordkey>& output)
{
	output.clear();
	output.reserve(input.size());
	for (const auto& pair : input) {
		output.push_back(pair.first);
	}
}

void getVectors(RecordsMap& input, vector<vector<double>>& output)
{
	output.clear();
	output.reserve(input.size());
	for (const auto& pair : input) {
		output.push_back(pair.second);
	}
}