#ifndef RECORDKEY_H
#define RECORDKEY_H

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <map>

using namespace std;



class recordkey
{
private:
	string napcs;
	string prov;
	string store;

public:
	recordkey(string n, string p, string s) : napcs(n), prov(p), store(s){}

	friend bool operator<(recordkey const& lhs, recordkey const& rhs);
	friend ostream& operator<<(ostream& os, recordkey const& rk);
	friend bool operator==(recordkey const& lhs, recordkey const& rhs);
	string returnCommaDelimited() const { string str; str = this->napcs + "," + this->prov + "," + this->store; return str; }
};

typedef map<recordkey, vector<double>> RecordsMap;

typedef map<recordkey, vector<double>> ResultsMap;

//void computeAverageMap(RecordsMap& in, ResultsMap& out);
void computeResultsMap(RecordsMap& in, ResultsMap& out);

bool compareRecordsMaps(RecordsMap& lhs, RecordsMap& rhs, double error = 1e-5);

bool compareResultsMaps(ResultsMap& lhs, ResultsMap& rhs, double error = 1e-5);

bool compareResultsMapsDigits(ResultsMap& lhs, ResultsMap& rhs, int minDigits = 5);

bool compareResultsMapsPercentage(ResultsMap& lhs, ResultsMap& rhs, double error = 1e-5);

// This function is for inserting RecordsMap entries with only one value in each group into a results map. It does not check if recMap is actually of this form.
// The total and mean are both the given value, while the variance is zero.
void insertRecordsMap(ResultsMap& resMap, RecordsMap& recMap);

double mean(vector<double>& vec);

void getListsOfSize(RecordsMap& in, RecordsMap& out, int greater, int less);

void printRecordsMap(RecordsMap const& rm, int const maxCount = -1);

void printResultsMap(ResultsMap const& rm, int const maxCount = -1);

void saveResultsMap(ResultsMap& inmap, ofstream& fout);

void loadResultsMap(ResultsMap& map, ifstream& fin);

void saveRecordKeys(vector<recordkey>& recordKeys, fstream& fout);

void loadRecordKeys(vector<recordkey>& recordKeys, fstream& fout);

void loadRecordKeysToBack(vector<recordkey>& recordKeys, fstream& fin);

void saveSingletons(RecordsMap& singletons, fstream& fout);

void loadSingletons(RecordsMap& singletons, fstream& fin);

void getRecordKeys(RecordsMap& input, vector<recordkey>& output);

void getVectors(RecordsMap& input, vector<vector<double>>& output);


#endif // !RECORDKEY_H