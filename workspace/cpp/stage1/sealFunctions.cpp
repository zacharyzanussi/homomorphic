#include "sealFunctions.h"
#include "csvFunctions.h"

using namespace std;
using namespace seal;



void cyclic_add_inplace(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, int N)
{
	if(N == -1)
		N = (int)log2(toAdd.poly_modulus_degree()) - 1;
	Ciphertext temp;

	for (int i = 0; i < N; i++) {
		evaluator.rotate_vector(toAdd, (int)pow(2, i), galois_keys, temp);
		evaluator.add_inplace(toAdd, temp);
	}
}

void cyclic_add(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, Ciphertext& result, int N)
{
	result = toAdd;
	cyclic_add_inplace(result, galois_keys, evaluator, N);
}

void cyclic_add_inplace_right(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, int N)
{
	if (N == -1)
		N = (int)log2(toAdd.poly_modulus_degree()) - 1;
	Ciphertext temp;

	for (int i = 0; i < N; i++) {
		evaluator.rotate_vector(toAdd, - (int)pow(2, i), galois_keys, temp);
		evaluator.add_inplace(toAdd, temp);
	}
}

void cyclic_add_right(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, Ciphertext& result, int N)
{
	result = toAdd;
	cyclic_add_inplace_right(result, galois_keys, evaluator, N);
}

void add_many(vector<Ciphertext>& ciphertexts, Evaluator& evaluator, Ciphertext& destination)
{
	if (ciphertexts.empty())
	{
		cout << "ciphertexts is empty in add_many: returning" << endl;
		return;
	}

	destination = ciphertexts.at(0);
	for (size_t i = 1; i < ciphertexts.size(); i++)
	{
		evaluator.add_inplace(destination, ciphertexts.at(i));
	}
}

void peekAtCiphertext(Ciphertext& ct, CKKSEncoder& encoder, Decryptor& decryptor, int n)
{
	vector<double> output; 
	decrypt(ct, encoder, decryptor, output);
	headOfVector(output, n);
}

void peekAtCiphertext(vector<Ciphertext>& ct, CKKSEncoder& encoder, Decryptor& decryptor, int n)
{
	vector<double> output;
	decryptVector(ct, encoder, decryptor, output);
	headOfVector(output, n);
}

//inline void print_parameters(std::shared_ptr<seal::SEALContext> context)
//{
//	// Verify parameters
//	if (!context)
//	{
//		throw std::invalid_argument("context is not set");
//	}
//	auto& context_data = *context->key_context_data();
//
//	/*
//	Which scheme are we using?
//	*/
//	std::string scheme_name;
//	switch (context_data.parms().scheme())
//	{
//	case seal::scheme_type::BFV:
//		scheme_name = "BFV";
//		break;
//	case seal::scheme_type::CKKS:
//		scheme_name = "CKKS";
//		break;
//	default:
//		throw std::invalid_argument("unsupported scheme");
//	}
//	std::cout << "/" << std::endl;
//	std::cout << "| Encryption parameters :" << std::endl;
//	std::cout << "|   scheme: " << scheme_name << std::endl;
//	std::cout << "|   poly_modulus_degree: " <<
//		context_data.parms().poly_modulus_degree() << std::endl;
//
//	/*
//	Print the size of the true (product) coefficient modulus.
//	*/
//	std::cout << "|   coeff_modulus size: ";
//	std::cout << context_data.total_coeff_modulus_bit_count() << " (";
//	auto coeff_modulus = context_data.parms().coeff_modulus();
//	std::size_t coeff_mod_count = coeff_modulus.size();
//	for (std::size_t i = 0; i < coeff_mod_count - 1; i++)
//	{
//		std::cout << coeff_modulus[i].bit_count() << " + ";
//	}
//	std::cout << coeff_modulus.back().bit_count();
//	std::cout << ") bits" << std::endl;
//
//	/*
//	For the BFV scheme print the plain_modulus parameter.
//	*/
//	if (context_data.parms().scheme() == seal::scheme_type::BFV)
//	{
//		std::cout << "|   plain_modulus: " << context_data.
//			parms().plain_modulus().value() << std::endl;
//	}
//
//	std::cout << "\\" << std::endl;
//}

void encode(vector<double>& vec, double scale, CKKSEncoder& encoder, Plaintext& destination)
{
	encoder.encode(vec, scale, destination);
}

void encodeVector(vector<double>& vec, double scale, CKKSEncoder& encoder, vector<Plaintext>& plaintexts)
{
	double slots = (double)encoder.slot_count();

	//int numberOfPlaintexts = (int)floor(vec.size() / slots) + 1;
	int numberOfPlaintexts = (int)ceil(vec.size() / slots);
	plaintexts.clear();
	plaintexts.reserve(numberOfPlaintexts);

	vector<double> input;
	Plaintext temp;
	for (int i = 0; i < numberOfPlaintexts - 1; i++) {
		input = vector(vec.begin() + i * slots, vec.begin() + (i + 1) * slots);
		encode(input, scale, encoder, temp);
		plaintexts.push_back(temp);
	}
	input = vector(vec.begin() + (numberOfPlaintexts - 1) * slots, vec.end());
	encode(input, scale, encoder, temp);
	plaintexts.push_back(temp);
}

void decode(Plaintext& plaintext, CKKSEncoder& encoder, vector<double>& destination, double error)
{
	encoder.decode(plaintext, destination);
	while (destination.size() > 0 && abs(destination.back()) < error)
		destination.pop_back();
}

void decodeVector(vector<Plaintext>& plaintexts, CKKSEncoder& encoder, vector<double>& vec, double error)
{
	if (plaintexts.size() == 0)
		return;
	vector<double> clear;
	vec.clear();
	int numberOfPlaintexts = (int)plaintexts.size();
	int slots = (int)encoder.slot_count();
	vec.reserve(slots * numberOfPlaintexts);
	cout << slots * numberOfPlaintexts << endl;
	for (int i = 0; i < numberOfPlaintexts - 1; i++) {
		decode(plaintexts.at(i), encoder, clear, 0);
		vec.insert(vec.end(), clear.begin(), clear.end());
	}
	decode(plaintexts.back(), encoder, clear, error);
	vec.insert(vec.end(), clear.begin(), clear.end());
}

void encrypt(Plaintext& plaintext, Encryptor& encryptor, Ciphertext& ciphertext)
{
	encryptor.encrypt(plaintext, ciphertext);
}

void encrypt(vector<double>& vec, double scale, CKKSEncoder& encoder, Encryptor& encryptor, Ciphertext& ciphertext)
{
	Plaintext plaintext;
	encode(vec, scale, encoder, plaintext);
	encrypt(plaintext, encryptor, ciphertext);
}

void encrypt(vector<vector<double>>& vecs, double scale, CKKSEncoder& encoder, Encryptor& encryptor, Ciphertext& ciphertext)
{
	size_t slots = encoder.slot_count();

	size_t maxListSize = 0;
	for (const auto& aList : vecs) {
		if (aList.size() > maxListSize)
			maxListSize = aList.size();
	}

	size_t expOf2 = 0;
	while (maxListSize > pow(2, expOf2))
		expOf2++;

	if (pow(2, expOf2) * vecs.size() > slots)
		throw logic_error("vecs is malformed; it won't fit in a single ciphertext!");

	vector<double> scanner;
	for (const auto& list : vecs) {
		for (const auto& val : list) {
			scanner.push_back(val);
		}
		for (int i = list.size(); i < pow(2, expOf2); i++) {
			scanner.push_back(0);
		}
	}

	encrypt(scanner, scale, encoder, encryptor, ciphertext);
}

void encryptVector(vector<double>& vec, double scale, CKKSEncoder& encoder, Encryptor& encryptor, vector<Ciphertext>& ciphertexts)
{
	if (vec.size() == 0)
		throw("vec is empty; I don't know what to do");

	double slots = (double)encoder.slot_count();
	
	//int numberOfCiphertexts = (int)floor(vec.size() / slots) + 1;
	int numberOfCiphertexts = (int)ceil(vec.size() / slots);
	ciphertexts.clear();
	ciphertexts.reserve(numberOfCiphertexts);

	vector<double> input;
	Ciphertext temp;
	for (int i = 0; i < numberOfCiphertexts - 1; i++) {
		input = vector(vec.begin() + i * slots, vec.begin() + (i + 1) * slots);
		encrypt(input, scale, encoder, encryptor, temp);
		ciphertexts.push_back(temp);
	}
	input = vector(vec.begin() + (numberOfCiphertexts - 1) * slots, vec.end());
	encrypt(input, scale, encoder, encryptor, temp);
	ciphertexts.push_back(temp);
}

void decrypt(Ciphertext& ciphertext, Decryptor& decryptor, Plaintext& plaintext)
{
	decryptor.decrypt(ciphertext, plaintext);
}

void decrypt(Ciphertext& ciphertext, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& vec, double error)
{
	Plaintext plaintext;
	decrypt(ciphertext, decryptor, plaintext);
	decode(plaintext, encoder, vec, error);
}

void decryptVector(vector<Ciphertext>& ciphertexts, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& vec, double error)
{
	if (ciphertexts.size() == 0)
		return;
	vector<double> clear;
	vec.clear();
	int numberOfCiphertexts = (int)ciphertexts.size();
	int slots = (int)encoder.slot_count();
	vec.reserve(slots * numberOfCiphertexts);

	for (int i = 0; i < numberOfCiphertexts - 1; i++) {
		decrypt(ciphertexts.at(i), encoder, decryptor, clear, 0);
		vec.insert(vec.end(), clear.begin(), clear.end());
	}
	decrypt(ciphertexts.back(), encoder, decryptor, clear, error);
	vec.insert(vec.end(), clear.begin(), clear.end());
}

void multiply_plain_to_many_inplace(vector<Ciphertext>& cts, Plaintext& plain, Evaluator& evaluator)
{
	for(vector<Ciphertext>::iterator it = cts.begin(); it!= cts.end(); ++it){
		evaluator.multiply_plain_inplace(*it, plain);
	}
}

void multiply_plain_to_many(vector<Ciphertext>& cts, Plaintext& plain, Evaluator& evaluator, vector<Ciphertext>& out)
{
	out.clear();
	out.reserve(cts.size());

	out = cts;
	multiply_plain_to_many_inplace(out, plain, evaluator);

	/*Ciphertext temp;
	for (vector<Ciphertext>::iterator it = cts.begin(); it != cts.end(); ++it) {
		evaluator.multiply_plain(*it, plain, temp);
		out.push_back(temp);
	}*/
}

void subtract_cipher_to_many_inplace(vector<Ciphertext>& cts, Ciphertext& sub, Evaluator& evaluator)
{
	for (vector<Ciphertext>::iterator it = cts.begin(); it != cts.end(); ++it) {
		evaluator.sub_inplace(*it, sub);
	}
}

void subtract_cipher_to_many(vector<Ciphertext>& cts, Ciphertext& sub, Evaluator& evaluator, vector<Ciphertext>& out)
{
	out.clear();
	out.reserve(cts.size());

	out = cts;
	subtract_cipher_to_many_inplace(out, sub, evaluator);
}

void rescale_many_to_next_inplace(vector<Ciphertext>& cts, Evaluator& evaluator)
{
	for (vector<Ciphertext>::iterator it = cts.begin(); it != cts.end(); ++it) {
		evaluator.rescale_to_next_inplace(*it);
	}
}

void square_many_inplace(vector<Ciphertext>& cts, Evaluator& evaluator)
{
	for (vector<Ciphertext>::iterator it = cts.begin(); it != cts.end(); ++it) {
		evaluator.square_inplace(*it);
	}
}

void square_many(vector<Ciphertext>& cts, Evaluator& evaluator, vector<Ciphertext>& out)
{
	out.clear();

	out = cts;
	square_many_inplace(out, evaluator);
}

void homomorphicTotal(Ciphertext& input, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output, size_t chunkExponent)
{
	if (chunkExponent == -1)
		chunkExponent = log2(input.poly_modulus_degree()) - (size_t)1;

	cyclic_add(input, gal_keys, evaluator, output, chunkExponent);
}

void homomorphicTotal(vector<Ciphertext>& input, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output)
{
	evaluator.add_many(input, output);

	cyclic_add_inplace(output, gal_keys, evaluator);
}

void homomorphicMean(Ciphertext& input, Plaintext& oneOverN, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output, size_t chunkExponent)
{
	homomorphicTotal(input, gal_keys, evaluator, output, chunkExponent);
	evaluator.multiply_plain_inplace(output, oneOverN);
}

void homomorphicMean(vector<Ciphertext>& input, Plaintext& oneOverN, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output)
{
	homomorphicTotal(input, gal_keys, evaluator, output);
	evaluator.multiply_plain_inplace(output, oneOverN);
}

void preparePlaintexts(double numberOfLines, double scale, CKKSEncoder& encoder, vector<Plaintext>& output)
{
	output.clear();
	
	Plaintext oneOverSqrt, oneOverSqrtCubed, fixitPlain, oneOverN;
	double slots = (double) encoder.slot_count();
	double numberOfCiphertexts = ceil(numberOfLines / slots);

	double sqrtN = pow(numberOfLines, -0.5);
	double threeRtN = pow(numberOfLines, -1.5);
	double fixitTerm = pow(numberOfLines, -1.5) * pow(slots * numberOfCiphertexts - numberOfLines, .5);
	double oneN = pow(numberOfLines, -1);


	encoder.encode(sqrtN, scale, oneOverSqrt);
	encoder.encode(threeRtN, scale, oneOverSqrtCubed);
	encoder.encode(fixitTerm, scale, fixitPlain);
	encoder.encode(oneN, scale, oneOverN);

	output.push_back(oneOverSqrt);
	output.push_back(oneOverSqrtCubed);
	output.push_back(fixitPlain);
	output.push_back(oneOverN);
}

void preparePlaintexts(Ciphertext& input, double numberOfLines, CKKSEncoder& encoder, vector<Plaintext>& output)
{
	double scale = input.scale();
	double poly_modulus_degree = (double)input.poly_modulus_degree();

	preparePlaintexts(numberOfLines, scale, encoder, output);
}

void preparePlaintexts(vector<size_t> numberOfLines, double scale, CKKSEncoder& encoder, vector<Plaintext>& output)
{
	output.clear();
	
	Plaintext oneOverSqrt, oneOverSqrtCubed, fixitPlain, oneOverN;
	vector<double> sqrtN, threeRtN, fixitTerm, oneN;
	double temp;

	size_t slots = encoder.slot_count();
	//cout << numberOfLines.size() << endl;
	size_t maxListSize = 0;
	for (const auto& val : numberOfLines) {
		//cout << val << endl;
		if (val > maxListSize)
			maxListSize = val;
	}
	//cout << "maxListSize = " << maxListSize << endl;
	size_t expOf2 = 0;
	while (maxListSize > pow(2, expOf2)) {
		expOf2++;
		//cout << expOf2 << " " << pow(2, expOf2) << endl;

	}

	if (pow(2, expOf2) * numberOfLines.size() > slots)
		throw logic_error("vecs is malformed; it won't fit in a single ciphertext!");

	for (int i = 0; i < numberOfLines.size(); i++) {
		temp = pow((double)numberOfLines.at(i), -0.5);
		for (int j = 0; j < pow(2, expOf2); j++) {
			sqrtN.push_back(temp);
		}
	}

	for (int i = 0; i < numberOfLines.size(); i++) {
		temp = pow((double)numberOfLines.at(i), -1.5);
		for (int j = 0; j < pow(2, expOf2); j++) {
			threeRtN.push_back(temp);
		}
	}

	for (int i = 0; i < numberOfLines.size(); i++) {
		temp = pow(pow(2, expOf2) - numberOfLines.at(i), 0.5) * pow((double)numberOfLines.at(i), -1.5);
		for (int j = 0; j < pow(2, expOf2); j++) {
			fixitTerm.push_back(temp);
		}
	}

	for (int i = 0; i < numberOfLines.size(); i++) {
		temp = pow((double)numberOfLines.at(i), -1);
		for (int j = 0; j < pow(2, expOf2); j++) {
			oneN.push_back(temp);
		}
	}

	encoder.encode(sqrtN, scale, oneOverSqrt);
	encoder.encode(threeRtN, scale, oneOverSqrtCubed);
	encoder.encode(fixitTerm, scale, fixitPlain);
	encoder.encode(oneN, scale, oneOverN);

	output.push_back(oneOverSqrt);
	output.push_back(oneOverSqrtCubed);
	output.push_back(fixitPlain);
	output.push_back(oneOverN);
}

void preparePlaintexts(vector<size_t> numberOfLines, double scale, CKKSEncoder& encoder, vector<vector<Plaintext>>& output)
{
	size_t slotsExp = log2(encoder.slot_count());

	size_t maxListSize = 0;
	for (const auto& val : numberOfLines) {
		if (val > maxListSize)
			maxListSize = val;
	}

	size_t expOf2 = 0;
	while (maxListSize > pow(2, expOf2))
		expOf2++;

	double numbLines = (double)numberOfLines.size();
	double numberOfListsPerCt = pow(2, slotsExp - expOf2);
	int numberOfPtSets = (int)ceil( numbLines / numberOfListsPerCt);
	
	output.clear();
	output.reserve(numberOfPtSets);
	//cout << "NumberofPts " << numberOfPtSets << endl;
	vector<size_t> input;
	vector<Plaintext> pts;
	for (size_t i = 0; i < numberOfPtSets - 1; i++) {
		input = vector(numberOfLines.begin() + i * numberOfListsPerCt, numberOfLines.begin() + (i + 1) * numberOfListsPerCt);
		preparePlaintexts(input, scale, encoder, pts);
		output.push_back(pts);
	}
	input = vector(numberOfLines.begin() + (numberOfPtSets - 1) * numberOfListsPerCt, numberOfLines.end());
	preparePlaintexts(input, scale, encoder, pts);
	output.push_back(pts);
}

void specialPlaintexts(size_t chunkExponent, double scale, CKKSEncoder& encoder, vector<Plaintext>& twoPts)
{
	twoPts.clear();
	size_t slotCount = encoder.slot_count();
	size_t chunkSize = pow(2, chunkExponent);

	Plaintext zoPt, onesPt;
	vector<double> zerosOnes, ones;

	for (int i = 0; i < slotCount; i++) {
		ones.push_back(1);
		if (0 == i % chunkSize)
			zerosOnes.push_back(1);
		else
			zerosOnes.push_back(0);
	}

	encode(ones, scale, encoder, onesPt);
	encode(zerosOnes, scale, encoder, zoPt);
	twoPts.push_back(zoPt);
	twoPts.push_back(onesPt);
}

void homomorphicVariance(Ciphertext& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output)
{
	Ciphertext totalct, firstTerm, secondTerm, thirdTerm, skews, summand, result;

	cyclic_add(input, gal_keys, evaluator, totalct);
	evaluator.multiply_plain(input, nPlaintexts.at(0), firstTerm);
	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}
	evaluator.sub(firstTerm, secondTerm, skews);

	evaluator.rescale_to_next_inplace(skews);

	evaluator.square(skews, summand);

	evaluator.relinearize_inplace(summand, relin_keys);

	cyclic_add(summand, gal_keys, evaluator, result);

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, output);
	}
	else {
		output = result;
	}
}

void homomorphicVariance(Ciphertext& input, vector<Plaintext>& nPlaintexts, vector<Plaintext>& specialPts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output, size_t chunkExponent)
{
	Ciphertext addct, totalct, firstTerm, secondTerm, thirdTerm, skews, summand, result;

	cyclic_add(input, gal_keys, evaluator, addct, chunkExponent);
	evaluator.multiply_plain_inplace(addct, specialPts.at(0));
	cyclic_add_right(addct, gal_keys, evaluator, totalct, chunkExponent);

	evaluator.multiply_plain(input, nPlaintexts.at(0), firstTerm);
	evaluator.multiply_plain_inplace(firstTerm, specialPts.at(1));

	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}
	evaluator.sub(firstTerm, secondTerm, skews);
	evaluator.rescale_to_next_inplace(skews);
	evaluator.rescale_to_next_inplace(skews);

	//evaluator.rescale_to_next_inplace(skews);

	evaluator.square(skews, summand);
	evaluator.relinearize_inplace(summand, relin_keys);
	cyclic_add(summand, gal_keys, evaluator, result, chunkExponent);

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, output);
	}
	else {
		output = result;
	}
}

void homomorphicVariance(vector<Ciphertext>& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output)
{
	Ciphertext totalct, ct, secondTerm, thirdTerm, result;
	vector<Ciphertext> firstTerm, skews, summand;
	add_many(input, evaluator, ct);
	cyclic_add(ct, gal_keys, evaluator, totalct);
	multiply_plain_to_many(input, nPlaintexts.at(0), evaluator, firstTerm);
	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	subtract_cipher_to_many(firstTerm, secondTerm, evaluator, skews);
	rescale_many_to_next_inplace(skews, evaluator);
	square_many(skews, evaluator, summand);

	add_many(summand, evaluator, result);
	evaluator.relinearize_inplace(result, relin_keys);
	cyclic_add_inplace(result, gal_keys, evaluator);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error& code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, output);
	}
	else {
		output = result;
	}
}

void homomorphicVariance(Ciphertext& input, double numberOfLines, CKKSEncoder& encoder, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output)
{
	vector<Plaintext> nPlaintexts;
	Plaintext oneOverSqrt, oneOverSqrtCubed, fixitPlain;
	double scale = input.scale();
	double poly_modulus_degree = (double)input.poly_modulus_degree();


	double sqrtN = pow(numberOfLines, -0.5);
	double threeRtN = pow(numberOfLines, -1.5);
	double fixitTerm = pow(numberOfLines, -1.5) * pow(poly_modulus_degree / 2 - numberOfLines, .5);

	encoder.encode(sqrtN, scale, oneOverSqrt);
	encoder.encode(threeRtN, scale, oneOverSqrtCubed);
	encoder.encode(fixitTerm, scale, fixitPlain);

	nPlaintexts.push_back(oneOverSqrt);
	nPlaintexts.push_back(oneOverSqrtCubed);
	nPlaintexts.push_back(fixitPlain);

	homomorphicVariance(input, nPlaintexts, gal_keys, evaluator, relin_keys, output);
}

void homomorphicVariance(vector<Ciphertext>& input, double numberOfLines, CKKSEncoder& encoder, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output)
{
	vector<Plaintext> nPlaintexts;
	Plaintext oneOverSqrt, oneOverSqrtCubed, fixitPlain;
	double scale = input.at(0).scale();
	double poly_modulus_degree = (double)input.at(0).poly_modulus_degree();


	double sqrtN = pow(numberOfLines, -0.5);
	double threeRtN = pow(numberOfLines, -1.5);
	double fixitTerm = pow(numberOfLines, -1.5) * pow(poly_modulus_degree / 2 - numberOfLines, .5);

	encoder.encode(sqrtN, scale, oneOverSqrt);
	encoder.encode(threeRtN, scale, oneOverSqrtCubed);
	encoder.encode(fixitTerm, scale, fixitPlain);

	nPlaintexts.push_back(oneOverSqrt);
	nPlaintexts.push_back(oneOverSqrtCubed);
	nPlaintexts.push_back(fixitPlain);

	homomorphicVariance(input, nPlaintexts, gal_keys, evaluator, relin_keys, output);
}

void homomorphicStats(Ciphertext& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs)
{
	outputs.clear();

	Ciphertext totalct, meanct, varct;

	homomorphicTotal(input, gal_keys, evaluator, totalct);
	outputs.push_back(totalct);
	evaluator.multiply_plain(totalct, nPlaintexts.at(3), meanct);
	outputs.push_back(meanct);

	Ciphertext firstTerm, secondTerm, thirdTerm, skews, summand, result;

	evaluator.multiply_plain(input, nPlaintexts.at(0), firstTerm);
	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}
	evaluator.sub(firstTerm, secondTerm, skews);

	evaluator.rescale_to_next_inplace(skews);

	evaluator.square(skews, summand);

	evaluator.relinearize_inplace(summand, relin_keys);

	cyclic_add(summand, gal_keys, evaluator, result);

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, varct);
	}
	else {
		varct = result;
	}

	outputs.push_back(varct);
}

void homomorphicStats(vector<Ciphertext>& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs)
{
	outputs.clear();

	Ciphertext totalct, meanct, varct;

	homomorphicTotal(input, gal_keys, evaluator, totalct);
	outputs.push_back(totalct);
	evaluator.multiply_plain(totalct, nPlaintexts.at(3), meanct);
	outputs.push_back(meanct);

	Ciphertext secondTerm, thirdTerm, result;
	vector<Ciphertext> firstTerm, skews, summand;

	multiply_plain_to_many(input, nPlaintexts.at(0), evaluator, firstTerm);
	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}
	subtract_cipher_to_many(firstTerm, secondTerm, evaluator, skews);

	rescale_many_to_next_inplace(skews, evaluator);

	square_many(skews, evaluator, summand);
	add_many(summand, evaluator, result);

	evaluator.relinearize_inplace(result, relin_keys);

	cyclic_add_inplace(result, gal_keys, evaluator);

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, varct);
	}
	else {
		varct = result;
	}

	outputs.push_back(varct);
}

void homomorphicStats(Ciphertext& input, vector<Plaintext>& nPlaintexts, vector<Plaintext>& specialPts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs, size_t chunkExponent)
{
	outputs.clear();

	Ciphertext addct, totalct, meanct, varct;

	homomorphicTotal(input, gal_keys, evaluator, addct, chunkExponent);
	evaluator.multiply_plain_inplace(addct, specialPts.at(0));
	cyclic_add_right(addct, gal_keys, evaluator, totalct, chunkExponent);

	outputs.push_back(totalct);
	evaluator.multiply_plain(totalct, nPlaintexts.at(3), meanct);
	outputs.push_back(meanct);

	Ciphertext firstTerm, secondTerm, thirdTerm, skews, summand, result;

	evaluator.multiply_plain(input, nPlaintexts.at(0), firstTerm);
	evaluator.multiply_plain_inplace(firstTerm, specialPts.at(1));

	evaluator.multiply_plain(totalct, nPlaintexts.at(1), secondTerm);

	int notFull = 0;
	try {
		evaluator.multiply_plain(totalct, nPlaintexts.at(2), thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.rescale_to_next_inplace(thirdTerm);
		evaluator.square_inplace(thirdTerm);
		evaluator.relinearize_inplace(thirdTerm, relin_keys);
	}
	catch (logic_error code) {
		if (strcmp(code.what(), "result ciphertext is transparent") != 0) {
			throw(code);
		}
		notFull = 1;
	}
	evaluator.sub(firstTerm, secondTerm, skews);
	evaluator.rescale_to_next_inplace(skews);
	evaluator.rescale_to_next_inplace(skews);

	evaluator.square(skews, summand);

	evaluator.relinearize_inplace(summand, relin_keys);

	cyclic_add(summand, gal_keys, evaluator, result, chunkExponent);

	if (notFull == 0) {
		evaluator.sub(result, thirdTerm, varct);
	}
	else {
		varct = result;
	}

	outputs.push_back(varct);
}

void decryptAndExtract(Ciphertext& input, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& outvec, size_t chunkExp, size_t capacity)
{
	outvec.clear();
	vector<double> result;
	decrypt(input, encoder, decryptor, result);

	size_t numberOfElements = capacity * pow(2, chunkExp);
	if (capacity == 0)
		numberOfElements = result.size();

	if (result.size() < numberOfElements)
		throw("Result doesn't have the number of elements requested");

	for (int i = 0; i < numberOfElements; i += pow(2, chunkExp)) {
		outvec.push_back(result.at(i));
	}
}

void decryptAndExtract(vector<Ciphertext>& input, CKKSEncoder& encoder, Decryptor& decryptor, vector<vector<double>>& outvec, size_t chunkExp, size_t capacity)
{
	outvec.clear();
	vector<double> resT, resM, resV;
	
	decryptAndExtract(input.at(0), encoder, decryptor, resT, chunkExp, capacity);
	decryptAndExtract(input.at(1), encoder, decryptor, resM, chunkExp, capacity);
	decryptAndExtract(input.at(2), encoder, decryptor, resV, chunkExp, capacity);

	outvec.push_back(resT);
	outvec.push_back(resM);
	outvec.push_back(resV);
}
