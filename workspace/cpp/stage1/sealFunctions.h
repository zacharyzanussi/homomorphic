#ifndef SEALFUNCTIONS_H
#define SEALFUNCTIONS_H

#pragma once

#include "seal/seal.h"

using namespace seal;
using namespace std;


void cyclic_add_inplace(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, int N = -1);

void cyclic_add(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, Ciphertext& result, int N = -1);

void cyclic_add_inplace_right(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, int N = -1);

void cyclic_add_right(Ciphertext& toAdd, const GaloisKeys& galois_keys, Evaluator& evaluator, Ciphertext& result, int N = -1);

void add_many(vector<Ciphertext>& ciphertexts, Evaluator& evaluator, Ciphertext& destination);

void encode(vector<double>& vec, double scale, CKKSEncoder& encoder, Plaintext& destination);

void encodeVector(vector<double>& vec, double scale, CKKSEncoder& encoder, vector<Plaintext>& plaintexts);

void decode(Plaintext& plaintext, CKKSEncoder& encoder, vector<double>& destination, double error);

void decodeVector(vector<Plaintext>& plaintexts, CKKSEncoder& encoder, vector<double>& vec, double error);

// Takes a single plaintext and returns an encryption of it
void encrypt(Plaintext& plaintext, Encryptor& encryptor, Ciphertext& ciphertext);

// Takes a vector of doubles and encodes and encrypts it. vec must be short enough to fit into a single ciphertext
void encrypt(vector<double>& vec, double scale, CKKSEncoder& encoder, Encryptor& encryptor, Ciphertext& ciphertext);

// Takes a vector of vectors of doubles and smart-packs them into a single ciphertext. Automatically determines the best packing
// technique. Throws an invalid_argument error if vec can't fit into a single ciphertext; this can happen if the maximum list size
// times the number of lists is greater than slot count
void encrypt(vector<vector<double>>& vec, double scale, CKKSEncoder& encoder, Encryptor& encryptor, Ciphertext& ciphertext);

// Takes a vector of doubles and encodes and encrypts it. vec can be any length; we return a vector of ciphertexts
void encryptVector(vector<double>& vec, double scale, CKKSEncoder& encoder, Encryptor& encryptor, vector<Ciphertext>& ciphertexts);

void decrypt(Ciphertext& ciphertext, Decryptor& decryptor, Plaintext& plaintext);

void decrypt(Ciphertext& ciphertext, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& vec, double error = 1e-5);

void decryptVector(vector<Ciphertext>& ciphertexts, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& vec, double error = 1e-5);

void peekAtCiphertext(Ciphertext& ct, CKKSEncoder& encoder, Decryptor& decryptor, int n = 4);

void peekAtCiphertext(vector<Ciphertext>& ct, CKKSEncoder& encoder, Decryptor& decryptor, int n = 4);

void multiply_plain_to_many_inplace(vector<Ciphertext>& cts, Plaintext& plain, Evaluator& evaluator);

void multiply_plain_to_many(vector<Ciphertext>& cts, Plaintext& plain, Evaluator& evaluator, vector<Ciphertext>& out);

void subtract_cipher_to_many_inplace(vector<Ciphertext>& cts, Ciphertext& sub, Evaluator& evaluator);

void subtract_cipher_to_many(vector<Ciphertext>& cts, Ciphertext& sub, Evaluator& evaluator, vector<Ciphertext>& out);

void rescale_many_to_next_inplace(vector<Ciphertext>& cts, Evaluator& evaluator);

void square_many_inplace(vector<Ciphertext>& cts, Evaluator& evaluator);

void square_many(vector<Ciphertext>& cts, Evaluator& evaluator, vector<Ciphertext>& out);

// Calculate the total of all values in input homomorphically. The parameter chunkExponent refers to the size of the 'chunks': that is, size of sublists
// that we are using for smart-packing. A value of -1 means that we aren't using smart packing; a value of n means that chunks are of length
// 2^n, and that there is 2^(N - n - 1) such lists, where N is the poly modulus exponent. If chunkExponent == -1, then the output value will fill the whole
// ciphertext; if not, the calculated values will be at indices == 0 mod 2^n.
void homomorphicTotal(Ciphertext& input, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output, size_t chunkExponent = -1);

void homomorphicTotal(vector<Ciphertext>& input, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output);

// Calculate the mean of all values in input homomorphically. The parameter chunkExponent refers to the size of the 'chunks': that is, size of sublists
// that we are using for smart-packing. A value of -1 means that we aren't using smart packing; a value of n means that chunks are of length
// 2^n, and that there is 2^(N - n - 1) such lists, where N is the poly modulus exponent. If chunkExponent == -1, then the output value will fill the whole
// ciphertext; if not, the calculated values will be at indices == 0 mod 2^n.
void homomorphicMean(Ciphertext& input, Plaintext& oneOverN, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output, size_t chunkExponent = -1);

void homomorphicMean(vector<Ciphertext>& input, Plaintext& oneOverN, GaloisKeys& gal_keys, Evaluator& evaluator, Ciphertext& output);

// Prepare a vector of plaintexts for use in the homomorphic fuctions. Prepares it in this order; one over sqrt(l), one over sqrt(l^3), sqrt(N - l) / sqrt(l^3), and one over l,
// where l is the number of lines, and N is the number of slots.
// The input ciphertext is there to make sure the parameters are appropriate; it will not be changed. This function extracts the scale and poly_modulus_degree from input for convenience
void preparePlaintexts(Ciphertext& input, double numberOfLines, CKKSEncoder& encoder, vector<Plaintext>& output);

// Prepare a vector of plaintexts for use in the homomorphic fuctions. Prepares it in this order; one over sqrt(l), one over sqrt(l^3), sqrt(N - l) / sqrt(l^3), and one over l,
// where l is the number of lines, and N is the number of slots, which is poly_modulus_degree divided by 2.
void preparePlaintexts(double numberOfLines, double scale, CKKSEncoder& encoder, vector<Plaintext>& output);

// Prepares a vector of plaintexts for use in smart-packing in homomorphic functions. 
void preparePlaintexts(vector<size_t> numberOfLines, double scale, CKKSEncoder& encoder, vector<Plaintext>& output);

// Prepares plaintexts for smart-packing for any size vector numberOfLines.
void preparePlaintexts(vector<size_t> numberOfLines, double scale, CKKSEncoder& encoder, vector<vector<Plaintext>>& output);

// Prepares the two special plaintexts for use in smart-packing for the variance. Outputs the plaintexts Zero-One and One, in that order
void specialPlaintexts(size_t chunkExponent, double scale, CKKSEncoder& encoder, vector<Plaintext>& twoPts);


// nPlaintexts should store the following values in this order; one over sqrt(l), one over sqrt(l^3), and sqrt(N - l) / sqrt(l^3),
// where N is the number of slots in a plaintext and l is the number of full slots in input.
void homomorphicVariance(Ciphertext& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output);

// Calculate the variance of the values in input homomorphically. The parameter chunkExponent refers to the size of the 'chunks': that is, size of sublists
// that we are using for smart-packing. A value of -1 means that we aren't using smart packing; a value of n means that chunks are of length
// 2^n, and that there is 2^(N - n - 1) such lists, where N is the poly modulus exponent. If chunkExponent == -1, then the output value will fill the whole
// ciphertext; if not, the calculated values will be at indices == 0 mod 2^n.
// nPlaintexts should store the following values in this order; one over sqrt(l), one over sqrt(l^3), and sqrt(N - l) / sqrt(l^3),
// where N is the number of slots in a plaintext and l is the number of full slots in input.
// specialPts should be an encoding of the Zero-One and One plaintexts, respectively.
void homomorphicVariance(Ciphertext& input, vector<Plaintext>& nPlaintexts, vector<Plaintext>& specialPts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output, size_t chunkExponent);

void homomorphicVariance(vector<Ciphertext>& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output);

// This one will compute nPlaintexts for you; you just need to supply the number of lines. This is for testing and is not in our use case. 
void homomorphicVariance(Ciphertext& input, double numberOfLines, CKKSEncoder& encoder, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output);

void homomorphicVariance(vector<Ciphertext>& input, double numberOfLines, CKKSEncoder& encoder, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, Ciphertext& output);

// nPlaintexts should store the following values in this order; one over sqrt(l), one over sqrt(l^3), sqrt(N - l) / sqrt(l^3), and one over l,
// where N is the number of slots in a plaintext and l is the number of full slots in input.
// outputs stores ciphertexts full of the total, mean, and variance, in that order. 
void homomorphicStats(Ciphertext& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs);

void homomorphicStats(vector<Ciphertext>& input, vector<Plaintext>& nPlaintexts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs);

void homomorphicStats(Ciphertext& input, vector<Plaintext>& nPlaintexts, vector<Plaintext>& specialPts, GaloisKeys& gal_keys, Evaluator& evaluator, RelinKeys& relin_keys, vector<Ciphertext>& outputs, size_t chunkExponent);

// WARNING: this function WILL spit out garbage at the end if your ciphertext is not full. It's up to you to realize this
void decryptAndExtract(Ciphertext& input, CKKSEncoder& encoder, Decryptor& decryptor, vector<double>& outvec, size_t chunkExp, size_t capacity = 0);

// WARNING: this function WILL spit out garbage at the end if your ciphertext is not full. It's up to you to realize this
void decryptAndExtract(vector<Ciphertext>& input, CKKSEncoder& encoder, Decryptor& decryptor, vector<vector<double>>& outvec, size_t chunkExp, size_t capacity = 0);

#endif // !SEALFUNCTIONS_H

/* Here are the functions for timing. Make sure to add #include <chrono>
	chrono::high_resolution_clock::time_point time_start, time_end;
	chrono::microseconds time_diff;

	time_start = chrono::high_resolution_clock::now();
	time_end = chrono::high_resolution_clock::now();
	time_diff = chrono::duration_cast<chrono::microseconds>(time_end - time_start);
*/