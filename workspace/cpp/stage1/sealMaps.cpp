#include "sealMaps.h"
#include "sealFunctions.h"

using namespace std;
using namespace seal;

EncList::EncList()
{
	pts.reserve(4);
}

EncList::EncList(vector<Ciphertext>& cts, CKKSEncoder& encoder, size_t numberOfLines)
{
	this->cts = cts;
	pts.reserve(4);
	preparePlaintexts(cts.back(), (double)numberOfLines, encoder, this->pts);
}

void encryptMap(RecordsMap& inmap, double scale, CKKSEncoder& encoder, Encryptor& encryptor, EncryptedMap& outmap)
{
	outmap.clear();
	
	vector<Ciphertext> cts;
	vector<Plaintext> pts;
	EncList enc;
	for (auto& pair : inmap){
		preparePlaintexts(pair.second.size(), scale, encoder, pts);
		encryptVector(pair.second, scale, encoder, encryptor, cts);
		enc = EncList(cts, pts);
		outmap.insert({ pair.first, enc });
	}
}

void decryptMap(EncryptedMap& inmap, CKKSEncoder& encoder, Decryptor& decryptor, RecordsMap& outmap)
{
	outmap.clear();

	vector<double> vec;
	vector<Ciphertext> cts;
	for (auto& pair : inmap) {
		pair.second.getCts(cts);
		decryptVector(cts, encoder, decryptor, vec);

		outmap.insert({ pair.first, vec });
	}
}

void decryptMap(EncResultsMap& inmap, CKKSEncoder& encoder, Decryptor& decryptor, ResultsMap& outmap)
{
	outmap.clear();

	vector<double> vec;
	vector<double> results;
	for (auto& pair : inmap) {
		results.clear();
		for (int i = 0; i < 3; i++) {
			decrypt(pair.second.at(i), encoder, decryptor, vec);
			results.push_back(vec.back());
		}

		outmap.insert({ pair.first, results });
	}
}

void computeEncStats(EncryptedMap& inmap, Evaluator& evaluator, GaloisKeys& gal_keys, RelinKeys& relin_keys, EncResultsMap& outmap)
{
	outmap.clear();

	Ciphertext ct;
	vector<Ciphertext> outcts;
	vector<Plaintext> pts;
	for (auto& pair : inmap) {
		pair.second.getFirstCt(ct);
		pair.second.getPts(pts);
		homomorphicStats(ct, pts, gal_keys, evaluator, relin_keys, outcts);

		outmap.insert({ pair.first, outcts });
	}
}
