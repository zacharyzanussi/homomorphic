#pragma once

#include "seal/ciphertext.h"
#include "seal/plaintext.h"
#include "seal/ckks.h"
#include "seal/encryptor.h"
#include "seal/decryptor.h"
#include "seal/evaluator.h"
#include "recordkey.h"
#include <vector>

using namespace std;
using namespace seal;

class recordKey;
//class CKKSEncoder;
//class seal::Encryptor;


class EncList
{
private:
	vector<Ciphertext> cts;
	vector<Plaintext>  pts;

public:
	EncList();
	EncList(vector<Ciphertext>& cts, CKKSEncoder& encoder, size_t numberOfLines);
	EncList(vector<Ciphertext>& cts, vector<Plaintext>& pts) { this->cts = cts; this->pts = pts; }


	size_t size() { return this->cts.size(); }

	void getSqrtN(Plaintext& pt) { pt = this->pts.at(0); }
	void getSqrtNCubed(Plaintext& pt) { pt = this->pts.at(1); }
	void getFixit(Plaintext& pt) { pt = this->pts.at(2); }
	void getN(Plaintext& pt) { pt = this->pts.at(3); }
	void getPts(vector<Plaintext>& pts) { pts = this->pts; }

	void getCts(vector<Ciphertext>& cts) { cts = this->cts; }
	void getFirstCt(Ciphertext& ct) { ct = this->cts.front(); }
};

// Unclear on whether we even need this.
//class EncResults
//{
//private:
//	vector<Ciphertext> results;
//
//	
//public:
//	EncResults() { results.reserve(3); }
//
//	
//};

typedef map<recordkey, EncList> EncryptedMap;

typedef map<recordkey, vector<Ciphertext>> EncResultsMap;

void encryptMap(RecordsMap& inmap, double scale, CKKSEncoder& encoder, Encryptor& encryptor, EncryptedMap& outmap);

// Takes an EncryptedMap inmap and decrypts it into a RecordsMap.
void decryptMap(EncryptedMap& inmap, CKKSEncoder& encoder, Decryptor& decryptor, RecordsMap& outmap);

// Takes an EncResultsMap inmap and decrypts it into a ResultsMap.
void decryptMap(EncResultsMap& inmap, CKKSEncoder& encoder, Decryptor& decryptor, ResultsMap& outmap);

// Since the variance is only implemented for a single ciphertext, this fucntion only takes the first ciphertext in each 
// entry in inmap
void computeEncStats(EncryptedMap& inmap, Evaluator& evaluator, GaloisKeys& gal_keys, RelinKeys& relin_keys, EncResultsMap& outmap);