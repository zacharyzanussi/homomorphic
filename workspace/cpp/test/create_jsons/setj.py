#!/usr/bin/env python
# coding: utf-8


import os, sys
from pathlib import Path
import argparse
import json
from json import JSONEncoder
from typing import List



def dict_from_obj(obj):
    """ Returns a dictionary with object attributes

    Parameters
    ----------
    obj : object

    Returns
    -------
    A python dictionary with attributes and values


    """
    return dict(
        (key, value)
        for (key, value) in obj.__dict__.items())

def convert_delim_keys(mydict, subs1=' ', subs2='_'):
    """ Replaces strings in keys

    Example: converts dict with snake like case keys into words separated by spaces

    Parameters
    ----------
    mydict : dict
        the input python dictionary
    subs1 : str
        the substring to substitute on the key
    subs2 : str
        the substring that replaces `subs1`

    Returns
    -------
    A python dictionary with attributes and values


    """
    return dict(
        (key.replace(subs1, subs2), value)
        for (key, value) in mydict.items())

class ParametersEncoder(JSONEncoder):
    """ JSON Encoder for Parameters class

        Notes
        ----
        We are replacing all keys with snake case to spaces
    """
    def default(self, o):
        return convert_delim_keys(o.__dict__, "_", " ")


class Parameters:
    """ Define parameters

        Notes
        ----
        We are using keys with spaces (not snake case)
    """
    def __init__(self, values: dict = None):
        values = values if values is not None else {}
        self.Polynomial_degree: int = values.get("Polynomial degree", 16)
        self.Number_of_levels: int = values.get("Number of levels", 9)
        self.Scale_bits: int = values.get("Scale bits", 30)
        self.Initial_scale: int = values.get("Initial scale", 55)
        self.Epochs: int = values.get("Epochs", 6)
        self.Training_samples: int = values.get("Training samples", 4000)
        self.Testing_samples: int = values.get("Testing samples", 4000)
        self.Batch_size: int = values.get("Batch size", 200)
        self.Maximum_tile_size: int = values.get("Maximum tile size", 100)
        self.Number_of_labels: int = values.get("Number of labels", 5)
        self.Number_of_features: int = values.get("Number of features", 4095)
        self.Hash_size: int = values.get("Hash size", 4096)
        self.Learning_rates: List[float] = values.get("Learning rates", [1.1, 2.2, 2.3])
        self.Regularization: float = values.get("Regularization", 1.5)
        self.Approximation: str = values.get("Approximation", "tight")
        self.Momentum_coefficient: float = values.get("Momentum coefficient", 0.95)
        self.Number_of_threads: int = values.get("Number of threads", 8)
        self.Files: str = values.get("Files", "files_1.json")

    def __repr__(self):
        return "Parameters Schema[" + ", ".join((
            f"Polynomial degree: {repr(self.Polynomial_degree)}",
            f"Epochs: {repr(self.Epochs)}",
            f"Training samples: {repr(self.Training_samples)}",
            f"Testing samples: {repr(self.Testing_samples)}",
            f"Batch size: {repr(self.Batch_size)}",
            f"Maximum tile size: {repr(self.Maximum_tile_size)}",
            f"Number of labels: {repr(self.Number_of_labels)}",
            f"Number of features: {repr(self.Number_of_features)}",
            f"Hash size: {repr(self.Hash_size)}",
            f"Learning rates: {repr(self.Learning_rates)}",
            f"Regularization: {repr(self.Regularization)}",
            f"Approximation: {repr(self.Approximation)}",
            f"Momentum coefficient: {repr(self.Momentum_coefficient)}",
            f"Number of threads: {repr(self.Number_of_threads)}",
            f"Files: {repr(self.Files)}",
        )) + "]"



class FilesEncoder(JSONEncoder):
    """ JSON Encoder for Parameters class

    Note
    ----
    We are replacing all keys with snake case to spaces
    """
    def default(self, o):
        return convert_delim_keys(o.__dict__, "_", " ")
    
class Files:
    """ Define file names

        Notes
        ----
        We are using keys with spaces (not snake case)
    """
    def __init__(self, values: dict = None):
        values = values if values is not None else {}
        self.Train_file: str = values.get("Train file", "train.csv")
        self.Test_file: str = values.get("Test file", "test.csv")
        self.Weights_location: str = values.get("Weights location", "./")
        self.Initial_weights_name: str = values.get("Initial weights name", "initial_weights.wt")
        self.Weights_name: str = values.get("Weights name", "weights1")
        self.Predict_weights_name: str = values.get("Predict weights name", "predict_weights.txt")
        self.Records_file: str = values.get("Records file", "records.csv")
        self.Ciphertext_location: str = values.get("Ciphertext location", "./")
        self.Ciphertext_train_name: str = values.get("Ciphertext train name", "batch")
        self.Ciphertext_test_name: str = values.get("Ciphertext test name", "test")
    
    
    def __repr__(self):
        return "Files Schema[" + ", ".join((
            f"Train file: {repr(self.Train_file)}",
            f"Test file: {repr(self.Test_file)}",
            f"Weights location: {repr(self.Weights_location)}",
            f"Initial weights name: {repr(self.Initial_weights_name)}",
            f"Weights name: {repr(self.Weights_name)}",
            f"Predict weights name: {repr(self.Predict_weights_name)}",
            f"Records file: {repr(self.Records_file)}",
            f"Ciphertext location: {repr(self.Ciphertext_location)}",
            f"Ciphertext train name: {repr(self.Ciphertext_train_name)}",
            f"Ciphertext test name: {repr(self.Ciphertext_test_name)}",
        )) + "]"


""" Description for help
"""
desc = """ Write parameters and files JSONs.
If json file is(are) provided they are read and can be modified by command line arguments.
If not new default jsons are created.
"""


def load_json(json_file):
    """ Loads a json file

    Parameters
    ----------
    json_file : str

    Returns
    -------
    The json in a python dictionary

    """
    with open(json_file, "r") as read_file:
        data = json.load(read_file)
    return data


def dump_json(json_dict, fname, defclass, encclass):
    """ Writes a json file from a dict

    Parameters
    ----------
    json_dict : dict
        the json dictionary
    fname : str
        the filename for the json
    defclass:
        the class that ingest the json_dict
    encclass:
        the JSON encoder for `defclass`

    """
    fjson = json.dumps(defclass(json_dict), indent=4, cls=encclass)
    #print(fjson)
    with open(fname, 'w') as outfile:
        outfile.write(fjson)

def main():

    # call the Argument parser
    parser = argparse.ArgumentParser(prog="./setj.py", description=desc)

    # positional optional arguments, json input files
    parser.add_argument('params', nargs='?', type=str,
                        default=None, help='parameters json template')
    parser.add_argument('files', nargs='?', type=str,
                        default=None, help='files json template')


    parser.add_argument('-n', '--name', type=str, required=True, help="file name suffix")
    parser.add_argument('-r', '--runs', type=int, help="Number of runs", default=0)
    parser.add_argument('-e', '--epochs_per_run', type=int, help="Epochs for each run", default=None)

    # parameters that exist in params
    parser.add_argument('--Polynomial_degree', type=int)
    parser.add_argument('--Number_of_levels', type=int)
    parser.add_argument('--Scale_bits', type=int)
    parser.add_argument('--Initial_scale', type=int)
    parser.add_argument('--Epochs', type=int)
    parser.add_argument('--Training_samples', type=int)
    parser.add_argument('--Testing_samples', type=int)
    parser.add_argument('--Batch_size', type=int)
    parser.add_argument('--Maximum_tile_size', type=int)
    parser.add_argument('--Number_of_labels', type=int)
    parser.add_argument('--Number_of_features', type=int)
    parser.add_argument('--Hash_size', type=int)
    parser.add_argument('--Learning_rates', type=str)
    parser.add_argument('--Regularization', type=float)
    parser.add_argument('--Approximation', type=str)
    parser.add_argument('--Momentum_coefficient', type=float)
    parser.add_argument('--Number_of_threads', type=int)
    parser.add_argument('--Files', type=str)

    # parameters that exist in files
    parser.add_argument('--Train_file', type=str)
    parser.add_argument('--Test_file', type=str)
    parser.add_argument('--Weights_location', type=str)
    parser.add_argument('--Initial_weights_name', type=str)
    parser.add_argument('--Weights_name', type=str)
    parser.add_argument('--Predict_weights_name', type=str)
    parser.add_argument('--Records_file', type=str)
    parser.add_argument('--Ciphertext_location', type=str)
    parser.add_argument('--Ciphertext_train_name', type=str)
    parser.add_argument('--Ciphertext_test_name', type=str)

    # parse arguments    
    args = parser.parse_args()

    # load or set the dictionary params
    params = args.params
    if params:
        print("Loading params from ", params)
        p = load_json(args.params)
    else:
        print("Using default values for params (optional)")
        p = Parameters()
        # we need to convert the snake case keys to spaces
        p = convert_delim_keys(dict_from_obj(p), "_", " ")

    # load or set the dictionary files
    files = args.files
    if files:
        print("Loading files from ", files)
        f = load_json(args.files)
    else:
        print("Using default values for files (optional)")
        f = Files()
        # we need to convert the snake case keys to spaces
        f = convert_delim_keys(dict_from_obj(f), "_", " ")

    # get the dictionary with arguments, convert snake case keys to spaces
    dvalues = convert_delim_keys(vars(args), "_", " ")

    # get the updated fields, i.e., common keys between two dicts
    # but value was changed via arguments
    new_params_key = dvalues.keys() & p.keys()
    # set values
    for key in new_params_key:
        # handle if we have a argument value for key
        if dvalues[key] is not None:
            p[key] = dvalues[key]

    # get the updated fields
    new_files_key = dvalues.keys() & f.keys()
    for key in new_files_key:
        if dvalues[key] is not None:
            f[key] = dvalues[key]

    # dump the main json
    param_fname = 'params_'+args.name+'.json'
    files_fname = 'files_'+args.name+'.json'
    # point the files_fname to the File field
    p['Files'] = files_fname
    if dvalues['Files'] is not None:
        p['Files'] = dvalues['Files']
        files_fname = p['Files'] + '_'+args.name+'.json'
    

    # First dump of jsons
    print("Generating {} and {}".format(param_fname, files_fname))
    dump_json(p.copy(), fname=param_fname, defclass=Parameters, encclass=ParametersEncoder)
    dump_json(f.copy(), fname=files_fname, defclass=Files, encclass=FilesEncoder)

    # if we have several runs we generate a set of jsons    
    if args.runs is not None and args.epochs_per_run is not None:
        print("Generating files for {} runs".format(args.runs))
        ne = args.epochs_per_run-1
        for i in range(args.runs):
            new_p = p.copy()
            new_f = f.copy()
            param_fname = 'params_' + args.name + '_' + str(i+1) + '.json'
            prefix = 'files_'
            if dvalues['Files'] is not None:
                prefix = dvalues['Files'] + "_"
            files_fname = prefix + args.name + '_' + str(i+1) + '.json'
            new_p['Files'] = files_fname
            if i == 0:
                initial_weights_name = f['Initial weights name']
            elif i == 1:
                initial_weights_name = f['Weights name'] + "_epoch" + "{:02}".format(ne) + '_refreshed.wt'
            else:
                initial_weights_name = f['Weights name'] + '_run' +  str(i) + '_epoch'\
                                     + "{:02}".format(ne) + '_refreshed.wt'
            new_f['Initial weights name'] = initial_weights_name
            weights_name = f['Weights name']
            if i != 0:
                weights_name = f['Weights name'] + '_' + 'run' + str(i+1)
            new_f['Weights name'] = weights_name
            print("  - Generating {} and {}".format(param_fname, files_fname))
            dump_json(new_p, fname=param_fname, defclass=Parameters, encclass=ParametersEncoder)
            dump_json(new_f, fname=files_fname, defclass=Files, encclass=FilesEncoder)

if __name__ == "__main__":
    main()


