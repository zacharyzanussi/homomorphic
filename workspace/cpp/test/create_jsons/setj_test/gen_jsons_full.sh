#!/bin/bash

# This will construct the jsons required to run a full test of the 
# project. Be warned: this is intensive, and should only be run with
# 32 GB RAM or more. 

python3 ../setj.py -n full \
				-r 6 \
				-e 3 \
				--Epochs 3 \
				--Polynomial_degree 15 \
				--Number_of_levels 12 \
				--Scale_bits 60 \
				--Training_samples 40000 \
				--Testing_samples 10000 \
				--Batch_size 1000 \
				--Maximum_tile_size 100 \
				--Number_of_labels 5 \
				--Number_of_features 4095 \
				--Hash_size 0 \
				--Regularization 0.1 \
				--Approximation "none" \
				--Momentum_coefficient 0.85 \
				--Number_of_threads 8 \
				--Train_file "/data/pytorch_training.csv" \
				--Test_file "/data/pytorch_testing.csv" \
				--Weights_location "/ct/test/full" \
				--Weights_name "weights" \
				--Predict_weights_name "predict_names.txt" \
				--Records_file "records.csv" \
				--Ciphertext_location "/ct/test/" \
				--Ciphertext_train_name "batch" \
				--Ciphertext_test_name "test"

# You might need to change some of the file names and locations,
# depending on your setup