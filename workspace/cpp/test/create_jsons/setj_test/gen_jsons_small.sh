#!/bin/bash

# This will construct the jsons required to run a smaller test of the
# project. This should run fine on most computers; it runs fine on a
# 8 GB laptop.

python3 ../setj.py -n small \
				-r 3 \
				-e 2 \
				--Epochs 2 \
				--Polynomial_degree 14 \
				--Number_of_levels 9 \
				--Scale_bits 30 \
				--Training_samples 64 \
				--Testing_samples 100 \
				--Batch_size 8 \
				--Maximum_tile_size 4 \
				--Number_of_labels 5 \
				--Number_of_features 4095 \
				--Hash_size 0 \
				--Regularization 0.1 \
				--Approximation "none" \
				--Momentum_coefficient 0.85 \
				--Number_of_threads 2 \
				--Train_file "/data/pytorch_training.csv" \
				--Test_file "/data/pytorch_testing.csv" \
				--Weights_location "/ct/test/small" \
				--Weights_name "weights" \
				--Predict_weights_name "predict_names.txt" \
				--Records_file "records.csv" \
				--Ciphertext_location "/ct/test/" \
				--Ciphertext_train_name "batch" \
				--Ciphertext_test_name "test"

# You might need to change some of the file names and locations,
# depending on your setup