Here is some code for running tests. The program requires a number of 
parameters encoded as .jsons in order to run, and these folders
give two combinations for doing so. 

test_small gives a small scale test that should work on most machines
test_full gives a full run, which will be very intensive on every 
machine. Recommend at least 32 GB RAM, multiple cores, and roughly 8 hours

These will run best inside the docker container provided. 
Data should be in /data/