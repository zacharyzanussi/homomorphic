#!/bin/bash

HE_PATH=/workspaces/homomorphic/workspace/cpp/build36

# Make generator and client
(cd $HE_PATH && make generator client)

# copy them here
cp $HE_PATH/generator $HE_PATH/client .

# run them both
./generator data_params.json
./client data_params.json

cd test_1
./test_run.sh