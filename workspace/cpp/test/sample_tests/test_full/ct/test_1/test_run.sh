#!/bin/bash

HE_PATH=/workspaces/homomorphic/workspace/cpp/build36

# Make generator and client
(cd $HE_PATH && make create_weights cloud_nesterov refresh_weights predict_clear)

# copy them here
cp $HE_PATH/create_weights $HE_PATH/cloud_nesterov $HE_PATH/refresh_weights $HE_PATH/predict_clear .

rm momentum.ct *.log predict_weights.txt

# run them both
./create_weights params_1.json 0

./cloud_nesterov params_1.json 0000
./refresh_weights params_1.json

./cloud_nesterov params_2.json 0060
./refresh_weights params_2.json

./cloud_nesterov params_3.json 0020
# ./refresh_weights params_3.json

./predict_clear params_3.json
